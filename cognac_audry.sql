-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2019 at 05:19 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cognac_audry`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(10) UNSIGNED NOT NULL,
  `ref_id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8_unicode_ci NOT NULL,
  `conten` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(10) NOT NULL,
  `reviews` int(10) NOT NULL,
  `more_config` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `admin_config` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'id',
  `facebook_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googleplus_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `ref_id`, `title`, `thumb_image`, `short_description`, `conten`, `meta_title`, `meta_keyword`, `meta_description`, `position`, `published`, `link`, `slug`, `parent_id`, `longitude`, `latitude`, `rating`, `reviews`, `more_config`, `admin_config`, `lang`, `facebook_link`, `twitter_link`, `googleplus_link`, `email`, `created_at`, `updated_at`) VALUES
(1, 0, 'logo', 'logoheader2.png', '', '', '', '', '', 'header', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 07:00:41', '2018-10-10 07:00:41'),
(2, 0, 'Audry cultivation', '', 'Short Description Home', '<p style="text-align:center">Audry cultivates the art of selecting, ageing and blending the best cognacs. Our blends are made from the finest cognacs of the two premier districts of the Cognac area: Grande Champagne and Petite Champagne.</p>\r\n\r\n<p style="text-align:center">We have retained a&nbsp;traditional approach offering the highest quality in limited quantities. The know-how of our Cellar Master consists in selecting and&nbsp;blending several lots of different cognacs having achieved maturity and revealing their supplementary qualities, all blooming into&nbsp;a harmonious whole, diservedly holding the&nbsp;attention of the connoisseur and worthy of an unhurried and deliberate tasting.</p>\r\n\r\n<p style="text-align:center">When a very old Cognac presents a strong personality with &nbsp;a particularly typified character we offer it as it is, straight from the barrel rather than&nbsp;blending it. Such&nbsp;is the case&nbsp;with our &quot;Grande Champagne R&eacute;serve Aristide&quot;.</p>\r\n', '', '', '', 'home-content', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 07:03:32', '2018-10-10 01:05:10'),
(3, 0, 'short_description', '', '<p>Audry cultivates the art of selecting, ageing and blending the best cognacs. Our blends are made from the finest cognacs of the two premier districts of the Cognac area: Grande Champagne and Petite Champagne. We have retained a&nbsp;traditional approach offering the highest quality in limited quantities.</p>\r\n', '', '', '', '', 'footer', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 07:07:30', '2018-10-10 09:49:32'),
(4, 0, 'address', '', '<p>- Le fief Gallet Pessines 17460 Th&eacute;nac (France)</p>\r\n\r\n<p>- 10, rue du Commandant Rivi&egrave;re 75008 Paris (France)</p>\r\n', '', '', '', '', 'footer', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 07:11:21', '2018-10-10 09:49:33'),
(5, 0, 'phone1', '', '+33 (0)5 46 92 65 38', '', '', '', '', 'footer', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 07:19:41', '2018-10-10 07:19:41'),
(6, 0, 'phone2', '', '+33 (0)1 45 63 25 66 ', '', '', '', '', 'footer', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 07:19:41', '2018-10-10 09:49:33'),
(7, 0, 'email', '', 'yogaapriadi@gmail.com', '', '', '', '', 'footer', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 07:40:23', '2018-10-10 07:40:23'),
(8, 0, '', 'Untitled-2.png', '', '', '', '', '', 'slider', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 01:51:24', '2018-10-10 07:51:24'),
(9, 0, '', 'Untitled-3.png', '', '', '', '', '', 'slider', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 01:51:24', '2018-10-10 07:51:24'),
(10, 0, '', 'Untitled-5.png', '', '', '', '', '', 'slider', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 01:51:24', '2018-10-10 07:51:24'),
(11, 0, 'Napoleon', '0_0_orig.jpg', '<p>Information about Napoleon</p>\r\n', '<p style="text-align:justify">Our Napoleon, blend containing 50% of Grande Champagne and 50%&nbsp;of Petite Champagne, is the cognac of entry into our range of Fines Champagnes. Supple and smooth the freshness of its bouquet is accompanied with scents of vanilla and lime tree.<br />\r\nLight mouth with notes of candied fruits. Nice balance between the bracing effect of a still young cognac and roundness of an already mature brandy.</p>\r\n', 'cognac,audry,napoleon', 'cognac,audry,napoleon', 'cognac,audry,napoleon', 'collection-item', 'yes', '', 'Napoleon', 0, '', '', 5, 10, '0', '0', 'eng', '', '', '', '', '2018-10-10 01:56:59', '2018-10-15 22:58:41'),
(12, 0, 'XO', 'fullsizeoutput_4834.jpeg', '<p>Information about XO</p>\r\n', '<p>Equal Blend of Grandes and Petites Champagnes, 40% vol.<br />\r\nThe first nose of black pepper and vanilla gives way to spicy, floral notes.<br />\r\nOn the patate : bittersweet, toasty, creamy flavour followed by hints of dark toffee and almond. A melted sensation voluptious and well-ballanced ; the alcohol does not overpower. Very expressive, lively and elegant finish.</p>\r\n', 'cognac,audry,xo', 'cognac,audry,xo', 'cognac,audry,xo', 'collection-item', 'yes', '', 'XO', 0, '', '', 5, 8, '0', '0', 'eng', '', '', '', '', '2018-10-10 01:58:48', '2018-10-10 07:58:48'),
(13, 0, 'Reserve Speciale', '61.jpg', '<p>Information about Reserve Speciale</p>\r\n', '<p>Equal Blend of Grandes and Petites Champagnes, 40% vol.<br />\r\nOak and resin aroma with notes of vanilla, toasted almonds, citrus and bittersweet nougat. Full-bodied bouquet with overtones of caramel, then black tea, honey and ancient oak come to the fore.</p>\r\n', 'cognac,audry,Reserve', 'cognac,audry,Reserve', 'cognac,audry,Reserve', 'collection-item', 'yes', '', 'Reserve-Speciale', 0, '', '', 5, 11, '0', '0', 'eng', '', '', '', '', '2018-10-10 02:11:20', '2018-10-10 08:11:20'),
(14, 0, 'Memorial', 'audry_memorial_carafe-441x640.jpg', '<p>Information about memorial</p>\r\n', '<p>Blend of Grandes and Petites Champagnes, 42% vol.<br />\r\nA rich and complex aromatic palette: green pepper and fresh apple, spices, tobacco, roasted coffee beans, cocoa, and rancio combine to give a somptuous bouquet. On the palate: full-bodied and mellow with subtle touches of anis, violette and licorice. Persistent smooth honeyed satiny aftertaste.</p>\r\n', 'cognac,audry,memorial', 'cognac,audry,memorial', 'cognac,audry,memorial', 'collection-item', 'yes', '', 'Memorial', 0, '', '', 5, 15, '0', '0', 'eng', '', '', '', '', '2018-10-10 02:13:37', '2018-10-10 08:13:37'),
(15, 0, 'Exception', 'cq5dam_web_1280_1280.jpeg', '<p>Information about exception</p>\r\n', '<p>Blend of Grandes and Petites Champagnes, 43% vol.<br />\r\nThe very best of our range of blends. An exceptional aromatic richness: at first old oak, roasted pear, with overtones of almond paste and then nougat and rancio come to the fore. &quot;I am delighted by the most coveted cognac virtue of all, the rancio, this precious gift a rising from the oxidation process and a very long maturing period&quot; explains Paul Pacult.</p>\r\n', 'cognac,audry,exception', 'cognac,audry,exception', 'cognac,audry,exception', 'collection-item', 'yes', '', 'Exception', 0, '', '', 5, 16, '0', '0', 'eng', '', '', '', '', '2018-10-10 02:15:43', '2018-10-10 08:15:43'),
(16, 0, 'La Tres Ancienne Grande Champagne Reserve Arisitide', '27754_hr3.jpg', '<p>Information about La Tres Ancienne Grande Champagne Reserve Arisitide</p>\r\n', '<p>Bottled straight out of the barrel at&nbsp;50%,&nbsp;after five decades maturing. An amazing array of aromas engulfs you in successive waves:spice, baked peach, black pepper, licorice, fig, nutmeg, cloves, honey, violette, marzipan...<br />\r\nIn the words of Jean Aubry, from Montreal: &quot;The most perfect example of an eau-de-vie that reveals itself like Russian nesting dolls. Once you have started you can&#39;t stop yourself looking ever further&quot;.</p>\r\n', 'cognac,audry', 'cognac,audry', 'cognac,audry', 'collection-item', 'yes', '', 'La-Tres-Ancienne-Grande-Champagne-Reserve-Arisitide', 0, '', '', 5, 10, '0', '0', 'eng', '', '', '', '', '2018-10-10 02:20:11', '2018-10-10 08:20:11'),
(17, 0, 'Awesome Cognac', '', 'Jason Mraz', '<p>The cognac&nbsp;is really awesome because its made by masterpiece</p>\r\n', '', '', '', 'user-comments', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:29:40', '2018-10-10 08:29:40'),
(18, 0, 'Great Wine', '', 'Yoga Apriadi', '<p>the wine is really great because it&#39;s made of something really interesting</p>\r\n', '', '', '', 'user-comments', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:31:06', '2018-10-10 08:31:06'),
(19, 0, 'Super Awesome', '', 'Agus Arim', '<p>The best winein the world</p>\r\n', '', '', '', 'user-comments', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:31:45', '2018-10-10 08:31:45'),
(20, 0, 'Partners A', 'partner_logo_dark_1.png', '', '', '', '', '', 'partners-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:57:38', '2018-10-10 08:57:38'),
(21, 0, 'Partners B', 'partner_logo_dark_2.png', '', '', '', '', '', 'partners-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:57:50', '2018-10-10 08:57:50'),
(22, 0, 'Partners C', 'partner_logo_dark_3.png', '', '', '', '', '', 'partners-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:58:03', '2018-10-10 08:58:03'),
(23, 0, 'Partners D', 'partner_logo_dark_4.png', '', '', '', '', '', 'partners-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:58:15', '2018-10-10 08:58:15'),
(24, 0, 'Partners E', 'partner_logo_dark_5.png', '', '', '', '', '', 'partners-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:58:28', '2018-10-10 08:58:28'),
(25, 0, 'Partners F', 'partner_logo_dark_6.png', '', '', '', '', '', 'partners-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 02:58:43', '2018-10-10 08:58:43'),
(26, 0, 'News-image', 'news.png', '', '', '', '', '', 'page-image', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 09:20:31', '2018-10-10 09:21:42'),
(27, 0, 'Legacy-image', 'legacy.jpg', '', '', '', '', '', 'page-image', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 09:24:06', '2018-10-10 09:43:05'),
(28, 0, 'Ourcognac-image', 'ourcognac.jpg', '', '', '', '', '', 'page-image', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 09:31:42', '2018-10-10 09:44:42'),
(29, 0, 'Editorial-image', 'editorial.jpg', '', '', '', '', '', 'page-image', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 09:33:03', '2018-10-10 09:45:43'),
(30, 0, 'Findus-image', 'findus.jpg', '', '', '', '', '', 'page-image', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 09:34:15', '2018-10-10 09:46:40'),
(31, 0, 'Contact-image', 'contactus.png', '', '', '', '', '', 'page-image', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 09:37:24', '2018-10-10 09:37:24'),
(32, 0, 'Legacy', '', 'A Legacy Rekindled', '<p style="text-align:justify">The house of&nbsp;<strong>A. Edmond Audry</strong>&nbsp;was founded in 1878 by the great-great-grandfather of the current owner. In the early 1950s, upon the death of Aristide Boisson who married Odette Audry in 1905, the Audry Company stopped selling cognac. However some very old stocks were held back in reserve in the hope that Audry would one day rise again to the fore. That day came in 1978 when Bernard Boisson decided to perpetuate the family tradition that had&nbsp;been dormant for four decades.</p>\r\n\r\n<p style="text-align:justify">Audry Cognacs quickly graced the tables of some of the world finest and most widely acclaimed restaurants. They were available at exclusive retailers, receive the highest praise from connoiseurs and attracted flattering reviews in food and wine magazines : Hatier, le Cognac de A &agrave; Z : &quot;<em>Edmond Audry,&nbsp;&nbsp;this discreet house is little known.&nbsp;This does not prevent connoisseurs from appreciating the quality of its production&quot;.</em></p>\r\n\r\n<p style="text-align:justify">Revue des Vins de France :&nbsp;<em>&quot;Audry, small little known producer.&nbsp;Cognacs with a rich bouquet&nbsp; and notes of aromatic citrus fruit.&nbsp;Their Tr&egrave;s Ancienne Grande Champagne reaches the sublime&quot;.</em></p>\r\n\r\n<p style="text-align:justify">Le Figaro : &quot;<em>Major tasting session, sixteen&nbsp;exceptional cognacs... Magnificient, some legentary bottles, third place Audry Tr&egrave;s Ancienne Grande Champagne&quot;.</em></p>\r\n\r\n<p style="text-align:justify">Cuisine et Vins de France :&nbsp;<em>&quot;... and an&nbsp;exquisite Fins Bois, &quot;Tr&egrave;s Ancien&quot; from Audry with woody aromas of exceptional finesse&quot;.</em></p>\r\n\r\n<p style="text-align:justify">So nobody had known Audry for nearly half a century and it reappeared on an exclusive niche market.</p>\r\n', '', '', '', 'legacy-content', '', '', 'Legacy', 0, '', '', 0, 0, '0', '0', '', '', '', '', '', '2018-10-10 09:53:07', '2018-10-10 03:54:32'),
(33, 0, 'Renaud Boisson', '01.png', 'CEO', '', '', '', '', 'team-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 03:58:17', '2018-10-10 09:58:17'),
(34, 0, 'Jonas Calneck', '03.png', 'CMO', '', '', '', '', 'team-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 03:58:46', '2018-10-10 09:58:46'),
(35, 0, 'Dwijayadi', '05.png', 'Lead Designer', '', '', '', '', 'team-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 03:59:16', '2018-10-10 09:59:16'),
(36, 0, 'Editorial', '', 'Have you heard of AUDRY?', '<p>Since its revival, 30 years ago, Audry cognacs have attracted many laudatory reviews. Until recently nearly all of them stressed the fact that we were very discreet and little known. &ldquo;<em><u>This discreet house is little known. This does not prevent connoisseurs from appreciating the quality of its production</u></em>&rdquo;Cognac dictionnary published by Hatier.</p>\r\n\r\n<p>&ldquo;<em><u>Audry is a small little-known producer.Their Tr&egrave;s Ancienne Grande Champagne reaches the sublime</u></em>&rdquo;</p>\r\n\r\n<p>The most striking comment was the conclusion of a report about Cognacs issued in Time Magazine by Alice Feiring (December 2005) :&nbsp;<strong>&quot;<em><u>the best cognac you have never heard of is Audry</u></em></strong>&rdquo; It was a challenge. We decided to take it up.</p>\r\n\r\n<p>The question was : how can we remain the best, or at least among the very best cognacs, and&nbsp;strengthen its reputation&nbsp;?</p>\r\n\r\n<p>The only option was to increase significantly the number of Audry cognac tastings.</p>\r\n\r\n<p>Today, as a result, we are rewarded with very positive comments from numerous Guides such as&nbsp;<a href="http://cognac-audry.com/liens/091106-Guide-Hachette.pdf" target="_blank"><u>Hachette</u></a>,&nbsp;<u><a href="http://cognac-audry.com/liens/091106-Guide-Gault.pdf" target="_blank"><u>Gault Millau</u></a></u>,&nbsp;<a href="http://cognac-audry.com/chroniques/Audry_Somm_Inter.pdf">SommelierS&nbsp;International</a>&nbsp;and&nbsp;<a href="http://cognac-audry.com/liens/tasted.jpg" target="_blank">Isa Bal</a>, Europe&rsquo;s best sommelier 2008 and with articles in New York, San Francisco, Toronto, London, Paris, Geneva, Moscow, Singapore, Seoul &hellip;/&hellip;</p>\r\n\r\n<p>Last but not least the&nbsp;<a href="http://cognac-audry.com/liens/2012_Audry_Tasted.pdf">tasting organized by Tasted</a>&nbsp;in Germany with Andreas Larsson (best sommelier in the world 2007) and Markus del Monego (best world&#39;s sommelier 1998).<br />\r\nOur Grande Champagne R&eacute;serve Aristide received a score of 94 and is in the top 3.<br />\r\nThe Exception received a score of&nbsp;92.5, the R&eacute;serve Sp&eacute;ciale 92, the Memorial&nbsp;91 and the XO 89 which are impressive results.</p>\r\n\r\n<p>Finally, F.&nbsp;Paul&nbsp;Pacult&nbsp;just ranked&nbsp;in 6th position&nbsp;our&nbsp;Exception.</p>\r\n', '', '', '', 'editorial-content', '', '', 'editorial', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 10:02:01', '2018-10-10 10:02:01'),
(37, 0, 'Indonesia', '', '', '', '', '', '', 'find-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 04:19:31', '2018-10-10 10:19:31'),
(38, 37, 'PT. Sukses Abadi', '', 'Jalan Dalung Permai', '', '', '', '', '', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 10:19:31', '2018-10-10 10:19:31'),
(39, 37, 'PT. Akasia Lancar', '', 'Jalan Akasia No.15', '', '', '', '', '', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 10:19:31', '2018-10-10 10:19:31'),
(40, 0, 'Malaysia', '', '', '', '', '', '', 'find-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 04:20:05', '2018-10-10 10:20:05'),
(41, 40, 'Cubit Company', '', 'Hentak Bumi Street', '', '', '', '', '', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-10 10:20:05', '2018-10-10 10:20:05'),
(42, 0, 'News Item 1', 'discover1_detail-11.jpg', '<p>News item 1</p>\r\n', '<p>News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1</p>\r\n\r\n<p>News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1 News item 1</p>\r\n\r\n<p>News item 1 News item 1 News item 1 News item 1 News item 1</p>\r\n', 'news1', 'news1', 'news1', 'news-item', 'yes', '', 'News-Item-1', 0, '', '', 0, 0, '0', '0', 'eng', '', '', '', '', '2018-10-10 04:30:38', '2018-10-15 22:21:38'),
(43, 0, 'News Item 2', 'adventure.jpg', '<p>News Item 2</p>\r\n', '<p>News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2</p>\r\n\r\n<p>News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2</p>\r\n\r\n<p>News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2 News Item 2</p>\r\n', 'news2', 'news2', 'news2', 'news-item', 'yes', '', 'News-Item-2', 0, '', '', 0, 0, '0', '0', 'eng', '', '', '', '', '2018-10-10 04:32:16', '2018-10-10 10:32:16'),
(44, 0, 'News Item 3', 'adventure-road-bike.jpg', '<p>News Item 3</p>\r\n', '<p>News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3</p>\r\n\r\n<p>News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3</p>\r\n\r\n<p>News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3</p>\r\n\r\n<p>News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3 News Item 3</p>\r\n\r\n<p>News Item 3 News Item 3 News Item 3 News Item 3</p>\r\n', 'news3', 'news3', 'news3', 'news-item', 'yes', '', 'News-Item-3', 0, '', '', 0, 0, '0', '0', 'eng', '', '', '', '', '2018-10-10 04:33:11', '2018-10-10 10:33:11'),
(45, 0, 'News Item 4', 'shop-sect1-post-1.jpg', '<p>News Item 4</p>\r\n', '<p>News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4</p>\r\n\r\n<p>News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4</p>\r\n\r\n<p>News Item 4 News Item 4 News Item 4 News Item 4 News Item 4 News Item 4</p>\r\n\r\n<p>News Item 4 News Item 4 News Item 4 News Item 4 News Item 4</p>\r\n\r\n<p>News Item 4 News Item 4 News Item 4</p>\r\n\r\n<p>News Item 4 News Item 4 News Item 4</p>\r\n', 'news4', 'news4', 'news4', 'news-item', 'yes', '', 'News-Item-4', 0, '', '', 0, 0, '0', '0', 'eng', '', '', '', '', '2018-10-10 04:35:08', '2018-10-10 10:35:08'),
(46, 0, 'News Item 5', 'gambar-kucing-lucu-111.jpg', '<p>News Item 5</p>\r\n', '<p>News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5&nbsp; News Item 5 News Item 5 News Item 5 News Item 5 News Item 5</p>\r\n\r\n<p>News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5</p>\r\n\r\n<p>News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5</p>\r\n\r\n<p>News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5 News Item 5</p>\r\n\r\n<p>News Item 5 News Item 5 News Item 5 News Item 5 News Item 5</p>\r\n', 'News Item 5 News Item 5', 'News Item 5', 'News Item 5', 'news-item', 'yes', '', 'News-Item-5', 0, '', '', 0, 0, '0', '0', 'eng', '', '', '', '', '2018-10-15 22:35:42', '2018-10-16 04:35:42'),
(47, 0, 'News Item 6', 'noimage.png', '<p>News Item 6</p>\r\n', '<p>News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6</p>\r\n\r\n<p>News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6</p>\r\n\r\n<p>News Item 6 News Item 6 News Item 6</p>\r\n\r\n<p>News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6 News Item 6</p>\r\n', 'News Item 6', 'News Item 6', 'News Item 6', 'news-item', 'yes', '', 'News-Item-6', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-15 22:36:35', '2018-10-16 04:36:35'),
(48, 0, 'Jhon Pantau', 'noimage.png', 'IT', '', '', '', '', 'team-item', '', '', '', 0, '', '', 0, 0, '0', '0', 'id', '', '', '', '', '2018-10-15 22:48:33', '2018-10-16 04:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `receipt` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider_text1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider_text2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider_text3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `image`, `link`, `type`, `slider_type`, `slider_text1`, `slider_text2`, `slider_text3`, `article_id`, `lang`, `created_at`, `updated_at`) VALUES
(1, '', 'IMG_0261.JPG', '', '', '', '', '', '', 11, 'id', NULL, NULL),
(2, '', 'fullsizeoutput_4836.jpeg', '', '', '', '', '', '', 12, 'id', NULL, NULL),
(3, '', 'IMG_02611.JPG', '', '', '', '', '', '', 12, 'id', NULL, NULL),
(4, '', '26470_hr.jpg', '', '', '', '', '', '', 13, 'id', NULL, NULL),
(5, '', '27754_hr.jpg', '', '', '', '', '', '', 13, 'id', NULL, NULL),
(6, '', 'audry-xo-cognac.jpg', '', '', '', '', '', '', 13, 'id', NULL, NULL),
(7, '', 'fullsizeoutput_48341.jpeg', '', '', '', '', '', '', 13, 'id', NULL, NULL),
(8, '', 'fullsizeoutput_48361.jpeg', '', '', '', '', '', '', 13, 'id', NULL, NULL),
(9, '', '26470_hr1.jpg', '', '', '', '', '', '', 14, 'id', NULL, NULL),
(10, '', '27754_hr1.jpg', '', '', '', '', '', '', 14, 'id', NULL, NULL),
(11, '', 'audry-xo-cognac1.jpg', '', '', '', '', '', '', 14, 'id', NULL, NULL),
(12, '', 'fullsizeoutput_48342.jpeg', '', '', '', '', '', '', 14, 'id', NULL, NULL),
(13, '', 'fullsizeoutput_48362.jpeg', '', '', '', '', '', '', 14, 'id', NULL, NULL),
(14, '', '26470_hr2.jpg', '', '', '', '', '', '', 15, 'id', NULL, NULL),
(15, '', '27754_hr2.jpg', '', '', '', '', '', '', 15, 'id', NULL, NULL),
(16, '', 'audry_memorial_carafe-441x6401.jpg', '', '', '', '', '', '', 15, 'id', NULL, NULL),
(17, '', 'audry-xo-cognac2.jpg', '', '', '', '', '', '', 15, 'id', NULL, NULL),
(18, '', 'fullsizeoutput_48343.jpeg', '', '', '', '', '', '', 15, 'id', NULL, NULL),
(19, '', 'fullsizeoutput_48363.jpeg', '', '', '', '', '', '', 15, 'id', NULL, NULL),
(20, '', '26470_hr3.jpg', '', '', '', '', '', '', 16, 'id', NULL, NULL),
(21, '', 'audry-xo-cognac3.jpg', '', '', '', '', '', '', 16, 'id', NULL, NULL),
(22, '', 'cq5dam_web_1280_12801.jpeg', '', '', '', '', '', '', 16, 'id', NULL, NULL),
(23, '', 'fullsizeoutput_48344.jpeg', '', '', '', '', '', '', 16, 'id', NULL, NULL),
(24, '', 'IMG_02612.JPG', '', '', '', '', '', '', 16, 'id', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `social_media` varchar(200) NOT NULL,
  `link` varchar(500) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `published` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`social_media`, `link`, `icon`, `published`) VALUES
('Facebook', 'yogaapriadi@gmail.com', 'fa fa-facebook', 'Yes'),
('Google-plus', 'yogaapriadi@gmail.com', 'fa fa-google-plus', 'Yes'),
('Instagram', 'yogaapriadi@gmail.com', 'fa fa-instagram', 'Yes'),
('Linkedin', 'yogaapriadi@gmail.com', 'fa fa-linkedin', 'Yes'),
('Twitter', 'yogaapriadi@gmail.com', 'fa fa-twitter', 'Yes'),
('Youtube', 'yogaapriadi@gmail.com', 'fa fa-youtube', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `full_admin` enum('0','1') NOT NULL DEFAULT '0',
  `type` varchar(15) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_token` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `full_admin`, `type`, `remember_token`, `created_at`, `updated_at`, `created_token`) VALUES
(1, 'Admin Cognac1', 'agusdownload5293@gmail.com', 'cognac098', 'audrylogo.png', '0', 'admin', '468bde706a88966b188f9c30889a64', '2018-10-10 08:38:27', '2018-10-30 22:21:51', '2018-10-16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
  ADD PRIMARY KEY (`social_media`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
