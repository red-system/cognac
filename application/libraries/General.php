<?php
/**
 * Created by PhpStorm.
 * User: Mahendra Wardana Z
 * Date: 8/5/2019
 * Time: 10:36 AM
 */

class General
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function resize_image($path_from, $path_to, $width = 1920, $create_thumb = FALSE)
    {
        $config1['image_library'] = 'gd2';
        $config1['source_image'] = $path_from;
        $config1['create_thumb'] = $create_thumb;
        $config1['maintain_ratio'] = TRUE;
        $config1['width'] = $width;
        $config1['new_image'] = $path_to;
        $this->ci->image_lib->initialize($config1);
        $this->ci->image_lib->resize();
        $this->ci->image_lib->clear();
    }

    public function resize_image_ratio_height($path_from, $path_to, $height = 1080, $create_thumb = FALSE)
    {
        $config1['image_library'] = 'gd2';
        $config1['source_image'] = $path_from;
        $config1['create_thumb'] = $create_thumb;
        $config1['maintain_ratio'] = TRUE;
        $config1['height'] = $height;
        $config1['new_image'] = $path_to;
        $this->ci->image_lib->initialize($config1);
        $this->ci->image_lib->resize();
        $this->ci->image_lib->clear();
    }

    public function resize_image_with_height($path_from, $path_to, $width = 1920, $height = 1080, $create_thumb = FALSE)
    {
        $config1['image_library'] = 'gd2';
        $config1['source_image'] = $path_from;
        $config1['create_thumb'] = $create_thumb;
        $config1['maintain_ratio'] = FALSE;
        $config1['width'] = $width;
        $config1['height'] = $height;
        $config1['new_image'] = $path_to;
        $this->ci->image_lib->initialize($config1);
        $this->ci->image_lib->resize();
        $this->ci->image_lib->clear();
    }

    public function slug($title)
    {
        return url_title($title, 'dash', true);
    }

    public function data_general()
    {
        $data = array(
            'facebook_link' => 'https://www.facebook.com/Red-System-444865549587464/',
            'facebook_title' => 'Red System',
            'twitter_link' => 'https://www.twitter.com/forstaff.id',
            'twitter_title' => 'Red System',
            'instagram_link' => 'https://www.instagram.com/red.system/',
            'instagram_title' => 'Red System',
            'linkedin_link' => 'https://www.linkedin.com/company/redsystemid/',
            'linkedin_title' => 'Red System',
        );
        return $data;
    }
}