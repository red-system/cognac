<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'Home';
$route['legacy'] = 'home/legacy';
$route['cognac'] = 'home/cognac';
$route['cognac-details/(:any)'] = 'home/cognac_details/$1';
$route['news-details/(:any)'] = 'home/news_details/$1';
$route['news'] = 'home/news';
$route['editorial'] = 'home/editorial';
$route['find'] = 'home/find';
$route['contact'] = 'home/contact';
$route['advanced_backend'] = 'advanced_backend/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
