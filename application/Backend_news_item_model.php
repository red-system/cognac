<?php
class Backend_news_item_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_count_by_position($position){
    $query = $this->db->query("SELECT * FROM article where position = '$position'");
    return $query->num_rows();
  }

  public function get_news_item($slug = FALSE){
      if($slug === FALSE){
        $this->db->order_by('updated_at', 'DESC');
        $query = $this->db->get_where('article',array('position' => 'news-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function record_count() {
    $query = $this->db->where('position', 'news-item')->get('article');
    return $query->num_rows();
  }

  public function get_news_item_page($limit, $start){
        $this->db->order_by('updated_at', 'DESC');
        $query = $this->db->where('position', 'news-item')->get('article',$limit, $start);
        return $query;
    }

  public function get_news_item_sort($slug = FALSE){
      if($slug === FALSE){
        $this->db->order_by('updated_at', 'DESC');
        $this->db->limit(3, 0);
        $query = $this->db->get_where('article',array('position' => 'news-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('position' => 'news-item','slug' => $slug));
      return $query->row_array();
  }

  public function get_news_item_other($slug = FALSE){
      if($slug === FALSE){
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('position', 'news-item');
        $this->db->order_by('rand()');
        $this->db->limit(3, 0);
        $query = $this->db->get();
        return $query->result_array();
      }
      $this->db->select('*');
      $this->db->from('article');
      $this->db->where('position', 'news-item');
      $this->db->where('slug !=',$slug);
      $this->db->order_by('rand()');
      $this->db->limit(3, 0);
      $query = $this->db->get();
      return $query->result_array();
  }

  public function get_news_item_without_pagination($slug = FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article',array('position' => 'news-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_news_item_by_id($id){
      $query = $this->db->get_where('article', array('id' => $id));
      return $query->row_array();
  }


  public function get_news_item_by_pagination($data){
        $result_array['active_row_controller'] = $data['active_row_controller'];
        $result_array['row_per_page'] = $data['row_per_page'];
        $result_array['row_start'] = $data['row_start'];
        $this->db->limit($result_array['row_per_page'], $result_array['row_start']);
        $query = $this->db->get_where('article',array('position' => 'news-item'));
        $result_array['result_array'] = $query->result_array();
        return $result_array;
  }

  public function add_news($add_data){
    $slug = url_title($this->input->post('title'));
    $thumb_image = $add_data['file_name'];
    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'thumb_image' => $thumb_image,
        'short_description' => $this->input->post('short_description'),
        'conten' => $this->input->post('content'),
        'meta_title' => $this->input->post('meta_title'),
        'meta_keyword' => $this->input->post('meta_keyword'),
        'meta_description' => $this->input->post('meta_description'),
        'position' => 'news-item',
        'published' => $this->input->post('published'),
        'lang' => $this->input->post('lang'),
        'created_at' =>date('Y-m-d H:i:s')
    );
    return $this->db->insert('article',$data);
  }

  public function update_news_content($additional_data){
    $id = $this->input->post('id');
    $slug = url_title($this->input->post('title'));

    if(isset($additional_data['image_name'])){
      $data = array(
      'title' => $this->input->post('title'),
      'slug' => $slug,
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'meta_title' => $this->input->post('meta_title'),
      'meta_keyword' => $this->input->post('meta_keyword'),
      'meta_description' => $this->input->post('meta_description'),
      'published' => $this->input->post('published'),
      'lang' => $this->input->post('lang'),
      'updated_at' =>date('Y-m-d H:i:s'),
      'thumb_image' => $additional_data['image_name']
      );
    } else {
      $data = array(
      'title' => $this->input->post('title'),
      'slug' => $slug,
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'meta_title' => $this->input->post('meta_title'),
      'meta_keyword' => $this->input->post('meta_keyword'),
      'meta_description' => $this->input->post('meta_description'),
      'published' => $this->input->post('published'),
      'lang' => $this->input->post('lang'),
      'updated_at' =>date('Y-m-d H:i:s')
       );
    }
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function delete_news_item($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }
}
