<section class="section-wrap-small pt-40 pb-40">
	<div class="container">
		<div class="row heading">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h2 class="bottom-line"><?php echo $home_content['title'];?></h2>
			</div>
		</div>
		<div class="about-description">
					<?php echo $home_content['conten'];?>
				</div>
	</div>
</section>

<section class="section-wrap-small pt-0 pb-40">
	<div class="container">

		<div class="row heading">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h2 class="bottom-line">Collection of Audry Cognac's</h2>
			</div>
		</div>

		<div class="row items-grid">
  <?php foreach ($collection_items as $collection_item) :  ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="product-item">
					<div class="product-img hover-1">
						<a href="#">
							<img style="width:293px;height:428.867px" src="<?php echo base_url();?>assets/img/collection/<?php echo $collection_item['thumb_image'];?>" alt="">
							<img src="img/shop/shop_item_1_back.jpg" alt="" class="back-img">
						</a>
						<div class="product-label">
							<span class="sale">Cognac Audry</span>
						</div>
						<div class="hover-overlay">
							<div class="product-add-to-wishlist">
								<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>">
							</div>
							<div class="product-actions">
								<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>" class="btn btn-dark btn-md">More Details</a>
								
							</div>
							<div class="product-quickview">
								<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>" class="btn-quickview">Quick View</a>
							</div>
						</div>
					</div>
					<div class="product-details">
						<h3>
							<a class="product-title" href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>"><?php echo $collection_item['title'];?></a>
						</h3>
					</div>
				</div>
			</div>
  <?php endforeach; ?>



    <!--Pagination-->
    <nav class="pagination clear">
      <?php

        $total_row = $count;

        //current row existence check
        if(!isset($active_row_controller)){
          $current_row = 1;
        } else {
          $current_row = $active_row_controller;
        }

        $row_per_page = 8 ;


        $last_row = 0;
        if(!isset($row_start)){
          $last_row_for_prev = 1;
        } else {
          $last_row_for_prev = $row_start - $row_per_page;
        }

        $next_last_row = 0;
        $active_row = $current_row;
        if($total_row<8){
          $mod_row=1;
        } else {
            $mod_row = $total_row / $row_per_page;
            if($total_row % $row_per_page != 0){
              $mod_row = $mod_row + 1;
            }
            $mod_row = floor($mod_row);
            //echo "<br>";
            //echo 'mod row = '.$mod_row;
            //echo "<br>";
            //echo 'current row = '.$current_row;
        }

      ?>

            <?php
              //showing prev button
              if($current_row!=1){
                echo '<a class="" href="'.base_url().'home/cognac_pagination/'.($current_row-1).'/'.$row_per_page.'/'.($last_row_for_prev).'"><i class="fa fa-angle-left"></i></a>';
              }
            ?>
            <?php if($mod_row<=5) {?>
            <?php  for ($i=1; $i <= $mod_row; $i++) { ?>
              <a class="<?php if($i == $current_row){ echo 'page-numbers current'; $next_last_row = $last_row; }else{}?>" href="<?php if($i == $current_row){ echo '#'; }else{ echo base_url().'home/cognac_pagination/'.$i.'/'.$row_per_page.'/'.$last_row; }?>"><?php echo $i ?></a>
              <?php $last_row = $last_row + $row_per_page; ?>
            <?php }
          } else {
            ?>
            <?php  for ($i=($mod_row-4); $i<=$mod_row; $i++) { ?>
              <a class="<?php if($i == $current_row){ echo 'page-numbers current'; $next_last_row = $last_row; }else{}?>" href="<?php if($i == $current_row){ echo '#'; }else{ echo base_url().'home/cognac_pagination/'.$i.'/'.$row_per_page.'/'.$last_row; }?>"><?php echo $i ?></a>
              <?php $last_row = $last_row + $row_per_page; ?>
            <?php }
            }
            ?>

            <?php
              //showing prev button
              if($current_row!=$mod_row){
                $data['active_row']=$active_row+1;
                $data['row_per_page']=$row_per_page;
                $data['row_start'] = $next_last_row+$row_per_page;
                echo '<a href="'.base_url().'home/cognac_pagination/'.($active_row+1).'/'.$row_per_page.'/'.($next_last_row+$row_per_page).'"><i class="fa fa-angle-right"></i></a>';
              }
            ?>

    </nav>



		</div> <!-- end row -->
	</div>
</section>


		<!-- From Blog / Testimonials -->
<section class="section-wrap-small pt-0 pb-40">
	<div class="container">
		<div class="row heading">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h2 class="bottom-line">Latest News</h2>
			</div>
		</div>

		<div class="row">

			<div class="col-md-12 from-blog">
				<div class="row">
					<?php foreach ($news as $news) :  ?>
						<article class="col-sm-3 col-xs-12 mb-20">
							<div class="entry-img">
								<a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>" class="hover-scale">
									<img style="height: 174.567px" src="<?php echo base_url(); ?>assets/img/blog/<?php echo $news['thumb_image'];?>" alt="">
								</a>
							</div>
							<div class="entry">
								<h4 class="entry-title"><a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>"><?php echo $news['title'];?></a></h4>
								<div class="entry-content">
									
									<a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>" class="read-more dark-link">Read More <i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						</article> <!-- end post -->
					<?php endforeach; ?>

				</div>
				<div  class="text-center">
					<a href="<?php echo base_url();?>news" class="btn btn-md btn-color rounded text-center">View All News</a>
				</div>
			</div> <!-- end from blog -->

		</div> <!-- end row -->
	</div>
</section> <!-- end from blog / testimonials -->

<!-- Testimonials -->
<section class="section-wrap parallax-testimonials nopadding relative">

	<div class="relative">

		<h2 class="text-center uppercase color-white">Happy Customers</h2>
		<div id="owl-testimonials" class="owl-carousel owl-theme text-center">
			<?php foreach ($user_comments as $user_comment) :  ?>
			<div class="item">
				<div class="container testimonial">
					<div class="row">
						<div class="col-md-12">
							<p class="testimonial-text"><?php echo strip_tags($user_comment['conten']);?></p>
							<span><?php echo $user_comment['short_description'];?></span>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>

	</div>

	<div class="parallax" data-stellar-background-ratio="0.5"></div>
</section> <!-- end testimonials -->

<br><br>
<!-- Brands -->
<section class="section-wrap brands-slider nopadding">
	<div class="container">
		<div class="row heading">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h2 class="bottom-line">Our Partners</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">

				<div id="owl-partners" >
					<?php foreach ($partners as $partner) :  ?>
					<div class="item">
						<a>
							<img src="<?php echo base_url(); ?>assets/img/partners/<?php echo $partner['thumb_image'];?>" alt="">
						</a>
					</div> <!-- end first logo -->
					<?php endforeach;?>
				</div> <!-- end carousel -->
				<br>
			</div>
		</div>
	</div>
</section> <!-- end brands -->
