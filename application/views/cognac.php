<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $ourcognac['thumb_image'];?>);">
	<div class="container relative clearfix">
		<div class="title-holder">
			<div class="title-text">
				<h1 class="uppercase">Our Cognac</h1>
        <ol class="breadcrumb">
          <li>
            <a href="<?php echo base_url();?>home">Home</a>
          </li>
          <li class="active">
            Our Cognac
          </li>
        </ol>
			</div>
		</div>
	</div>
</section>
<section class="section-wrap-small new-arrivals pb-20">
			<div class="container">

				<h2 class="heading text-center relative heading-small uppercase bottom-line style-2"><?php echo $collection_header['title']; ?></h2>

				<div class="row items-grid">
          <?php foreach ($collection_items as $collection_item) :  ?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="product-item">
							<div class="product-img hover-1">
								<a href="#">
									<img style="width:262.5px;height:382.233px" src="<?php echo base_url();?>assets/img/collection/<?php echo $collection_item['thumb_image'];?>" alt="">
                  <img style="width:262.5px;height:382.233px" src="<?php echo base_url();?>assets/img/collection/<?php echo $collection_item['thumb_image'];?>" alt="" class="back-img"
								</a>
								<div class="product-label">
									<span class="sale">Cognac Audry</span>
								</div>
								<div class="hover-overlay">
									<div class="product-add-to-wishlist">
										<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>">
									</div>
									<div class="product-actions">
										<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>" class="btn btn-dark btn-md">More Details</a>
									</div>
									<div class="product-quickview">
										<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>" class="btn-quickview">Quick View</a>
									</div>
								</div>
							</div>
							<div class="product-details">
								<h3>
									<a class="product-title" href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>"><?php echo $collection_item['title'];?></a>
								</h3>
							</div>
						</div>
					</div>
          <?php endforeach; ?>



            <!--Pagination-->
            <nav class="pagination clear">
              <?php

                $total_row = $count;

                //current row existence check
                if(!isset($active_row_controller)){
                  $current_row = 1;
                } else {
                  $current_row = $active_row_controller;
                }

                $row_per_page = 8 ;


                $last_row = 0;
                if(!isset($row_start)){
                  $last_row_for_prev = 1;
                } else {
                  $last_row_for_prev = $row_start - $row_per_page;
                }

                $next_last_row = 0;
                $active_row = $current_row;
                if($total_row<8){
                  $mod_row=1;
                } else {
                    $mod_row = $total_row / $row_per_page;
                    if($total_row % $row_per_page != 0){
                      $mod_row = $mod_row + 1;
                    }
                    $mod_row = floor($mod_row);
                    //echo "<br>";
                    //echo 'mod row = '.$mod_row;
                    //echo "<br>";
                    //echo 'current row = '.$current_row;
                }

              ?>

                    <?php
                      //showing prev button
                      if($current_row!=1){
                        echo '<a class="" href="'.base_url().'home/cognac_pagination/'.($current_row-1).'/'.$row_per_page.'/'.($last_row_for_prev).'"><i class="fa fa-angle-left"></i></a>';
                      }
                    ?>
                    <?php if($mod_row<=5) {?>
                    <?php  for ($i=1; $i <= $mod_row; $i++) { ?>
                      <a class="<?php if($i == $current_row){ echo 'page-numbers current'; $next_last_row = $last_row; }else{}?>" href="<?php if($i == $current_row){ echo '#'; }else{ echo base_url().'home/cognac_pagination/'.$i.'/'.$row_per_page.'/'.$last_row; }?>"><?php echo $i ?></a>
                      <?php $last_row = $last_row + $row_per_page; ?>
                    <?php }
                  } else {
                    ?>
                    <?php  for ($i=($mod_row-4); $i<=$mod_row; $i++) { ?>
                      <a class="<?php if($i == $current_row){ echo 'page-numbers current'; $next_last_row = $last_row; }else{}?>" href="<?php if($i == $current_row){ echo '#'; }else{ echo base_url().'home/cognac_pagination/'.$i.'/'.$row_per_page.'/'.$last_row; }?>"><?php echo $i ?></a>
                      <?php $last_row = $last_row + $row_per_page; ?>
                    <?php }
                    }
                    ?>

                    <?php
                      //showing prev button
                      if($current_row!=$mod_row){
                        $data['active_row']=$active_row+1;
                        $data['row_per_page']=$row_per_page;
                        $data['row_start'] = $next_last_row+$row_per_page;
                        echo '<a href="'.base_url().'home/cognac_pagination/'.($active_row+1).'/'.$row_per_page.'/'.($next_last_row+$row_per_page).'"><i class="fa fa-angle-right"></i></a>';
                      }
                    ?>

            </nav>



				</div> <!-- end row -->
			</div>
		</section>
