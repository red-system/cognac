<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $legacy['thumb_image'];?>);">
	<div class="container relative clearfix">
		<div class="title-holder">
			<div class="title-text">
				<h1 class="uppercase">Legacy</h1>
				<ol class="breadcrumb">
					<li>
						<a href="<?php echo base_url();?>home">Home</a>
					</li>
					<li class="active">
						Legacy
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>

<!-- About us / Progress Bars -->
<section class="section-wrap-small pt-40 pb-40">
	<div class="container">
		<div class="row heading">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h2 class="bottom-line"><?php echo strip_tags($legacy_content['short_description']);?></h2>
			</div>
		</div>
		<div class="row">

			<div class="col-sm-12">
				<div class="about-description">
					<?php echo $legacy_content['conten'];?>
				</div>
			</div> <!-- end col -->



		</div>
	</div>
</section> <!-- end progress bars -->

<!-- Our Team -->
<section class="section-wrap-lg our-team pb-0">
	<div class="container">

		<div class="row heading">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h2 class="bottom-line">Our Team</h2>
			</div>
		</div>

		<div class="row">
			<?php foreach ($teams as $team) :  ?>
			<div class="col-md-3 col-sm-6 team-wrap mb-40 wow fadeInUp" data-wow-duration="1.2s" data-wow-delay=".1s">
				<div class="team-member text-center">
					<div class="team-img" onclick="return true">
						<img style="width: 220px;height: 220px"  src="<?php echo base_url();?>assets/img/team/<?php echo $team['thumb_image'];?>" alt="">
						<div class="overlay">
							<div class="team-details text-center">
								<div class="social-icons rounded">
									<a href="<?php echo $team['facebook_link'];?>"><i class="fa fa-facebook"></i></a>
									<a href="<?php echo $team['twitter_link'];?>"><i class="fa fa-twitter"></i></a>
									<a href="<?php echo $team['googleplus_link'];?>"><i class="fa fa-google-plus"></i></a>
									<a href="<?php echo $team['email'];?>"><i class="fa fa-envelope"></i></a>
								</div>
							</div>
						</div>
					</div>
					<h4 class="team-title"><?php echo $team['title'];?></h4>
					<span><?php echo $team['short_description'];?></span>
				</div>
			</div> <!-- end team member -->
			<?php endforeach; ?>
			
		</div>
	</div>
</section> <!-- end our team -->


<!-- Call to Action -->
<section class="call-to-action bg-light">
	<div class="container">
		<div class="row">

			<div class="col-sm-9 col-xs-12">
				<h2 class="mb-0">Do you have questions?</h2>
			</div>

			<div class="col-sm-3 col-xs-12 cta-button">
				<a href="<?php echo base_url();?>contact" class="btn btn-lg btn-dark">Contact Us Now</a>
			</div>

		</div>
	</div>
</section> <!-- end call to action -->
