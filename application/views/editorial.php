<!-- Page Title -->
<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $editorial['thumb_image'];?>);">
	<div class="container relative clearfix">
		<div class="title-holder">
			<div class="title-text">
				<h1 class="uppercase">Editorial</h1>
        <ol class="breadcrumb">
          <li>
            <a href="<?php echo base_url();?>home">Home</a>
          </li>
          <li class="active">
            Editorial
          </li>
        </ol>
			</div>
		</div>
	</div>
</section><!-- end page title -->

<!-- About us / Progress Bars -->
<section class="section-wrap" id="intro">
	<div class="container">
		<div class="row">

			<div class="col-sm-12">
				<div class="about-description">
					<h4><?php echo $editorial_content['short_description'];?></h4>
					<?php echo $editorial_content['conten'];?>

				</div>
			</div> <!-- end col -->



		</div>
	</div>
</section> <!-- end progress bars -->


<!-- Call to Action -->
<section class="call-to-action bg-light">
	<div class="container">
		<div class="row">

			<div class="col-sm-9 col-xs-12">
				<h2 class="mb-0">Are you ready to enjoy?</h2>
			</div>

			<div class="col-sm-3 col-xs-12 cta-button">
				<a href="<?php echo base_url();?>contact" class="btn btn-lg btn-dark">Contact Us Now</a>
			</div>

		</div>
	</div>
</section> <!-- end call to action -->
