
		
		<!-- Footer Type-1 -->
		<footer class="footer footer-type-1">
			<div class="container">
				<div class="footer-widgets">
					<div class="row">

						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="widget footer-about-us">
								<h5>About Us</h5>
								<p class="mb-0"><?php echo $short_description['short_description'];?></p>
							</div>
							<div class="footer-socials mt-20">
								<div class="social-icons dark rounded">
									<?php
									foreach ($social_medias as $social_media) :
										echo '<a target="_blank" href="'.$social_media['link'].'"><i class="'.$social_media['icon'].'"></i></a>';
									endforeach;
									?>
								</div>
							</div>
						</div> <!-- end about us -->

						<div class="col-md-3 col-sm-6 col-xs-12">
							<h5>Get in Touch</h5>
							<div class="contact-item">
								<div class="contact-icon">
									<i class="icon icon_house_alt"></i>
								</div>
								<p class="footer-address"><?php echo $address['short_description'];?></p>
								<div class="contact-icon">
									<i class="fa fa-phone"></i>
								</div>
								<p><a href="tel:<?php echo $phone1['short_description'];?>"><?php echo $phone1['short_description'];?></a></p>
								<p><a href="tel:<?php echo $phone2['short_description'];?>"><?php echo $phone2['short_description'];?></a></p>
								<div class="contact-icon">
									<i class="fa fa-envelope"></i>
								</div>
								<p><a href="mailto:<?php echo $email['short_description'];?>"><?php echo $email['short_description'];?></a></p>
							</div> <!-- end address -->
							
						</div> <!-- end stay in touch -->

						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="widget footer-links">
								<h5>Useful Links</h5>
								<ul>
									<li><a href="<?php echo base_url();?>news">News</a></li>
									<li><a href="<?php echo base_url();?>contact">Contact Us</a></li>
								</ul>
							</div>
						</div> <!-- end useful links -->

						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="widget footer-posts">
								<h5>Latest Posts</h5>
								<div class="footer-entry-list">
									<ul class="posts-list no-top-pad">
										<?php foreach ($news as $news) :  ?>
										<li class="footer-entry small-space">
											<article class="post-small clearfix">
												<div class="entry">
													<h3 class="entry-title"><a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>"><?php echo $news['title'];?></a></h3>
													<ul class="entry-meta list-inline">
														<li class="entry-date">
															<a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>"><?php echo $news['updated_at'];?></a>
														</li>
													</ul>
												</div>
											</article>
										</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div> <!-- end latest posts -->

						

					</div>
				</div>		
			</div> <!-- end container -->

			<div class="bottom-footer">
				<div class="container">
					<div class="row">

						<div class="col-sm-8 copyright">
							<span>
								© <?php echo date ('Y');?> <a href="http://www.audrycognac.com/" target="_blank">Cognac Audry</a> |  Template by <a href="http://cortechstudio.com" target="_blank">Cortech Studio</a>
							</span>
						</div>

						

					</div>
				</div>
			</div> <!-- end bottom footer -->
		</footer> <!-- end footer -->

		<div id="back-to-top">
			<a href="#top"><i class="fa fa-angle-up"></i></a>
		</div>

	</div> <!-- end main-wrapper -->

	<!-- jQuery Scripts -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/rev-slider.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/scripts.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>

	<script type="text/javascript">
      var $star_rating = $('.star-rating .fa');

      var SetRatingStar = function() {
        return $star_rating.each(function() {
          if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
          } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
          }
        });
      };

      SetRatingStar();
      $(document).ready(function() {

      });

      
    </script>

</body>
</html>
