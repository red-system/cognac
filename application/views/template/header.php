<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cognac Audry</title>

	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="cognac , wine ">
	<link rel="author" href="https://plus.google.com/u/0/104296509460513856975" />

	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700,600,800,400,300%7CMontserrat:400,700%7CRaleway:600,400' rel='stylesheet'>

	<!-- Css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/magnific-popup.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/revolution/css/settings.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/rev-slider.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-icons.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/sliders.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/spacings.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.min.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color.css" />

	<!-- Favicons -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/img/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/img/apple-touch-icon-114x114.png">

</head>

<body style="font-family: Raleway, sans-serif">
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <header class="nav-type-3">

    <nav class="navbar navbar-static-top">
      <div class="navigation">
        <div class="container relative">

          <div class="row">

            <div class="navbar-header">
              
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div> <!-- end navbar-header -->

            <div class="header-wrap col-md-12">

              <!-- Search -->
              <div class="nav-search type-2 hidden-sm hidden-xs">
                <?php echo form_open_multipart('home/collection_search','method="post"'); ?>
                  <input type="text" name="title" class="form-control" placeholder="Search our cognac">
                  <button type="submit" class="search-button">
                    <i class="icon icon_search"></i>
                  </button>
                </form>
              </div>

              <!-- Logo -->
              <div class="logo-container">
                <div class="logo-wrap text-center">
                  <a href="<?php echo base_url();?>">
                    <img class="logo" src="<?php echo base_url();?>assets/img/<?php echo $logo['thumb_image'];?>" alt="logo" style="width: 170px;height: 60px">
                  </a>
                </div>
              </div>

              <!-- Cart -->
              <div class="nav-cart-wrap style-2 hidden-sm hidden-xs">
                <div class="nav-cart right">
                  <div class="nav-cart-ammount hidden-sm hidden-xs">
                    <a href="<?php echo base_url();?>contact">Contact Us</a>
                  </div>
                 
                  </div>

                </div>
              </div> <!-- end cart -->

            </div> <!-- end header wrap -->

            <div class="col-md-12 nav-wrap">
              <div class="collapse navbar-collapse" id="navbar-collapse">

                <ul class="nav navbar-nav">

                  <li id="mobile-search" class="hidden-lg hidden-md">
                    <form method="get" class="mobile-search relative">
                      <input type="search" class="form-control" placeholder="Search...">
                      <button type="submit" class="search-button">
                        <i class="icon icon_search"></i>
                      </button>
                    </form>
                  </li>

                  <li class="<?php if($this->session->menu=='home'){echo('active');} ?>">
                    <a href="<?php echo base_url();?>home" class="" data-toggle="">Home</a>
                  </li>

                  <li class="<?php if($this->session->menu=='legacy'){echo('active');} ?>">
                    <a href="<?php echo base_url();?>legacy" class="" data-toggle="">Legacy</a>
                  </li>

									<li class="<?php if($this->session->menu=='cognac'){echo('active');} ?>">
                    <a href="<?php echo base_url();?>cognac" class="" data-toggle="">Our Cognac</a>
                  </li>

									<li class="<?php if($this->session->menu=='editorial'){echo('active');} ?>">
                    <a href="<?php echo base_url();?>editorial" class="" data-toggle="">Editorial</a>
                  </li>

									<li class="<?php if($this->session->menu=='find'){echo('active');} ?>">
                    <a href="<?php echo base_url();?>find" class="" data-toggle="">Find Us</a>
                  </li>

                  

                </ul> <!-- end menu -->
              </div> <!-- end collapse -->
            </div> <!-- end col -->

          </div> <!-- end row -->
        </div> <!-- end container -->
      </div> <!-- end navigation -->
    </nav> <!-- end navbar -->
  </header>

  	<div class="main-wrapper shop oh">
