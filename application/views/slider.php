<section>
  <div class="rev_slider_wrapper">
    <div class="rev_slider" id="slider2" data-version="5.0">
      <ul>
        <!-- SLIDE 1 -->
          <?php foreach ($sliders as $slider) :  ?>
        <li data-fstransition="fade"
          data-transition="zoomout"
          data-slotamount="1"
          data-masterspeed="1000"
          data-delay="8000">

          <!-- MAIN IMAGE -->
          <img src="<?php echo base_url();?>assets/img/slides/<?php echo $slider['thumb_image'];?>"
            alt=""
            data-bgrepeat="no-repeat"
            data-bgfit="cover"
            data-bgparallax="7"
            class="rev-slidebg"
            >

          <!-- LAYER NR. 1 -->
          <!-- <div class="tp-caption hero-text style-2 size-70"
            data-x="center"
            data-y="center"
            data-voffset="[-73,-73,-73,-90]"
            data-fontsize="[70,60,50,40]"
            data-lineheight="['70','60','50','40']"
            data-whitespace="[nowrap, nowrap, nowrap, normal]"
            data-transform_idle="o:1;s:500"
            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
            data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
            data-start="1000"
            data-splitout="none"
            data-elementdelay="0.01"
            style="text-align: center;"><?php echo $slider['title'];?>
          </div> -->

          <!-- LAYER NR. 2 -->
          <!-- <div class="tp-caption medium-text"
            data-x="center"
            data-y="center"
            data-voffset="[0,0,0,20]"
            data-whitespace="[nowrap, nowrap, nowrap, normal]"
            data-width="[none, none, none, 300]"
            data-transform_idle="o:1;s:500"
            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
            data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
            data-start="1200"
            data-elementdelay="0.01"
            style="text-align: center;"><?php echo $slider['short_description'];?>
          </div> -->

          <!-- LAYER NR. 3 -->
          <!-- <div class="tp-caption tp-resizeme"
            data-x="center"
            data-y="center"
            data-voffset="[80,80,80,100]"
            data-hoffset="0"
            data-transform_idle="o:1;s:500"
            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
            data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
            data-start="1400"
            data-elementdelay="0.01"
            data-linktoslide="next"
            style="letter-spacing: 2px;"><a href='#' class='btn btn-lg btn-transparent'><?php echo $slider['conten'];?></a>
          </div> -->

        </li>
<?php endforeach; ?>
      </ul>
    </div>
  </div>
</section>
