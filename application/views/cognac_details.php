<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $ourcognac['thumb_image'];?>);">
	<div class="container relative clearfix">
		<div class="title-holder">
			<div class="title-text">
				<h1 class="uppercase">Our Cognac</h1>
        <ol class="breadcrumb">
          <li>
            <a href="<?php echo base_url();?>home">Home</a>
          </li>
          <li class="active">
            <a href="<?php echo base_url();?>cognac">Our Cognac</a>
          </li>
          <li class="active">
            <?php echo $cognac_details['title'];?>
          </li>
        </ol>
			</div>
		</div>
	</div>
</section>


<!-- Single Product -->
<section class="section-wrap-small single-product">
	<div class="container relative">
		<div class="row">

			<div class="col-sm-5 col-xs-12 mb-60">

				<div class="gallery-main" >

					<div class="gallery-cell">
						<a href="<?php echo base_url();?>assets/img/collection/<?php echo $cognac_details['thumb_image'];?>" class="lightbox-product">
							<img src="<?php echo base_url();?>assets/img/collection/<?php echo $cognac_details['thumb_image'];?>" style="height: 600px " alt="" />
							<i class="arrow_expand"></i>
						</a>
					</div>
					<?php foreach ($ourcognacpic as $ourcognac) : ?>
						<div class="gallery-cell">
							<a href="<?php echo base_url();?>assets/img/collection/<?php echo $ourcognac['image'];?>" class="lightbox-product">
								<img src="<?php echo base_url();?>assets/img/collection/<?php echo $ourcognac['image'];?>" style="height: 600px" alt="" />
								<i class="arrow_expand"></i>
							</a>
						</div>
					<?php endforeach; ?>
				</div> <!-- end gallery main -->

				<div class="gallery-thumbs">

					<div class="gallery-cell">
						<img src="<?php echo base_url();?>assets/img/collection/<?php echo $cognac_details['thumb_image'];?>" alt="" />
					</div>
					<?php foreach ($ourcognacpic as $ourcognac) : ?>
						<div class="gallery-cell">
							<img src="<?php echo base_url();?>assets/img/collection/<?php echo $ourcognac['image'];?>" alt="" />
						</div>
					<?php endforeach; ?>
					
				</div> <!-- end gallery thumbs -->

			</div> <!-- end col img slider -->

			<div class="col-sm-7 col-xs-12 product-description-wrap">
				<h1 class="product-title"><?php echo $cognac_details['title'];?></h1>
				<div class="star-rating" style="font-size:2em;padding-left:0px;">
					<span class="fa fa-star-o" data-rating="1"></span>
					<span class="fa fa-star-o" data-rating="2"></span>
					<span class="fa fa-star-o" data-rating="3"></span>
					<span class="fa fa-star-o" data-rating="4"></span>
					<span class="fa fa-star-o" data-rating="5"></span>
					<input type="hidden" id="rate" name="rating" class="rating-value" value="<?php echo $cognac_details['rating'];?>">
				</div>

				<a href="#"><?php echo $cognac_details['reviews'];?> Reviews</a>	
				
				<span class="price">
					<ins>
						<span class="ammount"><?php echo $cognac_details['short_description'];?></span>
					</ins>
				</span>
				<p class="product-description"><?php echo $cognac_details['conten'];?></p>
				
				<div class="socials-share clearfix">
					<span>Share:</span>
					<div class="social-icons">
						<a href="https://twitter.com/share?url=http://www.audrycognac.com/cognac-details/<?php echo $cognac_details['slug'];?>" target="_blank" class="social-twitter" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
						<a href="https://www.facebook.com/sharer.php?p[url]=http://www.audrycognac.com/cognac-details/<?php echo $cognac_details['slug'];?>" target="_blank" class="social-facebook" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
						<a href="https://plus.google.com/share?url=http://www.audrycognac.com/cognac-details/<?php echo $cognac_details['slug'];?>" target="_blank" class="social-google-plus" data-toggle="tooltip" data-placement="top" title="Google +"><i class="fa fa-google-plus"></i></a>
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=http://www.audrycognac.com/cognac-details/<?php echo $cognac_details['slug'];?>" target="_blank" class="social-linkedin" data-toggle="tooltip" data-placement="top" title="Linked in"><i class="fa fa-linkedin"></i></a>
						
					</div>
				</div>
			</div> <!-- end col product description -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- end single product -->


<!-- Related Items -->
<section class="section-wrap-small related-products pt-0">
	<div class="container">
		<h2 class="heading text-center relative heading-small uppercase bottom-line style-2">Related Products</h2>
		
		<div class="row">

			<div id="owl-related-products" class="owl-carousel owl-theme">
				<?php foreach ($collection_items as $collection_item) :  ?>
				<div class="product-item">
					<div class="product-img hover-1">
						<a href="#">
							<img style="width:262.5px;height:382.233px" src="<?php echo base_url();?>assets/img/collection/<?php echo $collection_item['thumb_image'];?>" alt="">
							<img src="<?php echo base_url();?>assets/img/collection/<?php echo $collection_item['thumb_image'];?>" alt="" class="back-img">
						</a>
						<div class="product-label">
							<span class="sale">Cognac Audry</span>
						</div>
						<div class="hover-overlay">
							<div class="product-add-to-wishlist">
								<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>"></a>
							</div>
							<div class="product-actions">
								<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>" class="btn btn-dark btn-md">More Details</a>
							</div>
							<div class="product-quickview">
								<a href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>" class="btn-quickview">Quick View</a>
							</div>
						</div>
					</div>
					<div class="product-details">
						<h3>
							<a class="product-title" href="<?php echo base_url();?>cognac-details/<?php echo $collection_item['slug'];?>"><?php echo $collection_item['title'];?></a>
						</h3>
					</div>                				
				</div>
				<?php endforeach; ?>
			</div> <!-- end owl -->

		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- end related products -->