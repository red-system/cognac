<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $contactpage['thumb_image'];?>);">
	<div class="container relative clearfix">
		<div class="title-holder">
			<div class="title-text">
				<h1 class="uppercase">Contact Us</h1>
        		<ol class="breadcrumb">
			        <li>
			        	<a href="<?php echo base_url();?>home">Home</a>
			        </li>
          			<li class="active">
            			Contact Us
          			</li>
        		</ol>
			</div>
		</div>
	</div>
</section>

<section class="section-wrap-lg contact" id="contact">
	<div class="container">

		<div class="row">

			<div class="col-md-4 mb-40">
				<h5 class="uppercase">Information</h5>

				<div class="contact-item">
					<div class="contact-icon">
						<i class="icon icon_house_alt"></i>
					</div>
					<p class="footer-address"><?php echo $address['short_description'];?></p>
					<div class="contact-icon">
						<i class="fa fa-phone"></i>
					</div>
					<p><a href="tel:<?php echo $phone1['short_description'];?>"><?php echo $phone1['short_description'];?></a></p>
					<p><a href="tel:<?php echo $phone2['short_description'];?>"><?php echo $phone2['short_description'];?></a></p>
					<div class="contact-icon">
						<i class="fa fa-envelope"></i>
					</div>
					<p><a href="mailto:<?php echo $email['short_description'];?>"><?php echo $email['short_description'];?></a></p>
				</div> <!-- end address -->


			</div>

			<div class="col-md-8">
				<?php echo form_open_multipart('home/kirim_email','id="contact-form"'); ?>
					<div class="row row-16">
						<div class="col-md-6 contact-name">
							<input type="text" class="form-control" name="first-name" placeholder="First Name" required>
						</div>
						<div class="col-md-6 contact-email">
							<input type="email" class="form-control" name="email" placeholder="Email" required>
						</div>
					</div>

					<textarea rows="9" class="form-control" name="message" placeholder="Your Message" required></textarea>
					<button type="submit" class="btn btn-lg btn-color btn-submit"> Send Message </button> 
					<div id="msg" class="message"></div>
				</form>
			</div> <!-- end col -->

		</div>
	</div>
</section>
