<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li><a href="<?php echo base_url();?>backend_home/slides">slides</a></li>
					<li class="active"><a><?php echo $title;?></a></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
      <!-- page start-->
      	<section class="panel">
      		<div class="row">
				<div class="col-lg-12">
		         	<header class="panel-heading">
		              	<?php echo $title;?> File Image Upload
		          	</header>
		          	
			          	<div class="panel-body">
			          		<form action="<?php echo base_url();?>backend_home/slides_addprocess" class="dropzone" id="my-awesome-dropzone" enctype="multipart/form-data">
			          			
			          		</form>
			          		
			          	</div>
			          	<div class="form-group">
							<div class="col-lg-12">
								<a class="btn btn-shadow btn-default" title="view" href="<?php echo base_url();?>backend_home/slides" type="button"><i class="icon-reply"></i> Back</a>
								<a class="btn btn-info btn-default pull-right" title="view" href="<?php echo base_url();?>backend_home/slides_add" type="button"><i class="icon-repeat"></i> Refresh</a>
							</div>
						</div>
			          	
				</div>
			</div>
      	</section>
      <!-- page end-->
  	</section>
</section>

