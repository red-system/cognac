<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li><a href="<?php echo base_url();?>backend_findus/find">Find Us</a></li>
					<li class="active">Add Country</li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Add Country
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php echo form_open_multipart('backend_findus/find_addprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Country</strong></label>
								<div class="col-sm-10">
									<input type="text" name="country" class="form-control" required >
								</div>
							</div>

							<header>
								<h3 class="text-center">Add Company</h3>
							</header>

							<div id="repeater">
								<!-- Repeater Heading -->
			                    <div class="repeater-heading">
			                        <a type="button" class="btn btn-info pull-right repeater-add-btn">
										Add More <i class="icon-plus"></i>
									</a>
			                    </div>
			                    <div class="clearfix"></div><br>
			                    <div class="items" data-group="form[]">
			                        <!-- Repeater Content -->
			                        <div class="item-content">
			                            <div class="form-group">
			                                <label class="col-sm-2 control-label col-lg-2"><strong>Company</strong></label>
			                                <div class="col-sm-10">
			                                    <input type="text" data-skip-name="true" data-name="title[]" class="form-control"  id="title">
			                                </div>
			                            </div>
			                            <div class="form-group">
			                                <label class="col-sm-2 control-label col-lg-2">Address</label>
											<div class="col-sm-10">
												<input type="text" data-skip-name="true" data-name="short_description[]" class="form-control">
											</div>
			                            </div>
			                        </div><br>
			                        <!-- Repeater Remove Btn -->
			                        <div class="pull-right repeater-remove-btn">
			                            <button class="btn btn-danger remove-btn">
			                                Remove
			                            </button>
			                        </div>
			                        <div class="clearfix"></div><br>
			                    </div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-default" title="view" href="<?php echo base_url();?>backend_findus/find" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-success" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->



             