<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li><a href="<?php echo base_url();?>backend_findus/find">Find Us</a></li>
					<li class="active">Edit Country</li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Edit Country
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
	                         if($this->session->flashdata('true')){
	                       ?>
	                         <div class="alert alert-success"> 
	                           <?php  echo $this->session->flashdata('true'); ?>
	                          </div>
	                      <?php    
	                      }else if($this->session->flashdata('err')){
	                      ?>
	                       <div class = "alert alert-success">
	                         <?php echo $this->session->flashdata('err'); ?>
	                       </div>
	                      <?php } ?>
						<?php echo form_open_multipart('backend_findus/update_find/'.$find['id'],'class="form-horizontal tasi-form"','method="post"'); ?>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Country</strong></label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" name="idcountry" value="<?php echo $find['id'];?>" id="title">
									<input type="text" name="country" class="form-control" name="country" id="title" value="<?php echo $find['title'];?>" required>
								</div>
							</div>
							<header>
								<h3 class="text-center">Company</h3><br>
							</header>
							<?php foreach ($pts as $pt) :  ?>
							<div class="item-content">
	                            <div class="form-group">
	                                <label class="col-sm-2 control-label col-lg-2"><strong>Company</strong></label>
	                                <div class="col-sm-10">
	                                   	<input type="hidden" class="form-control" name="id[]" value="<?php echo $pt['id'];?>" id="title">
										<input type="text" class="form-control" name="title[<?php echo $pt['id']; ?>]" id="title" value="<?php echo $pt['title'];?>">
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label class="col-sm-2 control-label col-lg-2">Address</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="short_description[<?php echo $pt['id']; ?>]" value="<?php echo $pt['short_description'];?>">
									</div>
	                            </div>
	                        </div><br>
	                        <!-- Repeater Remove Btn -->
	                        <div class="pull-right repeater-remove-btn">
	                            <a class="btn btn-danger" href="<?php echo site_url('backend_findus/delete_company/'.$pt['id']); ?>" onclick="return confirm('Are you sure to delete <?php echo $pt['title'];?>?')"><i class="icon-trash"></i> Delete</a>
	                        </div>
	                        <div class="clearfix"></div><br>
	                        <?php endforeach; ?>
							

							<div class="text-center">
								<button type="submit" class="btn btn-primary">Submit</button>
								<a href="<?php echo base_url(); ?>backend_findus/find" ><button type="button" class="btn btn-default">Cancel</button></a>
								<a type="button" id="formButton" class="btn btn-success">Add More Company</a>
							</div><!--/form-group-->
						</form>
					</div>
				</section>
			</div>
		</div>

		<div id="form1">
			<div class="row">
				<div class="col-lg-12">
					<section class="panel">
						<header class="panel-heading">
		                  	Form Add Company
		                  	<span class="tools pull-right">
		                    	<a href="javascript:;" class="icon-chevron-down"></a>	
		                  	</span>
		              	</header>
						<div class="panel-body">
							<?php echo form_open_multipart('backend_findus/processcompany_add','class="form-horizontal tasi-form"','method="post"'); ?>
		                
								<div id="repeater">
									<!-- Repeater Heading -->
				                    <div class="repeater-heading">
				                        <a type="button" class="btn btn-info pull-right repeater-add-btn">
											Add More <i class="icon-plus"></i>
										</a>
				                    </div>
				                    <div class="clearfix"></div><br>
				                    <div class="items" data-group="form[]">
				                        <!-- Repeater Content -->
				                        <div class="item-content">
				                            <div class="form-group">
				                                <label class="col-sm-2 control-label col-lg-2"><strong>Company</strong></label>
				                                <div class="col-sm-10">
				                                    <input type="hidden" class="form-control" data-skip-name="true" data-name="id[]" value="<?php echo $find['id'];?>" id="title">
													<input type="text" data-skip-name="true" data-name="name[<?php echo $find['id'];?>][]" class="form-control"  id="title">
				                                </div>
				                            </div>
				                            <div class="form-group">
				                                <label class="col-sm-2 control-label col-lg-2">Address</label>
												<div class="col-sm-10">
													<input type="text" data-skip-name="true" data-name="address[<?php echo $find['id'];?>][]" class="form-control">
												</div>
				                            </div>
				                        </div><br>
				                        <!-- Repeater Remove Btn -->
				                        <div class="pull-right repeater-remove-btn">
				                            <button class="btn btn-danger remove-btn">
				                                Remove
				                            </button>
				                        </div>
				                        <div class="clearfix"></div><br>
				                    </div>
								</div>

								<div class="form-group">
									<div class="col-lg-12">
										<a class="btn btn-shadow btn-default" title="view" href="<?php echo base_url();?>backend_findus/find" type="button"><i class="icon-reply"></i> Back</a>
										<button class="btn btn-shadow btn-success" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
									</div>
								</div>
							</form>
						</div>
					</section>
				</div>
			</div>
		</div>

	</section>
</section>
<!--main content start-->




             