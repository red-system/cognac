<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li><a href="<?php echo base_url();?>backend_home/usercomment">User Comments</a></li>
					<li class="active">Edit User Comment</li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Edit User Comment
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
		                   if($this->session->flashdata('true')){
		                 ?>
		                   <div class="alert alert-success"> 
		                     <?php  echo $this->session->flashdata('true'); ?>
		                    </div>
		                <?php    
		                }else if($this->session->flashdata('err')){
		                ?>
		                 <div class = "alert alert-success">
		                   <?php echo $this->session->flashdata('err'); ?>
		                 </div>
		                <?php } ?>
						<?php echo form_open_multipart('backend_home/usercomment_updateprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Title</strong></label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" name="id" value="<?php echo $usercomment['id'];?>" >
									<input type="text" name="title" class="form-control" value="<?php echo $usercomment['title'];?>" required >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>User Name</strong></label>
								<div class="col-sm-10">
									<input type="text" name="short_description" class="form-control" value="<?php echo $usercomment['short_description'];?>" required >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Comment</strong></label>
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" id="editor1" name="content" rows="6" required><?php echo $usercomment['conten'];?></textarea><br>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-default" title="view" href="<?php echo base_url();?>backend_home/usercomment" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-success pull-right" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->

             