<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
	                <li class="active">Header & Footer</li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Header & Footer
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_home/update_home','class="form-horizontal tasi-form"'); ?>
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Short Description</strong></label>
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" name="short_description" rows="6" required><?php echo $short_description['short_description'];?></textarea><br>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Address</strong></label>
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" name="address" rows="6" required><?php echo $address['short_description'];?></textarea><br>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Phone 1</strong></label>
								<div class="col-lg-10">
									<input type="text" name="phone1" class="form-control" placeholder="Phone....." value="<?php echo $phone1['short_description'];?>" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Phone 2</strong></label>
								<div class="col-lg-10">
									<input type="text" name="phone2" class="form-control" placeholder="Phone....." value="<?php echo $phone2['short_description'];?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Email</strong></label>
								<div class="col-lg-10">
									<input type="text" name="email" class="form-control" placeholder="Email....." value="<?php echo $email['short_description'];?>" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong><?php echo $facebook['social_media'];?></strong></label>
								<div class="col-lg-6">
									<input type="text" name="facebook" class="form-control" placeholder="Facebook....." value="<?php echo $facebook['link'];?>"><br>
								</div>
								
								<label class="col-sm-1 control-label col-lg-1" ><strong>Publish ?</strong></label>
								<div class="col-lg-3">
									<select class="form-control" id="facebookpublished" name="facebookpublished">
	                                	<option value="<?php echo $facebook['published'];?>"><?php echo $facebook['published'];?> </option>
				                      	<option value="Yes"> Yes </option>
				                      	<option value="No"> No </option>
				                    </select>
				                </div>
							</div>
	                      
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong><?php echo $googleplus['social_media'];?></strong></label>
								<div class="col-lg-6">
									<input type="text" name="googleplus" class="form-control" placeholder="Google-plus....." value="<?php echo $googleplus['link'];?>"><br>
								</div>
								
								<label class="col-sm-1 control-label col-lg-1" ><strong>Publish ?</strong></label>
								<div class="col-lg-3">
									<select class="form-control" id="googlepluspublished" name="googlepluspublished">
	                                	<option value="<?php echo $googleplus['published'];?>"><?php echo $googleplus['published'];?> </option>
				                      	<option value="Yes"> Yes </option>
				                      	<option value="No"> No </option>
				                    </select>
				                </div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong><?php echo $linkedin['social_media'];?></strong></label>
								<div class="col-lg-6">
									<input type="text" name="linkedin" class="form-control" placeholder="Google-plus....." value="<?php echo $linkedin['link'];?>"><br>
								</div>
								
								<label class="col-sm-1 control-label col-lg-1" ><strong>Publish ?</strong></label>
								<div class="col-lg-3">
									<select class="form-control" id="linkedinpublished" name="linkedinpublished">
	                                	<option value="<?php echo $linkedin['published'];?>"><?php echo $linkedin['published'];?> </option>
				                      	<option value="Yes"> Yes </option>
				                      	<option value="No"> No </option>
				                    </select>
				                </div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong><?php echo $instagram['social_media'];?></strong></label>
								<div class="col-lg-6">
									<input type="text" name="instagram" class="form-control" placeholder="Google-plus....." value="<?php echo $instagram['link'];?>"><br>
								</div>
								
								<label class="col-sm-1 control-label col-lg-1" ><strong>Publish ?</strong></label>
								<div class="col-lg-3">
									<select class="form-control" id="instagrampublished" name="instagrampublished">
	                                	<option value="<?php echo $instagram['published'];?>"><?php echo $instagram['published'];?> </option>
				                      	<option value="Yes"> Yes </option>
				                      	<option value="No"> No </option>
				                    </select>
				                </div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong><?php echo $twitter['social_media'];?></strong></label>
								<div class="col-lg-6">
									<input type="text" name="twitter" class="form-control" placeholder="Google-plus....." value="<?php echo $twitter['link'];?>"><br>
								</div>
								
								<label class="col-sm-1 control-label col-lg-1" ><strong>Publish ?</strong></label>
								<div class="col-lg-3">
									<select class="form-control" id="twitterpublished" name="twitterpublished">
	                                	<option value="<?php echo $twitter['published'];?>"><?php echo $twitter['published'];?> </option>
				                      	<option value="Yes"> Yes </option>
				                      	<option value="No"> No </option>
				                    </select>
				                </div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong><?php echo $youtube['social_media'];?></strong></label>
								<div class="col-lg-6">
									<input type="text" name="youtube" class="form-control" placeholder="Google-plus....." value="<?php echo $youtube['link'];?>"><br>
								</div>
								
								<label class="col-sm-1 control-label col-lg-1" ><strong>Publish ?</strong></label>
								<div class="col-lg-3">
									<select class="form-control" id="youtubepublished" name="youtubepublished">
	                                	<option value="<?php echo $youtube['published'];?>"><?php echo $twitter['published'];?> </option>
				                      	<option value="Yes"> Yes </option>
				                      	<option value="No"> No </option>
				                    </select>
				                </div>
							</div>
	                      
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Logo</strong></label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 200px; height: 100px;">
											<img src="<?php echo base_url();?>assets/img/<?php echo $logo['thumb_image'];?>" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="logo" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	</section>
</section>