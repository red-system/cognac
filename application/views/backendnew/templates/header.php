<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/<?php echo $profile['image'];?>">

    <title>Admin-Cognac Audry</title>


    <link href="<?php echo base_url();?>assets_b/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets_b/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets_b/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link href="<?php echo base_url();?>assets_b/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/css/gallery.css" />
    <link href="<?php echo base_url();?>assets_b/assets/dropzone/css/dropzone.css" rel="stylesheet"/>

    <link href="<?php echo base_url();?>assets_b/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets_b/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-daterangepicker/daterangepicker.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-fileupload/bootstrap-fileupload.css" />

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets_b/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets_b/css/style-responsive.css" rel="stylesheet" />




    <!-- Bootstrap core CSS -->
    <!-- <link href="<?php echo base_url();?>assets_b/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- <link href="<?php echo base_url();?>assets_b/css/bootstrap-reset.css" rel="stylesheet"> -->

    <!--external css-->
    <!-- <link href="<?php echo base_url();?>assets_b/assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> -->
    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets_b/css/owl.carousel.css" type="text/css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-fileupload/bootstrap-fileupload.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-datepicker/css/datepicker.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-colorpicker/css/colorpicker.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/assets/bootstrap-daterangepicker/daterangepicker.css" /> -->
    <!-- <link href="<?php echo base_url();?>assets_b/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets_b/css/gallery.css" /> -->
    <!-- <link href="<?php echo base_url();?>assets_b/assets/dropzone/css/dropzone.css" rel="stylesheet"/> -->

    <!-- Custom styles for this template -->
    <!-- <link href="<?php echo base_url();?>assets_b/css/style.css" rel="stylesheet"> -->
    <!-- <link href="<?php echo base_url();?>assets_b/css/style-responsive.css" rel="stylesheet" /> -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header white-bg">
            <div class="sidebar-toggle-box">
                <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
            </div>
            <!--logo start-->
            <a href="<?php echo base_url();?>backend_profile/profile" class="logo"><span>C-AUDRY</span> ADMIN</a>
            <!--logo end-->
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" style="width: 29px;height: 29px" src="<?php echo base_url();?>assets/img/<?php echo $profile['image'];?>">
                            <span class="username"><?php echo $profile['name'];?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li><a href="<?php echo base_url();?>user_auth/logout"><i class="icon-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>
      <!--header end-->
      