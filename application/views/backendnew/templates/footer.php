      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              <span>
                © <?php echo date ('Y');?> <a href="http://www.audrycognac.com/" target="_blank">Cognac Audry</a> |  Template by <a href="http://cortechstudio.com" target="_blank">Cortech Studio</a>
              </span>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/jquery.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/jquery-1.8.3.min.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/bootstrap.min.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/jquery-ui-1.9.2.custom.min.js"></script> -->
    <!-- <script class="include" type="text/javascript" src="<?php echo base_url();?>assets_b/js/jquery.dcjqaccordion.2.7.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/jquery.scrollTo.min.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/jquery.nicescroll.js" type="text/javascript"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/owl.carousel.js" ></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/jquery.customSelect.min.js" ></script> -->
    <!-- <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets_b/assets/advanced-datatable/media/js/jquery.dataTables.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/data-tables/jquery.dataTables.js"></script> -->
    <!-- <script type="text/javascript" src="?php echo base_url();?>assets_b/assets/data-tables/DT_bootstrap.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/assets/dropzone/dropzone.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/respond.min.js" ></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/modernizr.custom.js"></script> -->
    <!-- <script src="<?php echo base_url();?>assets_b/js/toucheffects.js"></script> -->
    <!-- <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets_b/assets/advanced-datatable/media/js/jquery.js"></script> -->

    
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/jquery-multi-select/js/jquery.multi-select.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/ckeditor/ckeditor.js"></script> -->
    
    <!--custom tagsinput-->
    <!-- <script src="<?php echo base_url();?>assets_b/js/jquery.tagsinput.js"></script> -->

    <!--common script for all pages-->
    <!-- <script src="<?php echo base_url();?>assets_b/js/common-scripts.js"></script> -->

    <!--script for this page body-->
    <!-- <script src="<?php echo base_url();?>assets_b/js/count.js"></script> -->


<!-- js placed at the end of the document so the pages load faster -->
  <script src="<?php echo base_url();?>assets_b/js/jquery.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/jquery.scrollTo.min.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/js/jquery.validate.min.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/jquery-ui-1.9.2.custom.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo base_url();?>assets_b/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/repeater.js"></script>
  

  <script src="<?php echo base_url();?>assets_b/assets/fancybox/source/jquery.fancybox.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/modernizr.custom.js"></script>
  <script src="<?php echo base_url();?>assets_b/js/toucheffects.js"></script>

  <script src="<?php echo base_url();?>assets_b/assets/dropzone/dropzone.js"></script>

  <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets_b/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

  <!--custom switch-->
  <script src="<?php echo base_url();?>assets_b/js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="<?php echo base_url();?>assets_b/js/jquery.tagsinput.js"></script>
  <!--custom checkbox & radio-->
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/js/ga.js"></script>
  <!--script for this page-->
  <script src="<?php echo base_url();?>assets_b/js/form-validation-script.js"></script>


  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/ckeditor/ckeditor.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets_b/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  
  

  <!--common script for all pages-->
  <script src="<?php echo base_url();?>assets_b/js/common-scripts.js"></script>

  <!--script for this page-->
  <script src="<?php echo base_url();?>assets_b/js/form-component.js"></script>


  <!--script for this page form-->
  <script src="<?php echo base_url();?>assets_b/js/advanced-form-components.js"></script>
  

  <script src="<?php echo base_url();?>assets_b/js/respond.min.js" ></script>
    
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#example').dataTable( {
                "aaSorting": [[ 0, "asc" ]]
            } );
        } );
    </script>

    <script>
        window.onload = function() {

        document.getElementById("form1").style.display = "none";

      };
      
        $("#formButton").click(function(){
            $("#form1").toggle();
        });
    </script>

    <script>
        /* Create Repeater */
        $("#repeater").createRepeater({
            showFirstItemToDefault: true,
        });
    </script>
  
    <script type="text/javascript">
      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
      });

    </script>

    <script>
      function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
   
          return false;
        return true;
      }
    </script>

    <script type="text/javascript">
      var $star_rating = $('.star-rating .fa');

      var SetRatingStar = function() {
        return $star_rating.each(function() {
          if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
          } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
          }
        });
      };

      SetRatingStar();
      $(document).ready(function() {

      });

      $star_rating.on('click', function() {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
      });
    </script>

  </body>
</html>
