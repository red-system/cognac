<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a class="<?php if($this->session->menu=='backend_admin'){echo('active');} ?>" href="<?php echo base_url();?>backend_profile/profile">
                          <i class="icon-user"></i>
                          <span>Administrator</span>
                      </a>
                  </li>

                  <li>
                      <a class="<?php if($this->session->menu=='backend_headerfooter'){echo('active');} ?>" href="<?php echo base_url();?>backend_home/headerfooter" >
                          <i class="icon-laptop"></i>
                          <span>Header & Footer</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_slides'||$this->session->menu=='backend_descriptionhome'||$this->session->menu=='backend_news'||$this->session->menu=='backend_comments'||$this->session->menu=='backend_partner'){echo('active');} ?>" href="javascript:;"  >
                          <i class="icon-home"></i></i>
                           <span>Home</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_slides'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_home/slides">Slides</a></li>
                          <li class="<?php if($this->session->menu=='backend_descriptionhome'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_home/descriptionhome">Description Home</a></li>
                          <li class="<?php if($this->session->menu=='backend_news'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_home/news">News</a></li>
                          <li class="<?php if($this->session->menu=='backend_comments'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_home/usercomment">User Comments</a></li>
                          <li class="<?php if($this->session->menu=='backend_partner'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_home/ourpartners">Our Partners</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_legacydescription'||$this->session->menu=='backend_team'||$this->session->menu=='backend_piclegacy'){echo('active');} ?>" href="javascript:;"  >
                          <i class=" icon-exclamation-sign"></i></i>
                           <span>Legacy</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_piclegacy'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_legacy/picturelegacy">Image Legacy Pages</a></li>
                          <li class="<?php if($this->session->menu=='backend_legacydescription'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_legacy/legacydescription">Legacy Description</a></li>
                          <li class="<?php if($this->session->menu=='backend_team'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_legacy/team">Team</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_ourcognac'||$this->session->menu=='backend_picourcognac'){echo('active');} ?>" href="javascript:;"  >
                          <i class="icon-glass"></i>
                          <span>Our Cognac</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_picourcognac'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_ourcognac/pictureourcognac"> Image Our Cognac Pages</a></li>
                          <li class="<?php if($this->session->menu=='backend_ourcognac'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_ourcognac/ourcognac">Collection Cognac</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_editorial'||$this->session->menu=='backend_piceditorial'){echo('active');} ?>" href="javascript:;"  >
                          <i class="icon-location-arrow"></i>
                          <span>Editorial</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_piceditorial'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_editorial/pictureeditorial">Image Editorial Pages</a></li>
                          <li class="<?php if($this->session->menu=='backend_editorial'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_editorial/editorialdescription">Editorial Description</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_findus'||$this->session->menu=='backend_picfindus'){echo('active');} ?>" href="javascript:;"  >
                          <i class="icon-map-marker"></i>
                          <span>Find Us</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_picfindus'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_findus/picturefindus">Image Find Us Pages</a></li>
                          <li class="<?php if($this->session->menu=='backend_findus'){echo('active');} ?>"><a href="<?php echo base_url();?>backend_findus/find">Manage Find Us</a></li>
                      </ul>
                  </li>

                  <li>
                      <a class="<?php if($this->session->menu=='backend_piccontact'){echo('active');} ?>" href="<?php echo base_url();?>backend_contact/picturecontact" >
                          <i class="icon-phone"></i>
                          <span>Image Contact Us Pages</span>
                      </a>
                  </li>

                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->