<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets_b/img/favicon.png">

    <title>Cognac Audry - Administrator</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets_b/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets_b/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets_b/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets_b/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets_b/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

      <?php echo form_open_multipart('user_auth/login_process','class="form-signin"');?>
          <?php 
           if($this->session->flashdata('true')){
         ?>
           <div class="alert alert-success"> 
             <?php  echo $this->session->flashdata('true'); ?>
            </div>
        <?php    
        }else if($this->session->flashdata('err')){
        ?>
         <div class = "alert alert-success">
           <?php echo $this->session->flashdata('err'); ?>
         </div>
        <?php } ?>
        <h2 class="form-signin-heading">sign in now</h2>
        
        <div class="login-wrap">
            <input type="text" class="form-control" placeholder="Email" name="email" id="email" required>
            <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
            <label class="checkbox">
                
                <span class="pull-right">
                    <a href="<?php echo base_url(); ?>user_auth/forgot"> Forgot Password?</a>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
           

        </div>

       

      </form>

    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets_b/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets_b/js/bootstrap.min.js"></script>


  </body>
</html>
