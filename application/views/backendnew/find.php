<!--main content start-->
<section id="main-content">
    <section class="wrapper">
      <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
                <li class="active">Find Us</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
      </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form <?php echo $title;?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                      <?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
                      <div class="adv-table">
                          <div class="clearfix">
                            <div class="btn-group pull-right">
                                <a  class="btn btn-info" href="<?php echo base_url();?>backend_findus/find_add">
                                    Add Country <i class="icon-plus"></i>
                                </a>
                            </div>
                          </div>
                        <div class="space15"></div> <br>
                        <table  class="display table table-bordered table-striped" id="example">
                          <thead>
                            <tr>
                              <th width="5%"> No</th>
                              <th width="40%"><i class="icon-bullhorn"></i> Country</th>
                              <th width="10%"><i class="icon-eye-open"></i> View & Edit</th>
                              <th width="10%"><i class="icon-trash"></i> Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php $i=0; foreach ($finds as $find) :  ?>
                          <tr class="gradeX">
                              <td><?php echo $i+=1; ?></td>
                              <td><?php echo $find['title'];?></td>
                              <td class="text-center"><a class="btn btn-round btn-success" title="view & edit" href="<?php echo site_url('backend_findus/find_edit/'.$find['id']); ?>" type="button"><i class="icon-eye-open"></i></a></td>
                              <td class="text-center"><a class="btn btn-round btn-danger" title="delete" href="<?php echo site_url('backend_findus/delete_contry/'.$find['id']); ?>" onclick="return confirm('Are you sure to delete <?php echo $find['title'];?>?')" type="button"><i class="icon-trash "></i></a></td>
                          </tr>
                          <?php endforeach; ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <th width="5%"> No</th>
                              <th width="40%"><i class="icon-bullhorn"></i> Country</th>
                              <th width="10%"><i class="icon-eye-open"></i> View & Edit</th>
                              <th width="10%"><i class="icon-trash"></i> Delete</th>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

