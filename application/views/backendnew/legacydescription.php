<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li class="active"><?php echo $title;?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form <?php echo $title;?>
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_legacy/legacydescription_update','class="form-horizontal tasi-form"','method="post"'); ?>
                    
							<div class="form-group">
                              	<label class="col-sm-2 col-sm-2 control-label"><strong>Title</strong></label>
                              	<div class="col-sm-10">
                              		<input type="hidden" class="form-control" name="id" value="<?php echo $legacy_content['id'];?>" id="title">
                                  	<input type="text"  class="form-control" name="title" value="<?php echo $legacy_content['title'];?>" id="title" required>
                              	</div>
                          	</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Short Description</strong></label>
								<div class="col-sm-10">
                                  	<input type="text"  class="form-control" name="short_description" value="<?php echo $legacy_content['short_description'];?>" id="short_description" required>
                              	</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Content</strong></label>
								<div class="col-lg-10">
									<textarea class="form-control ckeditor" id="editor1" name="content" rows="6" name="content" required ><?php echo $legacy_content['conten']; ?></textarea><br>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-success pull-right" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->

             