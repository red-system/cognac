<!--main content start-->
<section id="main-content">
    <section class="wrapper">
      <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
                <li class="active">News</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
      </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form <?php echo $title;?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                      <?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
                      <div class="adv-table">
                          <div class="clearfix">
                            <div class="btn-group pull-right">
                                <a  class="btn btn-info" href="<?php echo base_url();?>backend_legacy/team_add">
                                    Add Team <i class="icon-plus"></i>
                                </a>
                            </div>
                          </div>
                        <div class="space15"></div> <br>
                        <table  class="display table table-bordered table-striped" id="example">
                          <thead>
                            <tr>
                              <th width="5%"> No</th>
                              <th width="10%"><i class="icon-bullhorn"></i> Name</th>
                              <th width="15%"><i class="icon-pushpin"></i>Position</th>
                              <th width="9%"><i class="icon-picture"></i> Thumb Image</th>
                              <th width="11%"><i class="icon-eye-open"></i> View & Edit</th>
                              <th width="11%"><i class="icon-trash"></i> Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php $i=0; foreach ($teams as $team) :  ?>
                          <tr class="gradeX">
                              <td><?php echo $i+=1; ?></td>
                              <td><?php echo $team['title'];?></td>
                              <td><?php echo $team['short_description']; ?></td>
                              <td class="text-center"><img src="<?php echo base_url();?>assets/img/team/<?php echo $team['thumb_image'];?>" style="width:120px;height: 85px" /></td>
                              <td class="text-center"><a class="btn btn-round btn-success" title="view & edit" href="<?php echo site_url('backend_legacy/team_edit/'.$team['id']); ?>" type="button"><i class="icon-eye-open"></i></a></td>
                              <td class="text-center"><a class="btn btn-round btn-danger" title="delete" href="<?php echo site_url('backend_legacy/team_delete/'.$team['id']); ?>" onclick="return confirm('Are you sure to delete <?php echo $team['title'];?>?')" type="button"><i class="icon-trash "></i></a></td>
                          </tr>
                          <?php endforeach; ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <th width="5%"> No</th>
                              <th width="10%"><i class="icon-bullhorn"></i> Name</th>
                              <th width="15%"><i class="icon-pushpin"></i> Position</th>
                              <th width="9%"><i class="icon-picture"></i> Thumb Image</th>
                              <th width="11%"><i class="icon-eye-open"></i> View & Edit</th>
                              <th width="11%"><i class="icon-trash"></i> Delete</th>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

