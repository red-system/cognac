<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li><a href="<?php echo base_url();?>backend_legacy/team">Team</a></li>
					<li class="active">Add Team</li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Edit Team
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
		                   if($this->session->flashdata('true')){
		                 ?>
		                   <div class="alert alert-success"> 
		                     <?php  echo $this->session->flashdata('true'); ?>
		                    </div>
		                <?php    
		                }else if($this->session->flashdata('err')){
		                ?>
		                 <div class = "alert alert-success">
		                   <?php echo $this->session->flashdata('err'); ?>
		                 </div>
		                <?php } ?>
						<?php echo form_open_multipart('backend_legacy/team_updateprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Name</strong></label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" name="id" value="<?php echo $team['id'];?>" >
									<input type="text" name="title" class="form-control" required value="<?php echo $team['title'];?>" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Position</strong></label>
								<div class="col-sm-10">
									<input type="text" name="short_description" class="form-control" value="<?php echo $team['short_description'];?>" required >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Thumbnail</strong></label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 150px; height: 150px;">
											<img src="<?php echo base_url();?>assets/img/team/<?php echo $team['thumb_image'];?>" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="thumb_image" type="file" class="default"/>
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Facebook</strong></label>
								<div class="col-sm-10">
									<input type="text" name="facebook_link" class="form-control" value="<?php echo $team['facebook_link'];?>" >
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Twitter</strong></label>
								<div class="col-sm-10">
									<input type="text" name="twitter_link" class="form-control" value="<?php echo $team['twitter_link'];?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Google-plus</strong></label>
								<div class="col-sm-10">
									<input type="text" name="googleplus_link" class="form-control" value="<?php echo $team['googleplus_link'];?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Email</strong></label>
								<div class="col-sm-10">
									<input type="text" name="email" class="form-control" value="<?php echo $team['email'];?>">
								</div>
							</div>
                      
							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-default" title="view" href="<?php echo base_url();?>backend_legacy/team" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-success pull-right" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->

             