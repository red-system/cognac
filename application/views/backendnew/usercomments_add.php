<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li><a href="<?php echo base_url();?>backend_home/usercomment">User Comment</a></li>
					<li class="active">Add User Commnert</li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Add User Comment
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php echo form_open_multipart('backend_home/usercomment_addprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Title</strong></label>
								<div class="col-sm-10">
									<input type="text" name="title" class="form-control" required >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>User Name</strong></label>
								<div class="col-sm-10">
									<input type="text" name="short_description" class="form-control" required >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Comment</strong></label>
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" id="editor1" name="content" rows="6" required></textarea><br>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-default" title="view" href="<?php echo base_url();?>backend_home/usercomment" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-success pull-right" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->

             