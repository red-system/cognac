<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li class="active"><?php echo $title;?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form <?php echo $title;?>
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_home/update_descriptionhome','class="form-horizontal tasi-form"','method="post"'); ?>
                    
							<div class="form-group">
                              	<label class="col-sm-2 col-sm-2 control-label"><strong>Title</strong></label>
                              	<div class="col-sm-10">
                              		<input type="hidden" class="form-control" name="id" value="<?php echo $home_content['id'];?>" id="title">
                                  	<input type="text"  class="form-control" name="title" value="<?php echo $home_content['title'];?>" id="title" required>
                              	</div>
                          	</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Short Description</strong></label>
								<div class="col-sm-10">
                                  	<input type="text"  class="form-control" name="short_description" value="<?php echo $home_content['short_description'];?>" id="short_description" required>
                              	</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Content</strong></label>
								<div class="col-lg-10">
									<textarea class="form-control ckeditor" id="editor1" name="content" name="content" required ><?php echo $home_content['conten']; ?></textarea><br>
								</div>
							</div>

							<!-- <div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Meta Title</strong></label>
								<div class="col-sm-10">
                                  	<input type="text"  class="form-control" name="meta_title" value="<?php echo $home_content['meta_title'];?>" required>
                              	</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Meta Keyword</strong></label>
								<div class="col-sm-10">
                                  	<input type="text"  class="form-control" name="meta_keyword" value="<?php echo $home_content['meta_keyword'];?>" required>
                              	</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Meta Description</strong></label>
								<div class="col-sm-10">
                                  	<input type="text"  class="form-control" name="meta_description" value="<?php echo $home_content['meta_description'];?>" required>
                              	</div>
							</div>

							<div class="form-group">
                              	<label class="col-sm-2 control-label col-lg-2" for="inputSuccess"><strong>Published</strong></label>
                              	<div class="col-lg-10">
                                  	<select class="form-control m-bot15" id="published" name="published">
                                      	<option value="<?php echo $home_content['published'];?>"> <?php echo $home_content['published'];?> </option>
                                      	<option value="yes"> Yes </option>
              						  	<option value="no"> No </option>
                                  	</select>
                              	</div>
                         	</div>

                         	<div class="form-group">
                              	<label class="col-sm-2 control-label col-lg-2" for="inputSuccess"><strong>Lang</strong></label>
                              	<div class="col-lg-10">
                                  	<select class="form-control m-bot15" id="lang" name="lang">
                                      	<option value="<?php echo $home_content['lang'];?>"> <?php echo $home_content['lang'];?> </option>
                                      	<option value="eng"> Indonesia </option>
					                    <option value="eng"> English </option>
					                    <option value="other"> Other </option>
                                  	</select>
                              	</div>
                         	</div> -->

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-success pull-right" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->

             