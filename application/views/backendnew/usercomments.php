<!--main content start-->
<section id="main-content">
    <section class="wrapper">
      <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
                <li class="active">News</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
      </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form <?php echo $title;?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                      <?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
                      <div class="adv-table">
                          <div class="clearfix">
                            <div class="btn-group pull-right">
                                <a  class="btn btn-info" href="<?php echo base_url();?>backend_home/usercomment_add">
                                    Add User Comment <i class="icon-plus"></i>
                                </a>
                            </div>
                          </div>
                        <div class="space15"></div> <br>
                        <table  class="display table table-bordered table-striped" id="example">
                          <thead>
                            <tr>
                              <th width="5%"> No</th>
                              <th width="10%"><i class="icon-bullhorn"></i> Name</th>
                              <th width="15%"><i class="icon-question-sign"></i>Short Descrition</th>
                              <th width="24%"><i class="icon-comment"></i> Comment</th>
                              <th width="11%"><i class="icon-eye-open"></i> View & Edit</th>
                              <th width="11%"><i class="icon-trash"></i> Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php $i=0; foreach ($user_comments as $user_comment) :  ?>
                          <tr class="gradeX">
                              <td><?php echo $i+=1; ?></td>
                              <td><?php echo $user_comment['short_description']; ?></td>
                              <td><?php echo $user_comment['title'];?></td>
                              <td><?php echo word_limiter($user_comment['conten'],20); ?></td>
                              <td class="text-center"><a class="btn btn-round btn-success" title="view & edit" href="<?php echo site_url('backend_home/usercomment_edit/'.$user_comment['id']); ?>" type="button"><i class="icon-eye-open"></i></a></td>
                              <td class="text-center"><a class="btn btn-round btn-danger" title="delete" href="<?php echo site_url('backend_home/usercomment_delete/'.$user_comment['id']); ?>" onclick="return confirm('Are you sure to delete <?php echo $user_comment['title'];?>?')" type="button"><i class="icon-trash "></i></a></td>
                          </tr>
                          <?php endforeach; ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <th width="5%"> No</th>
                              <th width="10%"><i class="icon-bullhorn"></i> Name</th>
                              <th width="15%"><i class="icon-question-sign"></i>Short Descrition</th>
                              <th width="24%"><i class="icon-comment"></i> Comment</th>
                              <th width="11%"><i class="icon-eye-open"></i> View & Edit</th>
                              <th width="11%"><i class="icon-trash"></i> Delete</th>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

