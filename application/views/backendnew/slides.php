<!--main content start-->
<section id="main-content">
    <section class="wrapper">
      <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
                <li class="active">Slider</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
      </div>
        <!-- page start-->
        <div class="row">
      <div class="col-lg-12">
        <section class="panel">
              <header class="panel-heading">
                  <strong><?php echo $title;?></strong>
                  <span class="tools pull-right">
                  <a href="javascript:;" class="icon-chevron-down"></a> 
                  </span>
              </header>
              <div class="panel-body">
                <?php 
                   if($this->session->flashdata('true')){
                 ?>
                   <div class="alert alert-success"> 
                     <?php  echo $this->session->flashdata('true'); ?>
                    </div>
                <?php    
                }else if($this->session->flashdata('err')){
                ?>
                 <div class = "alert alert-success">
                   <?php echo $this->session->flashdata('err'); ?>
                 </div>
                <?php } ?>
                  <ul class="grid cs-style-3">
                    <?php foreach ($slides as $pic) :  ?>
                      <li>
                          <figure>
                              <img src="<?php echo base_url();?>assets/img/slides/<?php echo $pic['thumb_image'];?>" style="width:351.667px;height:198.033px" alt="">
                              
                              <figcaption>
                                  <a class="btn btn-danger btn-block" href="<?php echo base_url();?>backend_home/slides_delete/<?php echo $pic['id'];?>" onclick="return confirm('Are you sure to delete this picture?')" type="button"><i class="icon-trash"></i> Delete image</a> 
                              </figcaption>
                          </figure>
                      </li>
                    <?php endforeach;?>
                  </ul>
                <a type="button" class="btn btn-round btn-success btn-block" href="<?php echo base_url();?>backend_home/slides_add">Add Slides <i class="icon-plus"></i></a>
              </div>
          </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

