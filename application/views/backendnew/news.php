<!--main content start-->
<section id="main-content">
    <section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li class="active">News</li>
				</ul>
            <!--breadcrumbs end -->
			</div>
		</div>
    <?php 
       if($this->session->flashdata('true')){
     ?>
       <div class="alert alert-success"> 
       <?php  echo $this->session->flashdata('true'); ?>
      </div>
    <?php    
    }else if($this->session->flashdata('err')){
    ?>
     <div class = "alert alert-success">
       <?php echo $this->session->flashdata('err'); ?>
     </div>
    <?php } ?>
        <div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Form Image News Pages
						<span class="tools pull-right">
							<a href="javascript:;" class="icon-chevron-down"></a> 
            </span>
					</header>
					<div class="panel-body">
						
						<?php echo form_open_multipart('backend_home/picturenews_update','class="form-horizontal tasi-form"'); ?>
						<input type="hidden"  class="form-control" name="id" value="<?php echo $news_content['id'];?>" id="title" required>
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Image</strong></label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 190px; height: 106.85px;">
											<img src="<?php echo base_url();?>assets/img/imagepage/<?php echo $news_content['thumb_image'];?>" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="thumb_image" type="file" class="default" />
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
							
						</form>
					</div>
				</section>
			</div>
		</div>
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
              <section class="panel">
                  <header class="panel-heading">
                      Form <?php echo $title;?>
                      <span class="tools pull-right">
                        <a href="javascript:;" class="icon-chevron-down"></a> 
                      </span>
                  </header>
                  <div class="panel-body">
                    
                    
                    <div class="adv-table">
                        <div class="clearfix">
                          <div class="btn-group pull-right">
                              <a  class="btn btn-info" href="<?php echo base_url();?>backend_home/news_add">
                                  Add News <i class="icon-plus"></i>
                              </a>
                          </div>
                        </div>
                      <div class="space15"></div> <br>
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th width="5%"> No</th>
                            <th width="10%"><i class="icon-bullhorn"></i> Title</th>
                            <th width="15%"><i class="icon-question-sign"></i>Short Descrition</th>
                            <th width="24%"><i class="icon-reorder"></i> Content</th>
                            <th width="9%"><i class="icon-picture"></i> Thumb Image</th>
                            <th width="11%"><i class="icon-eye-open"></i> View & Edit</th>
                            <th width="11%"><i class="icon-trash"></i> Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php $i=0; foreach ($newss as $news) :  ?>
                        <tr class="gradeX">
                            <td><?php echo $i+=1; ?></td>
                            <td><?php echo $news['title'];?></td>
                            <td><?php echo $news['short_description']; ?></td>
                            <td><?php echo word_limiter($news['conten'],20); ?></td>
                            <td><img src="<?php echo base_url();?>assets/img/blog/<?php echo $news['thumb_image'];?>" style="width:120px;height: 85px" /></td>
                            <td class="text-center"><a class="btn btn-round btn-success" title="view & edit" href="<?php echo site_url('backend_home/news_edit/'.$news['id']); ?>" type="button"><i class="icon-eye-open"></i></a></td>
                            <td class="text-center"><a class="btn btn-round btn-danger" title="delete" href="<?php echo site_url('backend_home/news_delete/'.$news['id']); ?>" onclick="return confirm('Are you sure to delete <?php echo $news['title'];?>?')" type="button"><i class="icon-trash "></i></a></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th width="5%"> No</th>
                            <th width="10%"><i class="icon-bullhorn"></i> Title</th>
                            <th width="15%"><i class="icon-question-sign"></i>Short Descrition</th>
                            <th width="24%"><i class="icon-reorder"></i> Content</th>
                            <th width="9%"><i class="icon-picture"></i> Thumb Image</th>
                            <th width="11%"><i class="icon-eye-open"></i> View & Edit</th>
                            <th width="11%"><i class="icon-trash"></i> Delete</th>
                        </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
              </section>
          </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

