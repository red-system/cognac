<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-home"></i> Home</a></li>
					<li><a href="<?php echo base_url();?>backend_ourcognac/ourcognac">Our Cognac</a></li>
					<li class="active">Add Collection Cognac</li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Add Collection Cognac
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_ourcognac/ourcognac_updateprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    
							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Title</strong></label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" name="id" value="<?php echo $ourcognac['id'];?>" >
									<input type="text" name="title" class="form-control" value="<?php echo $ourcognac['title'];?>" required >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Thumbnail</strong></label>
								<div class="col-md-10">
									<div class="fileupload fileupload-new" data-provides="fileupload">
										<div class="fileupload-new thumbnail" style="width: 250px; height: 150px;">
											<img src="<?php echo base_url();?>assets/img/collection/<?php echo $ourcognac['thumb_image'];?>" alt="" />
										</div>
										<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
												<span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
												<input name="thumb_image" type="file" class="default"/>
											</span>
											<a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Short Description</strong></label>
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" name="short_description" rows="6" required><?php echo $ourcognac['short_description'];?></textarea><br>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label col-lg-2" ><strong>Description</strong></label>
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" id="editor1" name="content" rows="6" required><?php echo $ourcognac['conten'];?></textarea><br>
								</div>
							</div>

							<div class="form-group ">
		                        <label class="col-sm-2 control-label col-lg-2" ><strong>Rating</strong></label>
		                        <div class="col-sm-10">
									<div class="star-rating" style="font-size:2em;padding-left:0px;">
										<span class="fa fa-star-o" data-rating="1"></span>
										<span class="fa fa-star-o" data-rating="2"></span>
										<span class="fa fa-star-o" data-rating="3"></span>
										<span class="fa fa-star-o" data-rating="4"></span>
										<span class="fa fa-star-o" data-rating="5"></span>
										<input type="hidden" id="rate" name="rating" class="rating-value" value="<?php echo $ourcognac['rating'];?>">
									</div>
								</div>
		                    </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label col-lg-2" ><strong>Reviews</strong></label>
                                <div class="col-sm-10">
                                    <input type="text" name="reviews" class="form-control" value="<?php echo $ourcognac['reviews'];?>"  >
                                </div>
                            </div>

							<div class="form-group">
                                <label class="col-sm-2 control-label col-lg-2" ><strong>Meta Title</strong></label>
                                <div class="col-sm-10">
                                    <input name="meta_title" id="tagsinput" class="tagsinput" value="<?php echo $ourcognac['meta_title'];?>"  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label col-lg-2" ><strong>Meta Keyword</strong></label>
                                <div class="col-sm-10">
                                    <input name="meta_keyword" id="tagsinput" class="tagsinput" value="<?php echo $ourcognac['meta_keyword'];?>"  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label col-lg-2" ><strong>Meta Description</strong></label>
                                <div class="col-sm-10">
                                    <input name="meta_description" id="tagsinput" class="tagsinput" value="<?php echo $ourcognac['meta_description'];?>"  />
                                </div>
                            </div>

							<div class="form-group">
	                            <label class="col-sm-2 control-label col-lg-2" ><strong>Published</strong></label>
	                            <div class="col-lg-10">
	                                <select class="form-control" id="published" name="published">
				                      	<option value="<?php echo $ourcognac['published'];?>"> <?php echo $ourcognac['published'];?> </option>
				                      	<option value="yes"> Yes </option>
				                      	<option value="no"> No </option>
				                    </select>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label class="col-sm-2 control-label col-lg-2" ><strong>Lang</strong></label>
	                            <div class="col-lg-10">
	                                <select class="form-control" id="lang" name="lang">
				                      	<option value="<?php echo $ourcognac['lang'];?>"> <?php echo $ourcognac['lang'];?> </option>
				                      	<option value="id"> Indonesia </option>
				                      	<option value="eng"> English </option>
				                      	<option value="other"> Other </option>
				                    </select>
	                            </div>
	                        </div>
                      
							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-default" title="view" href="<?php echo base_url();?>backend_ourcognac/ourcognac" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-success pull-right" type="submit" name="action">Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
              <header class="panel-heading">
                  <strong>Picture Gallery</strong>
                  <span class="tools pull-right">
                	<a href="javascript:;" class="icon-chevron-down"></a>	
              	  </span>
              </header>
              <div class="panel-body">
                  <ul class="grid cs-style-3">
                  	<?php foreach ($ourcognacpic as $pic):?>
                      <li>
                          <figure>
                              <img src="<?php echo base_url();?>assets/img/collection/<?php echo $pic['image'];?>" style="width:351.667px;height:198.033px" alt="">
                              
                              <figcaption>
                                  <a class="btn btn-danger btn-block" href="<?php echo base_url();?>backend_ourcognac/picourcognac_delete/<?php echo $pic['id'];?>" onclick="return confirm('Are you sure to delete this picture?')" type="button"><i class="icon-trash"></i> Delete image</a> 
                              </figcaption>
                          </figure>
                      </li>
                    <?php endforeach;?>
                  </ul>
                <a type="button" class="btn btn-round btn-default btn-block" href="<?php echo base_url();?>backend_ourcognac/picourcognac_add/<?php echo $ourcognac['id'];?>">Add Picture <i class="icon-plus"></i></a>
              </div>
          </section>
            </div>
        </div>
	</section>
</section>
<!--main content start-->



             