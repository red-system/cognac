<section class="section-wrap post-single pb-50">
	<div class="container relative">
		<div class="row">
			
			<!-- content -->
			<div class="col-md-12 post-content">

				<!-- gallery post -->
				<div class="entry-item">
					<div class="text-center">
						<div class="entry-slider">
							<div class="flexslider" id="flexslider">
								<ul class="slides clearfix">
									<li>
										<a>
											<img src="<?php echo base_url(); ?>assets/img/blog/<?php echo $news_details['thumb_image'];?>" alt="" style="width:75%;">
										</a>
									</li>
									
								</ul>
							</div>
						</div> <!-- end slider -->
					</div>

					<div class="entry">					
						<h2 class="entry-title">
							<?php echo $news_details['title'];?>
						</h2>
						<ul class="entry-meta list-inline">
							<li class="entry-date">
								<i class="fa fa-clock-o"></i><a><?php echo $news_details['updated_at'];?></a>
							</li>
						</ul>
						<div class="entry-content">
							<p><?php echo $news_details['conten'];?></p>

							<!-- tags -->
							<div class="entry-tags tags mb-30 mt-40 clearfix">
								<span class="uppercase left">Tags:</span>
								<span><?php echo $news_details['meta_title'];?></span>
								<span><?php echo $news_details['meta_keyword'];?></span>
								<span><?php echo $news_details['meta_description'];?></span>
							</div>

							<!-- entry share -->
							<div class="entry-share">
								<div class="socials-share clearfix">
									<span class="uppercase">Share:</span>
									<div class="social-icons colored">
										<a href="https://twitter.com/share?url=http://www.audrycognac.com/news-details/<?php echo $news_details['slug'];?>" class="social-twitter" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
										<a href="https://www.facebook.com/sharer.php?p[url]=http://www.audrycognac.com/news-details/<?php echo $news_details['slug'];?>" class="social-facebook" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
										<a href="https://plus.google.com/share?url=http://www.audrycognac.com/news-details/<?php echo $news_details['slug'];?>" class="social-google-plus" data-toggle="tooltip" data-placement="top" title="Google +"><i class="fa fa-google-plus"></i></a>
										<a href="https://www.linkedin.com/shareArticle?mini=true&url=http://www.audrycognac.com/news-details/<?php echo $news_details['slug'];?>" class="social-linkedin" data-toggle="tooltip" data-placement="top" title="Email"><i class="fa fa-linkedin"></i></a>
									</div>
								</div>
							</div>

							<!-- related posts -->
							<div class="related-posts mt-40">
								<div class="related-posts mt-12">
									<h3 class="heading relative heading-small uppercase bottom-line style-2 left-align mb-30">Other News
										
										<a href="<?php echo base_url();?>news" class="btn btn-sm btn-olive right ">View All News</a>
									</h3>
								<div class="related-posts mt-40">
									<div class="row">
										<?php foreach ($news_others as $news) :  ?>
											<article class="col-sm-4 col-xs-12 mb-20">
												<div class="entry-img">
													<a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>" class="hover-scale">
														<img style="height: 174.567px" src="<?php echo base_url(); ?>assets/img/blog/<?php echo $news['thumb_image'];?>" alt="">
													</a>
												</div>
												<div class="entry">
													<h4 class="entry-title"><a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>"><?php echo $news['title'];?></a></h4>
													<div class="entry-content">
														<a href="<?php echo base_url();?>news-details/<?php echo $news['slug'];?>" class="read-more dark-link">Read More <i class="fa fa-angle-right"></i></a>
													</div>
												</div>
											</article> <!-- end post -->
										<?php endforeach; ?>
									</div>
								</div>

							
						</div>
					</div>
				</div> <!-- end gallery post -->

			</div> <!-- end col -->

			
			

		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- end blog standard -->