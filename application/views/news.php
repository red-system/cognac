<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $news_image['thumb_image'];?>);">
  <div class="container relative clearfix">
    <div class="title-holder">
      <div class="title-text">
        <h1 class="uppercase">Our News</h1>
        <ol class="breadcrumb">
          <li>
            <a href="<?php echo base_url();?>home">Home</a>
          </li>
          <li class="active">
            News
          </li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Latest news -->
          <section class="section-wrap mag-categories relative pt-0 pb-50">
            <div class="container">
              <div class="row mt-40">

                <div class="col-md-12 content mb-50">
                  <h2 class="heading relative heading-small uppercase bottom-line style-2 left-align">Latest news</h2>

                  <div class="row">
                  <?php foreach ($data->result() as $news) :  ?>
                    <div class="col-sm-3">
                      <article>
                        <div class="entry-img">
                          <a href="<?php echo base_url();?>news-details/<?php echo $news->slug;?>" class="hover-scale">
                            <img style="height: 174.567px" src="<?php echo base_url(); ?>assets/img/blog/<?php echo $news->thumb_image;?>" alt="">
                          </a>
                        </div>
                        <div class="entry">
                          <h2 class="entry-title"><a href="<?php echo base_url();?>news-details/<?php echo $news->slug;?>"><?php echo $news->title;?></a></h2>
                          
                          <div class="entry-content">
                      <a href="<?php echo base_url();?>news-details/<?php echo $news->slug;?>" class="read-more dark-link">Read More <i class="fa fa-angle-right"></i></a>
                    </div>
                        </div>
                      </article>

                    </div> <!-- end col -->
                    <?php endforeach; ?>
                  </div> <!-- end row -->

                 
                  <!-- Pagination -->
                  <div class="text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Tampilkan pagination-->
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                  </div>
                  <!-- <nav class="pagination clear">
                    <a href="#"><i class="fa fa-angle-left"></i></a>
                    <span class="page-numbers current">1</span>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                    <a href="#"><i class="fa fa-angle-right"></i></a>
                  </nav> -->

                </div> <!-- end content col -->
                  
                
              </div> <!-- end content -->
            </div> <!-- end main container -->            
          </section> <!-- end latest news -->