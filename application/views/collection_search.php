<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $ourcognac['thumb_image'];?>);">
  <div class="container relative clearfix">
    <div class="title-holder">
      <div class="title-text">
        <h1 class="uppercase">Our Cognac</h1>
        <ol class="breadcrumb">
          <li>
            <a href="<?php echo base_url();?>home">Home</a>
          </li>
          <li class="active">
            Our Cognac
          </li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="section-wrap-small new-arrivals pb-20">
  <div class="container">

    <h2 class="heading relative heading-small uppercase bottom-line style-2 left-align">Result</h2>

    <div class="row items-grid">
      <?php foreach ($data->result() as $collection) :  ?>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="product-item">
          <div class="product-img hover-1">
            <a href="#">
              <img style="width:262.5px;height:382.233px" src="<?php echo base_url(); ?>assets/img/collection/<?php echo $collection->thumb_image;?>" alt="">
              <img style="width:262.5px;height:382.233px" src="<?php echo base_url(); ?>assets/img/collection/<?php echo $collection->thumb_image;?>" alt="" class="back-img"
            </a>
            <div class="product-label">
              <span class="sale">Cognac Audry</span>
            </div>
            <div class="hover-overlay">
              <div class="product-add-to-wishlist">
                <a href="<?php echo base_url();?>cognac-details/<?php echo $collection->slug;?>">
              </div>
              <div class="product-actions">
                <a href="<?php echo base_url();?>cognac-details/<?php echo $collection->slug;?>" class="btn btn-dark btn-md">More Details</a>
              </div>
              <div class="product-quickview">
                <a href="<?php echo base_url();?>cognac-details/<?php echo $collection->slug;?>" class="btn-quickview">Quick View</a>
              </div>
            </div>
          </div>
          <div class="product-details">
            <h3>
              <a class="product-title" href="<?php echo base_url();?>cognac-details/<?php echo $collection->slug;?>"><?php echo $collection->title;?></a>
            </h3>
          </div>
        </div>
      </div>
      <?php endforeach; ?>


    </div> <!-- end row -->
    <div  class="text-center">
      <a href="<?php echo base_url();?>cognac" class="btn btn-md btn-color rounded text-center">View All Collection</a>
    </div>
    <!-- Pagination -->
                  <div class="text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Tampilkan pagination-->
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                  </div>
  </div>
</section>