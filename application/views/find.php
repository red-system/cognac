<section class="page-title text-center" style="background-image: url(<?php echo base_url();?>assets/img/imagepage/<?php echo $findus['thumb_image'];?>);">
	<div class="container relative clearfix">
		<div class="title-holder">
			<div class="title-text">
				<h1 class="uppercase">Find Us</h1>
        <ol class="breadcrumb">
          <li>
            <a href="<?php echo base_url();?>home">Home</a>
          </li>
          <li class="active">
            Find Us
          </li>
        </ol>
			</div>
		</div>
	</div>
</section>
		<!-- F.A.Q. -->
		<section class="section-wrap faq">
			<div class="container">
				<div class="row">

					<div class="col-md-8 mb-50">
						<h1 class="widget-title heading relative heading-small uppercase bottom-line style-2 left-align">Select Country</h1>
						<!-- Toggles -->

						<div class="col-md-12 mb-50">

						<!-- Toggles -->
						<?php foreach ($country as $file) : ?>
						<div class="toggle">
							<div class="acc-panel">
								<a href="#" class=""><h1><i class="icon-target"></i>&nbsp <?php echo $file['title'];?></h1></a>
							</div>
							<div class="panel-content" style="display: none;">
									select distributor . . .
									<?php foreach ($pts[$file['id']] as $pt) :  ?>
									<div class="toggle">
										<div class="acc-panel">
											<a href="#" class=""><h3><i class="icon-profile-male"></i>&nbsp <?php echo $pt['title']; ?></h3></a>
										</div>
										<div class="panel-content" style="display: none;">
											<div class="alert alert-success fade in alert-dismissible" role="alert">

												<i class="icon-wine"></i>&nbsp Address <h4><?php echo $pt['short_description']; ?></h4>
											</div>
										</div>

									</div>
									<?php endforeach; ?>
							</div>

						</div> <!-- end toggle -->
						<?php endforeach; ?>


					</div>



					</div> <!-- end col -->

					<aside class="col-md-3 col-md-offset-1 sidebar">


						<div class="widget">

							<ul>
								<h5 class="uppercase">Information</h5>

								<div class="contact-item">
									<div class="contact-icon">
										<i class="icon icon_house_alt"></i>
									</div>
									<?php echo $address['short_description'];?>
									<div class="contact-icon">
										<i class="fa fa-phone"></i>
									</div>
									<a href="tel:<?php echo $phone1['short_description'];?>"><?php echo $phone1['short_description'];?></a><br>
									<a href="tel:<?php echo $phone2['short_description'];?>"><?php echo $phone2['short_description'];?></a>
									<div class="contact-icon">
										<i class="fa fa-envelope"></i>
									</div>
									<a href="mailto:<?php echo $email['short_description'];?>"><?php echo $email['short_description'];?></a>
								</div> <!-- end address -->
							</ul>
						</div> <!-- end information -->

					</aside>

				</div>
			</div>
		</section> <!-- end faq -->

		<!-- Call to Action -->



		
