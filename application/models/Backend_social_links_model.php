<?php
class Backend_social_links_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_count_by_position($position){
    $query = $this->db->query("SELECT * FROM article where position = '$position'");
    return $query->num_rows();
  }

  public function get_social_links_by_name($social_media =  FALSE){
      if($social_media === FALSE){
        $query = $this->db->get('social_links');
        return $query->result_array();
      }
      $query = $this->db->get_where('social_links', array('social_media' => $social_media));
      return $query->row_array();
  }

  public function update_social_link($additional_data)
  {
    $social_media = $this->input->post('social_media');
    $data  = array('link' => $this->input->post('link'),
      'published' => $this->input->post('published')
   );
   $this->db->where('social_media', $social_media);
   return $this->db->update('social_links', $data);
  }


}
