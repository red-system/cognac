<?php
class User_auth_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }
  public function getUserInfoByEmail($email)
  {
      $q = $this->db->get_where('users', array('email' => $email));  
      if($this->db->affected_rows() > 0){
          $row = $q->row();
          return $row;
      }else{
          error_log('no user found getUserInfo('.$email.')');
          return false;
      }
  }
   
  public function getUserInfo($id)
  {
      $q = $this->db->get_where('users', array('id' => $id), 1);  
      if($this->db->affected_rows() > 0){
          $row = $q->row();
          return $row;
      }else{
          error_log('no user found getUserInfo('.$id.')');
          return false;
      }
  }
   
   public function insertToken($user_id)  
   {    
      $token = substr(sha1(rand()), 0, 30);
      $date = date('Y-m-d');   
       
      $string = array(  
         'remember_token'=> $token,
         'created_token'=>$date  
      );

      $where = "id = 1 ";
  
      $query = $this->db->update_string('users',$string , $where);
      $this->db->query($query);  
      return $token . $user_id;  
       
   }  
   
   public function isTokenValid($token)  
   {  
     $tkn = substr($token,0,30);  
     $uid = substr($token,30);     
       
     $q = $this->db->get_where('users', array(  
       'users.remember_token' => $tkn,   
       'users.id' => $uid), 1);               
           
     if($this->db->affected_rows() > 0){  
       $row = $q->row();         
         
       $created = $row->created_token;  
       $createdTS = strtotime($created);  
       $today = date('Y-m-d');   
       $todayTS = strtotime($today);  
         
       if($createdTS != $todayTS){  
         return false;  
       }  
         
       $user_info = $this->getUserInfo($row->id);  
       return $user_info;  
         
     }else{  
       return false;  
     }  
       
   }      
   
   public function updatePassword($post)
    {   
        $this->db->where('id', $post['id']);  
        $this->db->update('users', array('password' => $post['password']));  
        $success = $this->db->affected_rows(); 
        
        if(!$success){
            error_log('Unable to updatePassword('.$post['id'].')');
            return false;
        }        
        return true;
    } 
} 