<?php
class Backend_slides_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_slider($slug = FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article',array('position' => 'slider'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }


  public function add_sliderimage($data){
   return $this->db->insert('article',$data);
  }

  public function add_slider_image($add_data){
    $slug = url_title($this->input->post('title'));
    $thumb_image = $add_data['file_name'];
    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'thumb_image' => $thumb_image,
        'short_description' => $this->input->post('short_description'),
        'conten' => $this->input->post('content'),
        'meta_title' => $this->input->post('meta_title'),
        'meta_keyword' => $this->input->post('meta_keyword'),
        'meta_description' => $this->input->post('meta_description'),
        'position' => 'slider',
        'published' => $this->input->post('published'),
        'lang' => $this->input->post('lang'),
        'created_at' =>date('Y-m-d H:i:s')
    );
      return $this->db->insert('article',$data);


  }

  public function get_posts_by_id($id = FALSE){
      if($id === FALSE){
        $query = $this->db->get('article');
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('id' => $id));
      return $query->row_array();
  }

  public function set_article($add_data){
    $slug = url_title($this->input->post('title'));
    $thumb_image = $add_data['file_name'];
    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'thumb_image' => $thumb_image,
        'short_description' => $this->input->post('short_description'),
        'conten' => $this->input->post('content')
    );

    return $this->db->insert('article',$data);
  }


  public function add_article_by_ajax($data_passed){
    $title = $data_passed['title'];
    $slug = url_title($title);
    $short_description = $data_passed['short_description'];
    $data = array(
        'title' => $title,
        'slug' => $slug,
        'short_description' => $this->input->post('short_description')
    );

    return $this->db->insert('article',$data);

  }

  public function delete_article($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }

  public function update_article(){
    $id = $this->input->post('id');
    $slug = url_title($this->input->post('title'));
    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug
    );
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

}
