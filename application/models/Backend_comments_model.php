<?php
class Backend_comments_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_count_by_position($position){
    $query = $this->db->query("SELECT * FROM article where position = '$position'");
    return $query->num_rows();
  }

  public function get_collumn_items($slug =  FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article',array('position' => 'collumns-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_comments_without_pagination($slug = FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article', array('position'=>'user-comments'));
        //$this->db->limit(2, 0);
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_comment_by_id($id)
  {
    $query = $this->db->get_where('article', array('id' => $id));
    return $query->row_array();
  }


  public function get_collumn_items_by_pagination($data){
        $result_array['active_row_controller'] = $data['active_row_controller'];
        $result_array['row_per_page'] = $data['row_per_page'];
        $result_array['row_start'] = $data['row_start'];
        $this->db->limit($result_array['row_per_page'], $result_array['row_start']);
        $query = $this->db->get_where('article',array('position' => 'collumns-item'));
        $result_array['result_array'] = $query->result_array();
        return $result_array;
  }

  public function add_comments_item($add_data){
    $slug = url_title($this->input->post('title'));
    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'short_description' => $this->input->post('short_description'),
        'conten' => $this->input->post('content'),
        'meta_title' => $this->input->post('meta_title'),
        'meta_keyword' => $this->input->post('meta_keyword'),
        'meta_description' => $this->input->post('meta_description'),
        'position' => 'user-comments',
        'published' => $this->input->post('published'),
        'lang' => $this->input->post('lang'),
        'created_at' =>date('Y-m-d H:i:s')
    );

    return $this->db->insert('article',$data);
  }

  public function add_comments(){
    $data = array(
        'title' => $this->input->post('title'),
        'short_description' => $this->input->post('short_description'),
        'conten' => $this->input->post('content'),
        'position' => 'user-comments',
        'created_at' =>date('Y-m-d H:i:s')
    );

    return $this->db->insert('article',$data);
  }

  public function update_comment_item($additional_data){
    $id = $this->input->post('id');
    $slug = url_title($this->input->post('title'));

    $data = array(
      'title' => $this->input->post('title'),
      'slug' => $slug,
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'meta_title' => $this->input->post('meta_title'),
      'meta_keyword' => $this->input->post('meta_keyword'),
      'meta_description' => $this->input->post('meta_description'),
      'published' => $this->input->post('published'),
      'lang' => $this->input->post('lang'),
      'updated_at' =>date('Y-m-d H:i:s')
    );
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function update_comment(){
    $id = $this->input->post('id');
    
    $data = array(
      'title' => $this->input->post('title'),
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'updated_at' =>date('Y-m-d H:i:s')
    );
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function delete_user_comment($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }
}
