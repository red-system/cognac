<?php
class Backend_profile_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_num_user_rows($slug =  FALSE){
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where('email', $email);
    $this->db->where('password', $password);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function get_profile($slug)
  {
    $query = $this->db->get_where('users', array('type' => $slug));
    return $query->row_array();
  }

  public function update_profile($additional_data = FALSE){
   $id = $this->input->post('id');
    
   if(isset($additional_data['image_name'])){
      $data = array(
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'password' => $this->input->post('password'),
        'image' => $additional_data['image_name'],
        'updated_at' =>date('Y-m-d H:i:s')
      );
    } else {
      $data = array(
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'password' => $this->input->post('password'),
        'updated_at' =>date('Y-m-d H:i:s')  
      );
    }
    $this->db->where('id', $id);
    return $this->db->update('users', $data);
  }

  public function update_profilepass($slug = FALSE){
    $id = $this->input->post('id');
    
    $data = array(
      'name' => $this->input->post('name'),
      'email' => $this->input->post('email'),
      'password' => $this->input->post('password'),
      'updated_at' =>date('Y-m-d H:i:s') 
    );

    $this->db->where('id', $id);
    return $this->db->update('users', $data);
  }

  
}
