<?php
class Backend_home_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function update_short_description($slug =  FALSE){
    $data = array(
      'short_description' => $this->input->post('short_description')
    );

    $this->db->where('title','short_description');
    return $this->db->update('article',$data);
  }

  public function update_address($slug =  FALSE){
     $data = array(
      'short_description' => $this->input->post('address')
    );

    $this->db->where('title','address');
    return $this->db->update('article',$data);
  }

  public function update_phone1($slug =  FALSE){
    $data = array(
      'short_description' => $this->input->post('phone1')
    );

    $this->db->where('title','phone1');
    return $this->db->update('article',$data);
  }

  public function update_phone2($slug =  FALSE){
    $data = array(
      'short_description' => $this->input->post('phone2')
    );

    $this->db->where('title','phone2');
    return $this->db->update('article',$data);
  }

  public function update_email($slug =  FALSE){
    $data = array(
      'short_description' => $this->input->post('email')
    );

    $this->db->where('title','email');
    return $this->db->update('article',$data);
  }

  public function update_facebook($slug =  FALSE){
    $data = array(
      'link' => $this->input->post('facebook'),
      'published' => $this->input->post('facebookpublished')
    );

    $this->db->where('social_media','Facebook');
    return $this->db->update('social_links',$data);
  }

  public function update_googleplus($slug =  FALSE){
    $data = array(
      'link' => $this->input->post('googleplus'),
      'published' => $this->input->post('googlepluspublished')
    );

    $this->db->where('social_media','Google-plus');
    return $this->db->update('social_links',$data);
  }

  public function update_linkedin($slug =  FALSE){
    $data = array(
      'link' => $this->input->post('linkedin'),
      'published' => $this->input->post('linkedinpublished')
    );

    $this->db->where('social_media','Linkedin');
    return $this->db->update('social_links',$data);
  }

  public function update_instagram($slug =  FALSE){
    $data = array(
      'link' => $this->input->post('instagram'),
      'published' => $this->input->post('instagrampublished')
    );

    $this->db->where('social_media','Instagram');
    return $this->db->update('social_links',$data);
  }

  public function update_twitter($slug =  FALSE){
    $data = array(
      'link' => $this->input->post('twitter'),
      'published' => $this->input->post('twitterpublished')
    );

    $this->db->where('social_media','Twitter');
    return $this->db->update('social_links',$data);
  }

  public function update_youtube($slug =  FALSE){
    $data = array(
      'link' => $this->input->post('youtube'),
      'published' => $this->input->post('youtubepublished')
    );

    $this->db->where('social_media','Youtube');
    return $this->db->update('social_links',$data);
  }

  public function update_logo($additional_data){
    $data  = array(
       'thumb_image'      => $additional_data['file_name']
    );
   
    $this->db->where('title','logo');
    return $this->db->update('article',$data);
  }

  public function get_home_content_by_position($position = FALSE){
      if($position === FALSE){
        $query = $this->db->get('article');
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('position' => $position));
      return $query->row_array();
  }

  public function update_homecontent($slug = FALSE){
    $id = $this->input->post('id');
    
    $data = array(
      'title' => $this->input->post('title'),
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'updated_at' =>date('Y-m-d H:i:s')
    );

    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function update_legacydescription()
  {
    $id = $this->input->post('id');
    
    $data  = array(
       'title' => $this->input->post('title'),
       'short_description' => $this->input->post('short_description'),
       'conten' => $this->input->post('content'),
       'meta_title' => $this->input->post('meta_title'),
       'meta_keyword' => $this->input->post('meta_keyword'),
       'meta_description' => $this->input->post('meta_description'),
       'published' => $this->input->post('published'),
       'lang' => $this->input->post('lang'),
       'updated_at' =>date('Y-m-d H:i:s')
      );
    
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function update_editorialdescription()
  {
    $id = $this->input->post('id');
    
    $data  = array(
       'title' => $this->input->post('title'),
       'short_description' => $this->input->post('short_description'),
       'conten' => $this->input->post('content'),
       'meta_title' => $this->input->post('meta_title'),
       'meta_keyword' => $this->input->post('meta_keyword'),
       'meta_description' => $this->input->post('meta_description'),
       'published' => $this->input->post('published'),
       'lang' => $this->input->post('lang'),
       'updated_at' =>date('Y-m-d H:i:s')
      );
    
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function update_pageimage($additional_data){
    $id = $this->input->post('id');
    $data  = array(
       'thumb_image'      => $additional_data['file_name']
    );
   
    $this->db->where('id', $id);
    return $this->db->update('article',$data);
  }

  public function update_home_content($additional_data){
    $id = $this->input->post('id');
    $slug = url_title($this->input->post('title'));

    $data = array(
      'title' => $this->input->post('title'),
      'slug' => $slug,
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'meta_title' => $this->input->post('meta_title'),
      'meta_keyword' => $this->input->post('meta_keyword'),
      'meta_description' => $this->input->post('meta_description'),
      'published' => $this->input->post('published'),
      'lang' => $this->input->post('lang'),
      'updated_at' =>date('Y-m-d H:i:s')
    );

    if(isset($additional_data['image_name'])){
      $data = array(
        'thumb_image' => $additional_data['image_name']
      );
    }
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function delete_slider($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }

}
