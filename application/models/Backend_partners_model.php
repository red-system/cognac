<?php
class Backend_partners_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_count_by_position($position){
    $query = $this->db->query("SELECT * FROM article where position = '$position'");
    return $query->num_rows();
  }

  public function get_partners_items($slug =  FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article',array('position' => 'partners-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_partners_without_pagination($slug = FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article', array('position'=>'partners-item'));
        //$this->db->limit(2, 0);
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_partners_by_id($id)
  {
    $query = $this->db->get_where('article', array('id' => $id));
    return $query->row_array();
  }


  public function get_partners_items_by_pagination($data){
        $result_array['active_row_controller'] = $data['active_row_controller'];
        $result_array['row_per_page'] = $data['row_per_page'];
        $result_array['row_start'] = $data['row_start'];
        $this->db->limit($result_array['row_per_page'], $result_array['row_start']);
        $query = $this->db->get_where('article',array('position' => 'collumns-item'));
        $result_array['result_array'] = $query->result_array();
        return $result_array;
  }

  public function add_partners_item($add_data){
    $thumb_image = $add_data['file_name'];
    $data = array(
        'title' => $this->input->post('title'),
        'thumb_image' => $thumb_image,
        'position' => 'partners-item',
        'created_at' =>date('Y-m-d H:i:s')
    );
    return $this->db->insert('article',$data);
  }

  public function update_partners($additional_data){
    $id = $this->input->post('id');
    
   if(isset($additional_data['image_name'])){
      $data = array(
        'title' => $this->input->post('title'),
        'updated_at' =>date('Y-m-d H:i:s'),
        'thumb_image' => $additional_data['image_name']
      );
    } else {
      $data = array(
        'title' => $this->input->post('title'),
        'updated_at' =>date('Y-m-d H:i:s')  
      );
    }
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function delete_partners($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }
}
