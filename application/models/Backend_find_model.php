<?php
class Backend_find_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_count_by_position($position){
    $query = $this->db->query("SELECT * FROM article where position = '$position'");
    return $query->num_rows();
  }

  public function get_find_items($slug =  FALSE){
    if($slug === FALSE){
      $query = $this->db->get_where('article',array('position' => 'find-item'));
      return $query->result_array();
    }
    $query = $this->db->get_where('article', array('slug' => $slug));
    return $query->row_array();
  }

  public function get_find_pt($country){
    $query = $this->db->get_where('article',array('ref_id' => $country));
    return $query->result_array();
  }

  public function get_find_pts($id){
    $this->db->select('*');
    $this->db->from('article');
    $this->db->where('ref_id', $id);
    $query = $this->db->get();
    return $query->result_array();
    
  }

  public function get_find_without_pagination($slug = FALSE){
    if($slug === FALSE){
      $query = $this->db->get_where('article', array('position'=>'find-item'));
      //$this->db->limit(2, 0);
      return $query->result_array();
    }
    $query = $this->db->get_where('article', array('slug' => $slug));
    return $query->row_array();
  }

  public function get_find_by_id($id)
  {
    $query = $this->db->get_where('article', array('id' => $id));
    return $query->row_array();
  }

  public function get_find_items_by_pagination($data){
    $result_array['active_row_controller'] = $data['active_row_controller'];
    $result_array['row_per_page'] = $data['row_per_page'];
    $result_array['row_start'] = $data['row_start'];
    $this->db->limit($result_array['row_per_page'], $result_array['row_start']);
    $query = $this->db->get_where('article',array('position' => 'find-item'));
    $result_array['result_array'] = $query->result_array();
    return $result_array;
  }

  public function add_find(){
    $data = array(
        'title' => $this->input->post('country'),
        'position' => 'find-item',
        'created_at' =>date('Y-m-d H:i:s')
    );
    return $this->db->insert('article',$data);
  }

  public function add_findcompany($data){
    $this->db->insert('article',$data);
    return;
  }

  public function update_find(){
    $id = $this->input->post('idcountry');
    $data = array(
      'title' => $this->input->post('country'),
      'updated_at' =>date('Y-m-d H:i:s')  
    );
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function update_findcompany($data){
    $this->db->where('id', $data['id']);
    $this->db->update('article',$data);
    return;
  }

  public function delete_find($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }

  public function delete_company($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }

  public function delete_companys($id){
    $this->db->where('ref_id', $id);
    $this->db->delete('article');
    return true;
  }

  public function delete_contry($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }
}
