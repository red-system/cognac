<?php
class Backend_collection_item_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_exist_generallink($general_link,$general_id){
    $this->db->from('article');
    $this->db->where('slug', $general_link);
    $this->db->where('id !=', $general_id);
    $query = $this->db->count_all_results();
    if($query>0) {
      $exist = 1;
    } else {
      $exist = 0;
    }
    return $exist;
  }
  
  public function get_count_by_position($position){
    $query = $this->db->query("SELECT * FROM article where position = '$position'");
    return $query->num_rows();
  }

  public function get_collection_item($slug = FALSE){
      if($slug === FALSE){
        $this->db->limit(8, 0);
        $query = $this->db->get_where('article',array('position' => 'collection-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_collection_item_related($slug =  FALSE){
      if($slug === FALSE){
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('position', 'collection-item');
        $this->db->order_by('rand()');
        $this->db->limit(8);
        $query = $this->db->get();
        return $query->result_array();
      }
      $this->db->select('*');
      $this->db->from('article');
      $this->db->where('position', 'collection-item');
      $this->db->where('slug !=',$slug);
      $this->db->order_by('rand()');
      $this->db->limit(8);
      $query = $this->db->get();
      return $query->result_array();
  }



  public function get_collection_item_without_pagination($slug = FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article',array('position' => 'collection-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_collection_item_by_id($id){
      $query = $this->db->get_where('article', array('id' => $id));
      return $query->row_array();
  }

  public function get_collection_item_by_pagination($data){
    $result_array['active_row_controller'] = $data['active_row_controller'];
    $result_array['row_per_page'] = $data['row_per_page'];
    $result_array['row_start'] = $data['row_start'];
    $this->db->limit($result_array['row_per_page'], $result_array['row_start']);
    $query = $this->db->get_where('article',array('position' => 'collection-item'));
    $result_array['result_array'] = $query->result_array();
    return $result_array;
  }

  public function add_cognac_collection($add_data){
    $slug = url_title($this->input->post('title'));
    $thumb_image = $add_data['file_name'];
    $data = array(
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'thumb_image' => $thumb_image,
        'short_description' => $this->input->post('short_description'),
        'conten' => $this->input->post('content'),
        'rating' => $this->input->post('rating'),
        'reviews' => $this->input->post('reviews'),
        'meta_title' => $this->input->post('meta_title'),
        'meta_keyword' => $this->input->post('meta_keyword'),
        'meta_description' => $this->input->post('meta_description'),
        'position' => 'collection-item',
        'published' => $this->input->post('published'),
        'lang' => $this->input->post('lang'),
        'created_at' =>date('Y-m-d H:i:s')
    );
    return $this->db->insert('article',$data);
  }

  public function update_cognac_collection($additional_data){
    $id = $this->input->post('id');
    $slug = url_title($this->input->post('title'));

    if(isset($additional_data['image_name'])){
      $data = array(
      'title' => $this->input->post('title'),
      'slug' => $slug,
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'rating' => $this->input->post('rating'),
      'reviews' => $this->input->post('reviews'),
      'meta_title' => $this->input->post('meta_title'),
      'meta_keyword' => $this->input->post('meta_keyword'),
      'meta_description' => $this->input->post('meta_description'),
      'published' => $this->input->post('published'),
      'lang' => $this->input->post('lang'),
      'updated_at' =>date('Y-m-d H:i:s'),
      'thumb_image' => $additional_data['image_name']
      );
    } else {
      $data = array(
      'title' => $this->input->post('title'),
      'slug' => $slug,
      'short_description' => $this->input->post('short_description'),
      'conten' => $this->input->post('content'),
      'rating' => $this->input->post('rating'),
      'reviews' => $this->input->post('reviews'),
      'meta_title' => $this->input->post('meta_title'),
      'meta_keyword' => $this->input->post('meta_keyword'),
      'meta_description' => $this->input->post('meta_description'),
      'published' => $this->input->post('published'),
      'lang' => $this->input->post('lang'),
      'updated_at' =>date('Y-m-d H:i:s')
       );
    }
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function delete_collection_item($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }

  public function get_collection_pic($id = FALSE){
      if($id === FALSE){
      $this->db->select('*');
      $this->db->from('article');
      $this->db->join('images', 'images.article_id=article.id');
      $query = $this->db->get();
      return $query->result_array();
    }

    $this->db->select('*');
    $this->db->from('article');
    $this->db->join('images', 'images.article_id=article.id');
    $this->db->where('images.article_id', $id);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function picourcognac_add($data){
    $this->db->insert('images', $data);
    return;
  }

  public function picourcognac_delete($id){
    $this->db->where('id', $id);
    $this->db->delete('images');
    return true;
  }
}
