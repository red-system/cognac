<?php
class Backend_team_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_count_by_position($position){
    $query = $this->db->query("SELECT * FROM article where position = '$position'");
    return $query->num_rows();
  }

  public function get_team_items($slug =  FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article',array('position' => 'team-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_team_without_pagination($slug = FALSE){
      if($slug === FALSE){
        $query = $this->db->get_where('article', array('position'=>'team-item'));
        //$this->db->limit(2, 0);
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_team_by_id($id)
  {
    $query = $this->db->get_where('article', array('id' => $id));
    return $query->row_array();
  }


  public function get_team_items_by_pagination($data){
        $result_array['active_row_controller'] = $data['active_row_controller'];
        $result_array['row_per_page'] = $data['row_per_page'];
        $result_array['row_start'] = $data['row_start'];
        $this->db->limit($result_array['row_per_page'], $result_array['row_start']);
        $query = $this->db->get_where('article',array('position' => 'team-item'));
        $result_array['result_array'] = $query->result_array();
        return $result_array;
  }

  public function add_team($add_data){
    $thumb_image = $add_data['file_name'];
    $data = array(
        'title' => $this->input->post('title'),
        'short_description' => $this->input->post('short_description'),
        'thumb_image' => $thumb_image,
        'position' => 'team-item',
        'facebook_link' => $this->input->post('facebook_link'),
        'twitter_link' => $this->input->post('twitter_link'),
        'googleplus_link' => $this->input->post('googleplus_link'),
        'email' => $this->input->post('email'),
        'created_at' =>date('Y-m-d H:i:s')
    );
    return $this->db->insert('article',$data);
  }

  public function update_team($additional_data){
    $id = $this->input->post('id');
    
   if(isset($additional_data['image_name'])){
      $data = array(
        'title' => $this->input->post('title'),
        'short_description' => $this->input->post('short_description'),
        'facebook_link' => $this->input->post('facebook_link'),
        'twitter_link' => $this->input->post('twitter_link'),
        'googleplus_link' => $this->input->post('googleplus_link'),
        'email' => $this->input->post('email'),
        'updated_at' =>date('Y-m-d H:i:s'),
        'thumb_image' => $additional_data['image_name']
      );
    } else {
      $data = array(
        'title' => $this->input->post('title'),
        'short_description' => $this->input->post('short_description'),
        'facebook_link' => $this->input->post('facebook_link'),
        'twitter_link' => $this->input->post('twitter_link'),
        'googleplus_link' => $this->input->post('googleplus_link'),
        'email' => $this->input->post('email'),
        'updated_at' =>date('Y-m-d H:i:s')  
      );
    }
    $this->db->where('id', $id);
    return $this->db->update('article', $data);
  }

  public function delete_team($id){
    $this->db->where('id', $id);
    $this->db->delete('article');
    return true;
  }
}
