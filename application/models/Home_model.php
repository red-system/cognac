<?php
class Home_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_slider(){
      $query = $this->db->get_where('article', array('position' => 'slider'));
      return $query->result_array();
  }

  public function get_header($slug =  FALSE){
      if($slug === FALSE){
        $query = $this->db->get('article');
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('position' => 'header', 'title' => $slug));
      return $query->row_array();
  }

  public function get_footer($slug =  FALSE){
      if($slug === FALSE){
        $query = $this->db->get('article');
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('position' => 'footer', 'title' => $slug));
      return $query->row_array();
  }

  public function get_footerlink($slug =  FALSE){
      if($slug === FALSE){
        $query = $this->db->get('article');
        return $query->result_array();
      }
      $query = $this->db->get_where('social_links', array('social_media' => $slug));
      return $query->row_array();
  }

  public function get_home_content(){
      $query = $this->db->get_where('article', array('position' => 'slider'));
      return $query->result_array();
  }

  public function get_collection_item($slug = FALSE){
      if($slug === FALSE){
        $this->db->limit(3, 0);
        $query = $this->db->get_where('article',array('position' => 'collection-item'));
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('slug' => $slug));
      return $query->row_array();
  }

  public function get_footer_collumns(){
    $this->db->limit(7,0);
    $query = $this->db->get_where('article', array('position'=>'collumns-item'));
    return $query->result_array();
  }

  public function get_social_medias(){
    $query = $this->db->get_where('social_links', array('published'=>'Yes'));
    //$this->db->limit(2, 0);
    return $query->result_array();
  }

  public function get_users_comments(){
    $query = $this->db->get_where('article', array('position'=>'user-comments'));
    //$this->db->limit(2, 0);
    return $query->result_array();
  }

  public function get_news(){
    $query = $this->db->get_where('article', array('position'=>'news-item'));
    //$this->db->limit(2, 0);
    return $query->result_array();
  }

  public function get_footer_content(){
    $query = $this->db->get_where('article', array('position'=>'footer-content'));
    //$this->db->limit(2, 0);
    return $query->row_array();
  }

  public function get_partners(){
    $query = $this->db->get_where('article',array('position' => 'partners-item'));
    return $query->result_array();
  }

  public function get_team(){
    $query = $this->db->get_where('article',array('position' => 'team-item'));
    return $query->result_array();
  }

  public function get_pageimage($slug =  FALSE){
      if($slug === FALSE){
        $query = $this->db->get('article');
        return $query->result_array();
      }
      $query = $this->db->get_where('article', array('position' => 'page-image', 'title' => $slug));
      return $query->row_array();
  }

  public function get_cognac_id($slug){
    $this->db->select('id');
    $this->db->from('article');
    $this->db->where('slug', $slug);
    $query = $this->db->get();
    $row = $query->first_row();
    return $row;
  }

  public function get_collection_search($slug = FALSE){
      $search = $this->input->post('title');
      if($slug === FALSE){
        $this->db->limit(3, 0);
        $query = $this->db->get_where('article',array('position' => 'collection-item','title' => $search));
        return $query->num_rows();
      }
      $query = $this->db->get_where('article', array('position' => 'collection-item','title' => $search,'slug' => $slug));
      return $query->num_rows();
  }

  public function get_collection_page($limit, $start){
      $search = $this->input->post('title');
      $query = $this->db->where(array('position' => 'collection-item','title' => $search))->get('article',$limit, $start);
      return $query;
  }

}
