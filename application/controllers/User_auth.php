<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_auth extends CI_Controller {
	
	function __construct(){
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->library('email');    
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
           
        }

	public function index()
	{
		$this->load->view('backendnew/login');
		
	}


 	public function login_process(){
	    $email = $this->input->post('email');
	   	$password = $this->input->post('password');
    	$num_user_row = $this->backend_profile_model->get_num_user_rows();
        if($num_user_row==0){
            $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                 </button>
				 <p>The login was unsucessful!!!
				 </p>');
			
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
				
			
			} else {
				//$this->post_model->set_article($data);
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				//cek database disini
				$this->session->set_userdata(array(
	                            'email'         => $email,
	                            'password'      => $password,
	                            'status'        => TRUE
	                    ));
				redirect('backend_profile/profile');
			}
		//$this->load->view('backend/login',$data);
	}
	
	public function logout($value='')
   	{
       $this->session->unset_userdata('email');
       $this->session->unset_userdata('password');
       $this->session->unset_userdata('status');
       $this->session->sess_destroy();
       redirect('user_auth');
   	}

   	public function forgot()
    {
     	$this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 
            
        if($this->form_validation->run() == FALSE) {
           $this->load->view('backendnew/forgotpassword'); 
        }else{
            $email = $this->input->post('email');  
            $clean = $this->security->xss_clean($email);
            $userInfo = $this->user_auth_model->getUserInfoByEmail($clean);
            
            if(!$userInfo){
                $this->session->set_flashdata('true', 
                '<button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="icon-remove"></i>
                 </button>
                 <p><strong>We cant find your email address!!!</strong>
                 </p>');
                $this->load->library('user_agent');
                redirect(site_url().'user_auth/forgot');
            }   
            
        //build token 
			
  		    $token = $this->user_auth_model->insertToken($userInfo->id);                        
            $qstring = $this->base64url_encode($token);                  
            $url = site_url() . 'user_auth/reset_password/' . $qstring;
            $link = '<a href="' . $url . '">' . $url . '</a>'; 
            
            // $message = '';                     
            // $message .= '<strong>A password reset has been requested for this email account</strong><br>';
            // $message .= '<strong>Please click:</strong> ' . $link;             
            // echo $message; //send this through mail
            // exit;

            // Konfigurasi email.
            $config = [
               'useragent' => 'CodeIgniter',
               'protocol'  => 'smtp',
               'mailpath'  => '/usr/sbin/sendmail',
               'smtp_host' => 'ssl://smtp.gmail.com',
               'smtp_user' => 'agusdownload5293@gmail.com',   // Ganti dengan email gmail Anda.
               'smtp_pass' => 'agusarim',             // Password gmail Anda.
               'smtp_port' => 465,
               'smtp_keepalive' => TRUE,
               'smtp_crypto' => 'SSL',
               'wordwrap'  => TRUE,
               'wrapchars' => 80,
               'mailtype'  => 'html',
               'charset'   => 'utf-8',
               'validate'  => TRUE,
               'crlf'      => "\r\n",
               'newline'   => "\r\n",
            ];

            // Load library email dan konfigurasinya.
            $this->email->initialize($config);

            // Pengirim dan penerima email.
            $this->email->from('info@audrycognac.com','Info Audry Cognac');    // Email dan nama pegirim.
            $this->email->to($this->input->post('email'));   // Penerima email.

            // Lampiran email. Isi dengan url/path file.
            // $this->email->attach('https://masrud.com/themes/masrud/img/logo.png');

            // Subject email.
            $this->email->subject('A password reset has been requested for www.audrycognac.com');

            // Isi email. Bisa dengan format html.
            $this->email->message('
                <strong>A password reset has been requested for this email account</strong><br>
                <br>
                <strong>Please click:</strong>'. $link);

            if ($this->email->send())
            {
                echo "<script>alert('Success! Please check your email to reset your password.');history.go(-1);</script>";
            }
            else
            {
                echo "<script>alert('Failed');history.go(-1);</script>";
            }
                
        }

    }

    public function reset_password()
    {
        $token = $this->base64url_decode($this->uri->segment(3));                  
        $cleanToken = $this->security->xss_clean($token);

        $user_info = $this->user_auth_model->isTokenValid($cleanToken); //either false or array();               
        
        if(!$user_info){
            $this->session->set_flashdata('true', 
            '<button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon-remove"></i>
             </button>
             <p><strong>Token is invalid or expired!!!</strong>
             </p>');
            $this->load->library('user_agent');
            redirect(site_url().'user_auth');
        } 

        $data = array(
            'name'=> $user_info->name, 
            'email'=>$user_info->email,                
            'remember_token'=>$this->base64url_encode($token)
        );

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');              
        if ($this->form_validation->run() == FALSE) {   
            $this->load->view('backendnew/resetpassword',$data); 
        }else{
                            
            $post = $this->input->post(NULL, TRUE);   
            $cleanPost = $this->security->xss_clean($post); 
            $cleanPost['password'] = $cleanPost['password'];
            $cleanPost['id'] = $user_info->id;
            unset($cleanPost['passconf']);                
            if(!$this->user_auth_model->updatePassword($cleanPost)){
                $this->session->set_flashdata('flash_message', 'There was a problem updating your password');
            }else{
               	$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p><strong>Your password has been updated. You may now login!!!</strong>
				 </p>');
			}
			$this->load->library('user_agent');
            redirect(site_url().'user_auth');                
        }
    }

    function base64url_encode( $data ){
	  return rtrim( strtr( base64_encode( $data ), '+/', '-_'), '=');
	}

	function base64url_decode( $data ){
	  return base64_decode( strtr( $data, '-_', '+/') . str_repeat('=', 3 - ( 3 + strlen( $data )) % 4 ));
	}

}
