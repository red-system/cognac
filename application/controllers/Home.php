<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'home');
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def
		
		$data['sliders'] = $this->home_model->get_slider();
		$data['home_content'] = $this->backend_home_model->get_home_content_by_position('home-content');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['collection_header'] = $this->backend_home_model->get_home_content_by_position('collection-header');
		$data['collection_items'] = $this->backend_collection_item_model->get_collection_item();
		$data['count']=$this->backend_collection_item_model->get_count_by_position('collection-item');
		$data['user_comments']=$this->home_model->get_users_comments();
		$data['partners']=$this->home_model->get_partners();
		$data['title'] = "Home";
		$this->load->view('template/header',$data);
		$this->load->view('slider');
		$this->load->view('home');
		$this->load->view('template/footer');
	}

	public function about()
	{
		$this->load->view('about');
	}

	public function legacy()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'legacy');
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def

		$data['legacy'] = $this->home_model->get_pageimage('Legacy-image');
		$data['legacy_content'] = $this->backend_home_model->get_home_content_by_position('legacy-content');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['teams'] = $this->home_model->get_team();
		$data['user_comments']=$this->home_model->get_users_comments();
		$data['title'] = "Legacy";
		$this->load->view('template/header',$data);
		$this->load->view('legacy');
		$this->load->view('template/footer');
	}

	public function cognac()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'cognac');
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def

		$data['ourcognac'] = $this->home_model->get_pageimage('Ourcognac-image');
		$data['collection_header'] = $this->backend_home_model->get_home_content_by_position('collection-header');
		$data['collection_items'] = $this->backend_collection_item_model->get_collection_item();
		$data['count']=$this->backend_collection_item_model->get_count_by_position('collection-item');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Cognac";
		$this->load->view('template/header',$data);
		$this->load->view('cognac');
		$this->load->view('template/footer');
	}

	public function cognac_pagination($active_row = FALSE, $row_per_page=FALSE, $row_start=FALSE)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'cognac');
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def

		$data['ourcognac'] = $this->home_model->get_pageimage('Ourcognac-image');

		$data['active_row_controller'] = $active_row;
		$data['row_per_page'] = $row_per_page;
		$data['row_start'] =  $row_start;
		$data['c_header'] = $this->backend_home_model->get_home_content_by_position('collection-header');
		$result_arrays =  $this->backend_collection_item_model->get_collection_item_by_pagination($data);
		$data['collection_items'] = $result_arrays['result_array'];
		$data['count']=$this->backend_collection_item_model->get_count_by_position('collection-item');
		$data['active_row_controller'] = $result_arrays['active_row_controller'];
		$data['row_per_page'] = $result_arrays['row_per_page'];
		$data['row_start'] =  $result_arrays['row_start'];
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Cognac";
		$this->load->view('template/header',$data);
		$this->load->view('cognac');
		$this->load->view('template/footer');
	}

	public function cognac_details($slug)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'cognac');
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def

		$data['ourcognac'] = $this->home_model->get_pageimage('Ourcognac-image');

		$data['slug']=$slug;
		$data['cognac_details'] = $this->backend_collection_item_model->get_collection_item($slug);

		$file = $this->home_model->get_cognac_id($slug);
		$data['ourcognacpic'] = $this->backend_collection_item_model->get_collection_pic($file->id);
		$data['collection_items'] = $this->backend_collection_item_model->get_collection_item_related($slug);
		
		
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Cognac Details";
		$this->load->view('template/header',$data);
		$this->load->view('cognac_details');
		$this->load->view('template/footer');
	}

	public function Editorial()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'editorial');
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def

		$data['editorial'] = $this->home_model->get_pageimage('Editorial-image');
		$data['editorial_content'] = $this->backend_home_model->get_home_content_by_position('editorial-content');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Editorial";
		$this->load->view('template/header',$data);
		$this->load->view('editorial');
		$this->load->view('template/footer');
	}

	public function Find()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'find');
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def

		$data['findus'] = $this->home_model->get_pageimage('Findus-image');
		$jumlahpt = 0;
		$data['contacts_content'] = $this->backend_home_model->get_home_content_by_position('contacts-content');
		$data['title'] = "Contacts";
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['country'] = $this->backend_find_model->get_find_items();
		foreach ($data['country'] as $key => $value) {
			$jumlahpt++;
			foreach ($value as $key => $isi) {
				if($key == 'id'){
					$data['pts'][$isi] = $this->backend_find_model->get_find_pt($isi);
				}
			}
		}
		
		$this->load->view('template/header',$data);
		$this->load->view('find');
		$this->load->view('template/footer');
	}

	public function Contact()
	{
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def

		$data['contactpage'] = $this->home_model->get_pageimage('Contact-image');
		$data['contacts_content'] = $this->backend_home_model->get_home_content_by_position('contacts-content');
		$data['title'] = "Contacts";
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$this->load->view('template/header',$data);
		$this->load->view('contact');
		$this->load->view('template/footer');
	}

	public function news() 
	{
 		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def
		$data['news_image'] = $this->home_model->get_pageimage('News-image');
 		//konfigurasi pagination
        $config['base_url'] = site_url('home/news'); //site url
        $config['total_rows'] = $this->backend_news_item_model->record_count(); //total row
        $config['per_page'] = 3;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 
        // Membuat Style pagination untuk BootStrap v4
      	$config['full_tag_open'] = '<nav class="pagination clear">';
		$config['full_tag_close'] = '</nav>';

		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		

		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		

		$config['cur_tag_open'] = '<span class="page-numbers current">';
		$config['cur_tag_close'] = '</span>';

		
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->backend_news_item_model->get_news_item_page($config["per_page"], $data['page']);           
 
        $data['pagination'] = $this->pagination->create_links();
        $data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
 
        //load view mahasiswa view
        $this->load->view('template/header',$data);
		$this->load->view('news');
		$this->load->view('template/footer');
	}

	public function news_details($slug = NULL)
	{
		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def
		$data['news_image'] = $this->home_model->get_pageimage('News-image');
		
		$data['slug']=$slug;
		$data['news_details'] = $this->backend_news_item_model->get_news_item($slug);
		$data['news_others'] = $this->backend_news_item_model->get_news_item_other($slug);
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "News Details";
		$this->load->view('template/header',$data);
		$this->load->view('news_detail');
		$this->load->view('template/footer');
	}

	public function __construct() 
	{ 
    	parent::__construct(); 
    	$this->load->helper(array('form', 'url', 'html','language'));
    	$this->load->library(array('form_validation','email','session')); //tambahkan dalam contruct pemanggil libarary mail
  	}
	
  	function kirim_email() 
  	{
        // Konfigurasi email.
        $config = [
               'useragent' => 'CodeIgniter',
	           'protocol'  => 'smtp',
	           'mailpath'  => '/usr/sbin/sendmail',
	           'smtp_host' => 'ssl://smtp.gmail.com',
	           'smtp_user' => 'agusdownload5293@gmail.com',   // Ganti dengan email gmail Anda.
	           'smtp_pass' => 'agusarim',             // Password gmail Anda.
	           'smtp_port' => 465,
	           'smtp_keepalive' => TRUE,
	           'smtp_crypto' => 'SSL',
	           'wordwrap'  => TRUE,
	           'wrapchars' => 80,
	           'mailtype'  => 'html',
	           'charset'   => 'utf-8',
	           'validate'  => TRUE,
	           'crlf'      => "\r\n",
	           'newline'   => "\r\n",
	           ];
 
        // Load library email dan konfigurasinya.
        $this->email->initialize($config);
 
        // Pengirim dan penerima email.
        $this->email->from($this->input->post('email'),$this->input->post('first-name'));    // Email dan nama pegirim.
        $this->email->to('agusdownload5293@gmail.com');                       // Penerima email.
 
        // Lampiran email. Isi dengan url/path file.
        // $this->email->attach('https://masrud.com/themes/masrud/img/logo.png');
 
        // Subject email.
        $this->email->subject('Email from user www.audrycognac.com');
 
        // Isi email. Bisa dengan format html.
        $this->email->message($this->input->post('message'));
 
        if ($this->email->send())
        {
             
        
            echo "<script>alert('Success!');history.go(-1);</script>";
        }
        else
        {
            echo "<script>alert('Failed');history.go(-1);</script>";
        }
    }

    public function collection_search() 
	{
 		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'cognac');
 		//def
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['news']=$this->backend_news_item_model->get_news_item_sort();
		//def
		$data['ourcognac'] = $this->home_model->get_pageimage('Ourcognac-image');

 		//konfigurasi pagination
        $config['base_url'] = site_url('home/collection_search'); //site url
        $config['total_rows'] = $this->home_model->get_collection_search(); //total row
        //print_r($config['total_rows']);die();
        $config['per_page'] = 12;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 
        // Membuat Style pagination untuk BootStrap v4
      	$config['full_tag_open'] = '<nav class="pagination clear">';
		$config['full_tag_close'] = '</nav>';

		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		

		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		

		$config['cur_tag_open'] = '<span class="page-numbers current">';
		$config['cur_tag_close'] = '</span>';

		
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->home_model->get_collection_page($config["per_page"], $data['page']);           
 
        $data['pagination'] = $this->pagination->create_links();
        $data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
 
        //load view mahasiswa view
        $this->load->view('template/header',$data);
		$this->load->view('collection_search');
		$this->load->view('template/footer');
	}

}
