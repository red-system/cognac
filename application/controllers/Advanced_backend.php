<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Advanced_backend extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
					redirect('backend_login/login_form');
        }
    }

	public function success_page($goback)
	{
		$data['goback']=$goback;
		$this->load->view('backend/success',$data);
	}
	public function home()
	{
		$data['title'] = "Home";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_home_page',$data);
		$this->load->view('backend/templates/footer');

	}

	public function manage_slider()
	{
		$data['title'] = "Manage Slider";
		$data['slides'] = $this->backend_slides_model->get_slider();
		$this->load->view('backend/templates/header');
		$this->load->view('backend/main',$data);
		$this->load->view('backend/templates/footer');

	}
	
	public function add_slider_image()
	{
		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Add a Slider Image';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if(($this->form_validation->run() === FALSE) || (! $this->upload->do_upload('thumb_image'))){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_add_slider',$data);
			$this->load->view('backend/templates/footer');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->backend_slides_model->add_slider_image($data);
			$data['link'] = "manage_slider";
			$data['title'] = "Manage Slider";
			$this->load->view('backend/success',$data);
		}

	}

	public function edit_home_content(){
		$data['home_content'] = $this->backend_home_model->get_home_content_by_position('home-content');
		$data['title'] = 'Update Home Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_home_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_home_content()
	{
		$data['home_content'] = $this->backend_home_model->get_home_content_by_position('home-content');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Home Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_home_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function edit_editorial_content(){
		$data['editorial_content'] = $this->backend_home_model->get_home_content_by_position('editorial-content');
		$data['title'] = 'Update Editorial Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_editorial_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_editorial_content()
	{
		$data['editorial_content'] = $this->backend_home_model->get_home_content_by_position('editorial-content');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Editorial Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_editorial_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function edit_legacy_content(){
		$data['legacy_content'] = $this->backend_home_model->get_home_content_by_position('legacy-content');
		$data['title'] = 'Update Legacy Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_legacy_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_legacy_content()
	{
		$data['legacy_content'] = $this->backend_home_model->get_home_content_by_position('legacy-content');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Legacy Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_legacy_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function edit_rekindled_content(){
		$data['rekindled_content'] = $this->backend_home_model->get_home_content_by_position('rekindled-content');
		$data['title'] = 'Update Rekindled Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_rekindled_content',$data);
		$this->load->view('backend/templates/footer');
	}


	public function update_rekindled_content()
	{
		$data['rekindled_content'] = $this->backend_home_model->get_home_content_by_position('rekindled-content');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Rekindled Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_rekindled_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function edit_perfect_ballance_content(){
		$data['perfect_ballance_content'] = $this->backend_home_model->get_home_content_by_position('perfect-ballance-content');
		$data['title'] = 'Update Rekindled Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_perfect_ballance_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_perfect_ballance_content()
	{
		$data['perfect_ballance_content'] = $this->backend_home_model->get_home_content_by_position('perfect-ballance-content');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Rekindled Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_perfect_ballance_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}


	public function edit_collection_header(){
		$data['collection_header'] = $this->backend_home_model->get_home_content_by_position('collection-header');
		$data['title'] = 'Update Collection Header';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_collection_header',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_collection_header()
	{
		$data['collection_header'] = $this->backend_home_model->get_home_content_by_position('collection-header');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Rekindled Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_collection_header',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function manage_collection_item()
	{
		$data['collection_items'] = $this->backend_collection_item_model->get_collection_item_without_pagination();
		$data['title'] = "Manage Collection Item";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_collection_item',$data);
		$this->load->view('backend/templates/footer');

	}

	public function edit_collection_item($id)
	{
		$data['collection_content'] = $this->backend_collection_item_model->get_collection_item_by_id($id);
		$data['title'] = 'Update Collection Itent';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_collection_item',$data);
		$this->load->view('backend/templates/footer');
	}


	public function update_collection_content($id)
	{
		$data['collection_content'] = $this->backend_collection_item_model->get_collection_item_by_id($id);

		$config['upload_path']          = './assets/img/collection';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Collection Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_collection_item',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				$data['link'] = "manage_collection_item";
				$data['title'] = "Manage Collection";
				$this->load->view('backend/success',$data);
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				$data['link'] = "manage_collection_item";
				$data['title'] = "Manage Collection";
				$this->load->view('backend/success',$data);
			}
		}
	}

	public function add_cognac_collection()
	{
		$config['upload_path']          = './assets/img/collection';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Add a Collection Item';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if(($this->form_validation->run() === FALSE) || (! $this->upload->do_upload('thumb_image'))){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_add_collection_item',$data);
			$data['link'] = "manage_collection_item";
			$data['title'] = "Manage Collection";
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$this->backend_collection_item_model->add_cognac_collection($data);
			$data['link'] = "manage_collection_item";
			$data['title'] = "Manage Collection";
			$this->load->view('backend/success',$data);
		}

	}


	public function edit_testings_content(){
		$data['testing_contents'] = $this->backend_home_model->get_home_content_by_position('testing-content');
		$data['title'] = 'Update Testings Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_testings_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_testings_content()
	{
		$data['testing_contents'] = $this->backend_home_model->get_home_content_by_position('testing-content');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Testings Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_testings_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function edit_collumns_content(){
		$data['collumns_content'] = $this->backend_home_model->get_home_content_by_position('collumns-content');
		$data['title'] = 'Update Testings Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_testings_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function edit_collumns_header(){
		$data['collumns_header'] = $this->backend_home_model->get_home_content_by_position('collumns-header');
		$data['title'] = 'Update Collumns Header';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_collumns_header',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_collumns_header()
	{
		$data['collumns_header'] = $this->backend_home_model->get_home_content_by_position('collumns-header');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Collumns Header';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_collumns_header',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function manage_collumns_item()
	{
		$data['collumns_items'] = $this->backend_collumns_item_model->get_collumn_items_without_pagination();
		$data['title'] = "Manage Collumns Item";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_collumn_items',$data);
		$this->load->view('backend/templates/footer');
	}

	public function add_collumns_item()
	{
		$config['upload_path']          = './assets/img/collection';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Add a Collumns Item';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if(($this->form_validation->run() === FALSE)){
			//$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_add_collumns_item',$data);
			$this->load->view('backend/templates/footer');
		} else {
			$this->upload->do_upload('my_file');
			$data['file_name']= $this->upload->data('file_name');
			$this->backend_collumns_item_model->add_collumns_item($data);
			$data['link'] = "manage_collumns_item";
			$data['title'] = "Manage Collumns";
			$this->load->view('backend/success',$data);
		}

	}

	public function edit_collumns_item($id)
	{
		$data['collumns_items'] = $this->backend_collumns_item_model->get_collumn_item_by_id($id);
		$data['title'] = "Edit Collumns Item";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_collumns_item',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_collumns_item($id)
	{
		$data['collumns_items'] = $this->backend_collumns_item_model->get_collumn_item_by_id($id);

		$config['upload_path']          = './assets/img/collection';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Collumns Item';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			//$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			//echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_collumns_item',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('my_file')){
				echo "form validation not false and image not false";
				$data['file_name']= $this->upload->data('file_name');
				$this->backend_collumns_item_model->update_collumns_item($data);
				$data['link'] = "manage_collumns_item";
				$data['title'] = "Manage Collumns";
				$this->load->view('backend/success',$data);
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_collumns_item_model->update_collumns_item($data);
				$data['link'] = "manage_collumns_item";
				$data['title'] = "Manage Collumns";
				$this->load->view('backend/success',$data);
			}
		}
	}

	public function edit_contacts_content(){
		$data['contacts_content'] = $this->backend_home_model->get_home_content_by_position('contacts-content');
		$data['title'] = 'Update Rekindled Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_contacts_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_contacts_content()
	{
		$data['contacts_content'] = $this->backend_home_model->get_home_content_by_position('contacts-content');

		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Update Contacts Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_contacts_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function edit_footer_content(){
		$data['footer_content'] = $this->backend_home_model->get_home_content_by_position('footer-content');
		$data['title'] = 'Update Footer Content';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_footer_content',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_footer_content()
	{
		$data['footer_content'] = $this->backend_home_model->get_home_content_by_position('footer-content');
		$data['title'] = 'Update Footer Content';
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_footer_content',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_home_model->update_home_content($data);
				redirect('advanced_backend/success_page');
			}
		}
	}

	public function manage_social_links(){
		$data['social_links'] = $this->backend_social_links_model->get_social_links_by_name();
		$data['title'] = 'Update Social Links';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_social_links',$data);
		$this->load->view('backend/templates/footer');
	}

	public function edit_social_links($social_media){
		$data['social_link'] = $this->backend_social_links_model->get_social_links_by_name($social_media);
		$data['title'] = 'Update Social Links';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_social_link',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_social_link($social_media){
		$data['social_link'] = $this->backend_social_links_model->get_social_links_by_name($social_media);
		$data['title'] = 'Update Social Link';
		$this->form_validation->set_rules('link', 'Link', 'required');
		if($this->form_validation->run() === FALSE){
			//echo "validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_social_link',$data);
			$this->load->view('backend/templates/footer');
		} else {
			//echo "validation true";
			$this->backend_social_links_model->update_social_link($social_media);
			redirect('advanced_backend/success_page');
		}
	}

	public function manage_user_comments()
	{
		$data['user_comments'] = $this->backend_comments_model->get_comments_without_pagination();
		$data['title'] = "Manage Comments";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_user_comments',$data);
		$this->load->view('backend/templates/footer');

	}

	public function edit_comment($id)
	{
		$data['comment_item'] = $this->backend_comments_model->get_comment_by_id($id);
		$data['title'] = "Edit Comments";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_comment_item',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_comment($id)
	{
		$data['collumns_items'] = $this->backend_comments_model->get_comment_by_id($id);

		$data['title'] = 'Update Comments Item';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			//$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			//echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_comment_item',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('my_file')){
				echo "form validation not false and image not false";
				$data['file_name']= $this->upload->data('file_name');
				$this->backend_collumns_item_model->update_collumns_item($data);
				$data['link'] = "manage_user_comments";
				$data['title'] = "Manage Comments";
				$this->load->view('backend/success',$data);
			} else {
				echo "form validation not false but thumb image false";
				$this->backend_collumns_item_model->update_collumns_item($data);
				$data['link'] = "manage_user_comments";
				$data['title'] = "Manage Comments";
				$this->load->view('backend/success',$data);
			}
		}
	}

	public function add_comment()
	{
		$data['title'] = 'Add Comments';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_add_comment_item',$data);
			$this->load->view('backend/templates/footer');
		} else {
			$this->backend_comments_model->add_comments_item($data);
			$data['link'] = "manage_user_comments";
			$data['title'] = "Manage Comments";
			$this->load->view('backend/success',$data);
		}
	}

	//form untuk manage news//
	//disini di buat dan dirangkum semuanya//
	public function manage_news_item()
	{
		$data['news_items'] = $this->backend_news_item_model->get_news_item_without_pagination();
		$data['title'] = "Manage News Item";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_news_item',$data);
		$this->load->view('backend/templates/footer');

	}

	public function add_news()
	{
		$config['upload_path']          = './assets/img/blog';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Add a News Item';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if(($this->form_validation->run() === FALSE) || (! $this->upload->do_upload('thumb_image'))){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_add_news',$data);
			$data['link'] = "manage_news_item";
			$data['title'] = "Manage News";
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_news_item_model->add_news($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
		redirect('advanced_backend/manage_news_item');
		}
	}

	public function edit_news_item($id)
	{
		$data['news_content'] = $this->backend_news_item_model->get_news_item_by_id($id);
		$data['title'] = 'Update News Item';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_news_item',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_news_content($id)
	{
		$data['news_content'] = $this->backend_news_item_model->get_news_item_by_id($id);

		$config['upload_path']          = './assets/img/blog';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Update News Content';
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('short_description', 'Short Desc', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		if($this->form_validation->run() === FALSE){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			echo "form validation false";
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_edit_news_item',$data);
			$this->load->view('backend/templates/footer');
		} else {
			if($this->upload->do_upload('thumb_image')){
				echo "form validation not false and image not false";
				$data['image_name']= $this->upload->data('file_name');
				$res=$this->backend_news_item_model->update_news_content($data);
				if($res==true)
				{
				  $this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Update done!!!
					 </p>');
				}
				else
				{
				  $this->session->set_flashdata('err', "Update failed!!!");
				}
			} else {
				//echo "form validation not false but thumb image false";
				$res=$this->backend_news_item_model->update_news_content($data);
				if($res==true)
				{
				  $this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Update done!!!
					 </p>');
				}
				else
				{
				  $this->session->set_flashdata('err', "Update failed!!!");
				}
			}
			$this->load->library('user_agent');
			redirect('advanced_backend/manage_news_item');
		}
	}

	public function delete_news_item($id)
	{
		$res=$this->backend_news_item_model->delete_news_item($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('advanced_backend/manage_news_item');
	}
	//end of news

	//start partners
	public function manage_partners()
	{
		$data['partners'] = $this->backend_partners_model->get_partners_without_pagination();
		$data['title'] = "Manage Partners";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_partners_item',$data);
		$this->load->view('backend/templates/footer');
	}

	public function add_partners()
	{
		$config['upload_path']          = './assets/img/partners';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Add a Partners Item';
		if (!$this->upload->do_upload('thumb_image')){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_add_partners',$data);
			$data['link'] = "manage_partners_item";
			$data['title'] = "Manage Partners";
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_partners_model->add_partners_item($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
			redirect('advanced_backend/manage_partners');
		}
	}

	public function edit_partners($id)
	{
		$data['partners'] = $this->backend_partners_model->get_partners_by_id($id);
		$data['title'] = 'Update Partners Item';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_partners',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_partners($id)
	{
		$data['partners'] = $this->backend_partners_model->get_partners_by_id($id);

		$config['upload_path']          = './assets/img/partners';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if($this->upload->do_upload('thumb_image')){
			echo "form validation not false and image not false";
			$data['image_name']= $this->upload->data('file_name');
			$res=$this->backend_partners_model->update_partners($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			//echo "form validation not false but thumb image false";
			$res=$this->backend_partners_model->update_partners($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
				}
		}
		$this->load->library('user_agent');
		redirect('advanced_backend/manage_partners');
	}

	public function delete_partners($id)
	{
		$res=$this->backend_partners_model->delete_partners($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('advanced_backend/manage_partners');
	}
	//end of partners

	//start of team
	public function manage_team()
	{
		$data['teams'] = $this->backend_team_model->get_team_without_pagination();
		$data['title'] = "Manage Team";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_team',$data);
		$this->load->view('backend/templates/footer');
	}

	public function add_team()
	{
		$config['upload_path']          = './assets/img/team';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		$data['title'] = 'Add a Team';
		if (!$this->upload->do_upload('thumb_image')){
			$data['error'] = $this->upload->display_errors('<p class="alert alert-danger center">','</p>');
			$this->load->view('backend/templates/header');
			$this->load->view('backend/backend_add_team',$data);
			$data['link'] = "manage_team";
			$data['title'] = "Manage team";
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_team_model->add_team($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
			redirect('advanced_backend/manage_team');
		}
	}

	public function edit_team($id)
	{
		$data['team'] = $this->backend_team_model->get_team_by_id($id);
		$data['title'] = 'Update Team';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_team',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_team($id)
	{
		$data['team'] = $this->backend_team_model->get_team_by_id($id);

		$config['upload_path']          = './assets/img/team';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if($this->upload->do_upload('thumb_image')){
			echo "form validation not false and image not false";
			$data['image_name']= $this->upload->data('file_name');
			$res=$this->backend_team_model->update_team($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			//echo "form validation not false but thumb image false";
			$res=$this->backend_team_model->update_team($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		$this->load->library('user_agent');
		redirect('advanced_backend/manage_team');
	}

	public function delete_team($id)
	{
		$res=$this->backend_team_model->delete_team($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('advanced_backend/manage_team');
	}
	//end of partners

	//start of find us
	public function manage_find()
	{
		$data['finds'] = $this->backend_find_model->get_find_without_pagination();
		$data['title'] = "Manage Find Us";
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_manage_find',$data);
		$this->load->view('backend/templates/footer');
	}

	public function find_add()
	{
		$data['title'] = 'Add a Country';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_add_find',$data);
		$this->load->view('backend/templates/footer');
	}

	public function processfind_add()
	{
		$this->backend_find_model->add_find();

		$query = $this->db->query('SELECT id FROM article ORDER BY id DESC LIMIT 1');  
	    $result = $query->result_array();  
	    $id = $result[0]['id'];

	    $post = $this->input->post();
 
        $file = 0;
        foreach ($post['title'] as $key => $value) {
        	if(!empty($value)){
	        	$file++;
	        }	
        }

        for($i=0; $i<$file; $i++)
      	{
      		$data = array('ref_id'=> $id,
      					  'short_description' => $post['short_description'][$i],
                          'title' => $post['title'][$i]
                       	 );

      		$this->backend_find_model->add_findcompany($data);
      	}
      	$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Add Country done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect('advanced_backend/manage_find');
	}

	public function edit_find($id)
	{
		$data['find'] = $this->backend_find_model->get_find_by_id($id);
		$data['pts'] = $this->backend_find_model->get_find_pts($id);
		$data['title'] = 'Update Country';
		$this->load->view('backend/templates/header');
		$this->load->view('backend/backend_edit_find',$data);
		$this->load->view('backend/templates/footer');
	}

	public function update_find(){
		$this->backend_find_model->update_find();

		$postId = $this->input->post('id');
		$post = $this->input->post();
		
        $file = 0;
        foreach ($postId as $key => $value) {
        	$data = array(
        					'id'=>$value,
        					'short_description' => $post['short_description'][$value],
                          	'title' => $post['title'][$value]
                       	 );
        	$this->backend_find_model->update_findcompany($data);
        }
        $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update Country done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect('advanced_backend/manage_find');
	}

	public function processcompany_add()
	{
		$postId = $this->input->post('id');
		$post = $this->input->post();
		
        $file = 0;
        foreach ($postId as $key => $value) {
        	$data = array(
        					'ref_id'=>$value,
        					'short_description' => $post['address'][$value][$key],
                          	'title' => $post['name'][$value][$key]
                       	 );
        	$this->backend_find_model->add_findcompany($data);
        }
        $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Add Company done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect('advanced_backend/manage_find');
    }

	public function delete_company($id)
	{
		$res=$this->backend_find_model->delete_company($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('advanced_backend/manage_find');
	}

	public function delete_contry($id)
	{
		$this->backend_find_model->delete_companys($id);
		$res=$this->backend_find_model->delete_contry($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('advanced_backend/manage_find');
	}



	





	public function delete_slider($id)
	{
		$this->backend_home_model->delete_slider($id);
		redirect('advanced_backend/manage_slider');
	}

	public function delete_collumns_item($id)
	{
		$this->backend_collumns_item_model->delete_collumn($id);
		redirect('advanced_backend/manage_collumns_item');
	}

	public function delete_collection_item($id)
	{
		$this->backend_collection_item_model->delete_collection_item($id);
		redirect('advanced_backend/manage_collection_item');
	}

	public function delete_comment_item($id)
	{
		$this->backend_collection_item_model->delete_collection_item($id);
		redirect('advanced_backend/manage_user_comments');
	}
}
