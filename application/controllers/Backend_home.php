<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }
    
	//headerfooter
	public function headerfooter()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_headerfooter');
		$data['title'] = "Manage Header Footer";
		
		$data['logo'] = $this->home_model->get_header('logo');
		$data['short_description'] = $this->home_model->get_footer('short_description');
		$data['address'] = $this->home_model->get_footer('address');
		$data['phone1'] = $this->home_model->get_footer('phone1');
		$data['phone2'] = $this->home_model->get_footer('phone2');
		$data['email'] = $this->home_model->get_footer('email');
		$data['facebook'] = $this->home_model->get_footerlink('Facebook');
		$data['googleplus'] = $this->home_model->get_footerlink('Google-plus');
		$data['linkedin'] = $this->home_model->get_footerlink('Linkedin');
		$data['instagram'] = $this->home_model->get_footerlink('Instagram');
		$data['twitter'] = $this->home_model->get_footerlink('Twitter');
		$data['youtube'] = $this->home_model->get_footerlink('Youtube');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/headerfooter', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function update_home(){
		$a1=$this->backend_home_model->update_short_description();
		if($a1==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$a=$this->backend_home_model->update_address();
		if($a==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$b=$this->backend_home_model->update_phone1();
		if($b==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$b1=$this->backend_home_model->update_phone2();
		if($b1==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$c=$this->backend_home_model->update_email();
		if($c==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$d=$this->backend_home_model->update_facebook();
		if($d==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$e=$this->backend_home_model->update_googleplus();
		if($e==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$f=$this->backend_home_model->update_linkedin();
		if($f==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$g=$this->backend_home_model->update_instagram();
		if($g==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$h=$this->backend_home_model->update_twitter();
		if($h==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$i=$this->backend_home_model->update_youtube();
		if($i==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}

		$config['upload_path']          = './assets/img';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('logo')){
			$errors = array('error' => $this->upload->display_errors());
			redirect('backend_home/headerfooter');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_home_model->update_logo($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			redirect('backend_home/headerfooter');
		}
		
		$this->load->library('user_agent');
		redirect('backend_home/headerfooter');
	}
	//headerfooter

	//slide
	public function slides()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_slides');
		$data['title'] = "Manage Slides";
		$data['slides'] = $this->backend_slides_model->get_slider();
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/slides', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function slides_add()
	{
		$data['title'] = "Add Slides";
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/slides_add', $data);
		$this->load->view('backendnew/templates/footer');
	}

	
	public function slides_addprocess()
	{
		$post = $this->input->post();
	
		$config['upload_path']          = './assets/img/slides';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$file = $this->upload->data();
			$data = array(	'thumb_image'		=> $file['orig_name'],
							'position' => 'slider',
    						'created_at' =>date('Y-m-d H:i:s')
						);
							
			//Menyimpan Informasi File pada database;
			$this->backend_slides_model->add_sliderimage($data);
		}
	}

	public function slides_delete($id)
	{
		$this->backend_slides_model->delete_article($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete slide done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//slide

	//descriptionhome
	public function descriptionhome()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_descriptionhome');
		
		$data['home_content'] = $this->backend_home_model->get_home_content_by_position('home-content');
		$data['title'] = 'Update Home Content';
		$data['title'] = "Manage Description Home";
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/homedescription', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function update_descriptionhome()
	{
		$res =$this->backend_home_model->update_homecontent();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//descriptionhome

	//news
	public function news()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_news');
		$data['title'] = "Manage News";
		$data['newss'] = $this->backend_news_item_model->get_news_item_without_pagination();
		$data['news_content'] = $this->home_model->get_pageimage('News-image');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/news', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function picturenews_update()
	{
		$config['upload_path']          = './assets/img/imagepage';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('thumb_image')){
			$errors = array('error' => $this->upload->display_errors());
			redirect('backend_home/news');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_home_model->update_pageimage($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}

	public function news_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_news');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/news_add');
		$this->load->view('backendnew/templates/footer');
	}

	public function news_addprocess()
	{
		$general_link = url_title($this->input->post('title'));
		$exist = $this->backend_news_item_model->get_exist_generallink($general_link);

		if($exist==1)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
            	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-exclamation-sign"></i>
				Error!
			 </h4> 
			 <p>Add failed, title has been used!!!
			 </p>');
		  $this->load->library('user_agent');
			redirect('backend_home/news_add');
		}

		$config['upload_path']          = './assets/img/blog';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Add a Collection Item';
		if((!$this->upload->do_upload('thumb_image'))&&($exist==0)){
			$data['file_name']= "noimage.png";
			$res=$this->backend_news_item_model->add_news($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add news done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}

		if(($this->upload->do_upload('thumb_image'))&&($exist==0)){
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_news_item_model->add_news($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add news done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		$this->load->library('user_agent');
		redirect('backend_home/news');
	}

	public function news_edit($id)
	{
		$data['news'] = $this->backend_news_item_model->get_news_item_by_id($id);
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/news_edit', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function news_updateprocess($id)
	{
		$general_link = url_title($this->input->post('title'));
		$general_id = $this->input->post('id');
		$exist = $this->backend_news_item_model->get_exist_generallink($general_link,$general_id);

		$config['upload_path']          = './assets/img/blog';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(($this->upload->do_upload('thumb_image'))&&($exist==0)){
			$data['image_name']= $this->upload->data('file_name');
			$res=$this->backend_news_item_model->update_news_content($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} 

		if((!$this->upload->do_upload('thumb_image'))&&($exist==0)){
			$res=$this->backend_news_item_model->update_news_content($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}

		if($exist==1)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
            	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-exclamation-sign"></i>
				Error!
			 </h4> 
			 <p>Update failed, title has been used!!!
			 </p>');
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function news_delete($id)
	{
		$res=$this->backend_news_item_model->delete_news_item($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('backend_home/news');
	}
	//news

	//user comments
	public function usercomment()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_comments');
		$data['title'] = "Manage User Comments";
		$data['user_comments'] = $this->backend_comments_model->get_comments_without_pagination();
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/usercomments', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function usercomment_add()
	{
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header',$data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/usercomments_add');
		$this->load->view('backendnew/templates/footer');
	}

	public function usercomment_addprocess()
	{
		$res =$this->backend_comments_model->add_comments();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Add done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('backend_home/usercomment');
	}

	public function usercomment_edit($id)
	{
		$data['usercomment'] = $this->backend_comments_model->get_comment_by_id($id);
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/usercomments_edit', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function usercomment_updateprocess()
	{
		$res =$this->backend_comments_model->update_comment();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function usercomment_delete($id)
	{
		$res=$this->backend_comments_model->delete_user_comment($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('backend_home/usercomment');
	}
	//usercomment

	//partner
	public function ourpartners()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_partner');
		$data['partners'] = $this->backend_partners_model->get_partners_without_pagination();
		$data['title'] = "Manage Partners";
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/ourpartner', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function ourpartner_add()
	{
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header',$data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/ourpartner_add');
		$this->load->view('backendnew/templates/footer');
	}

	public function ourpartners_addprocess()
	{
		$config['upload_path']          = './assets/img/partners';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Add a Collection Item';
		if(!$this->upload->do_upload('thumb_image')){
			$data['file_name']= "noimage.png";
			$res=$this->backend_partners_model->add_partners_item($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add partner done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_partners_model->add_partners_item($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add partner done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		$this->load->library('user_agent');
		redirect('backend_home/ourpartners');
	}

	public function ourpartners_edit($id)
	{
		$data['partners'] = $this->backend_partners_model->get_partners_by_id($id);
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/ourpartner_edit', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function ourpartners_updateprocess($id)
	{
		$config['upload_path']          = './assets/img/partners';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if($this->upload->do_upload('thumb_image')){
			$data['image_name']= $this->upload->data('file_name');
			$res=$this->backend_partners_model->update_partners($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$res=$this->backend_partners_model->update_partners($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function ourpartners_delete($id)
	{
		$res=$this->backend_partners_model->delete_partners($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('backend_home/ourpartners');
	}
	//partner



}
