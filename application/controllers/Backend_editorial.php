<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_editorial extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }
    
	//legacy
	public function pictureeditorial()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_piceditorial');
		
		$data['editorial_content'] = $this->home_model->get_pageimage('Editorial-image');
		$data['title'] = 'Editorial Page Image';
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/editorialpicture', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function pictureeditorial_update()
	{
		$config['upload_path']          = './assets/img/imagepage';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('thumb_image')){
			$errors = array('error' => $this->upload->display_errors());
			redirect('backend_editorial/pictureeditorial');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_home_model->update_pageimage($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	
	public function editorialdescription()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_editorialdescription');
		
		$data['editorial_content'] = $this->backend_home_model->get_home_content_by_position('editorial-content');
		$data['title'] = 'Update Editorial Content';
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/editorialdescription', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function editorialdescription_update()
	{
		$res=$this->backend_home_model->update_editorialdescription();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//legacy

	

}
