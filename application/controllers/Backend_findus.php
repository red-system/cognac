<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_findus extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }
    
	//ourcognac
	public function picturefindus()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_picfindus');
		
		$data['findus_content'] = $this->home_model->get_pageimage('Findus-image');
		$data['title'] = 'Find Us Page Image';
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/finduspicture', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function picturefindus_update()
	{
		$config['upload_path']          = './assets/img/imagepage';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('thumb_image')){
			$errors = array('error' => $this->upload->display_errors());
			redirect('backend_legacy/picturefindus');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_home_model->update_pageimage($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	
	public function find()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_findus');
		
		$data['finds'] = $this->backend_find_model->get_find_without_pagination();
		$data['title'] = "Manage Find Us";
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/find', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function find_add()
	{
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/find_add');
		$this->load->view('backendnew/templates/footer');
	}

	public function find_addprocess()
	{
		$this->backend_find_model->add_find();

		$query = $this->db->query('SELECT id FROM article ORDER BY id DESC LIMIT 1');  
	    $result = $query->result_array();  
	    $id = $result[0]['id'];

	    $post = $this->input->post();
 
        $file = 0;
        foreach ($post['title'] as $key => $value) {
        	if(!empty($value)){
	        	$file++;
	        }	
        }

        for($i=0; $i<$file; $i++)
      	{
      		$data = array('ref_id'=> $id,
      					  'short_description' => $post['short_description'][$i],
                          'title' => $post['title'][$i]
                       	 );

      		$this->backend_find_model->add_findcompany($data);
      	}
      	$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Add Country done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect('backend_findus/find');
	}

	public function find_edit($id)
	{
		$data['find'] = $this->backend_find_model->get_find_by_id($id);
		$data['pts'] = $this->backend_find_model->get_find_pts($id);
		$data['title'] = 'Update Country';
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/find_edit', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function update_find(){
		$this->backend_find_model->update_find();

		$postId = $this->input->post('id');
		$post = $this->input->post();
		
        $file = 0;
        foreach ($postId as $key => $value) {
        	$data = array(
        					'id'=>$value,
        					'short_description' => $post['short_description'][$value],
                          	'title' => $post['title'][$value]
                       	 );
        	$this->backend_find_model->update_findcompany($data);
        }
        $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update Country done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function processcompany_add()
	{
		$postId = $this->input->post('id');
		$post = $this->input->post();
		
        $file = 0;
        foreach ($postId as $key => $value) {
        	$data = array(
        					'ref_id'=>$value,
        					'short_description' => $post['address'][$value][$key],
                          	'title' => $post['name'][$value][$key]
                       	 );
        	$this->backend_find_model->add_findcompany($data);
        }
        $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Add Company done!!!
			 </p>');
      	$this->load->library('user_agent');
		redirect($this->agent->referrer());
    }

	public function delete_company($id)
	{
		$res=$this->backend_find_model->delete_company($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function delete_contry($id)
	{
		$this->backend_find_model->delete_companys($id);
		$res=$this->backend_find_model->delete_contry($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('backend_findus/find');
	}
}
