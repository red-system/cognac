<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_legacy extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }
    
	//legacy
	public function picturelegacy()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_piclegacy');
		
		$data['legacy_content'] = $this->home_model->get_pageimage('Legacy-image');
		$data['title'] = 'Legacy Page Image';
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/legacypicture', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function picturelegacy_update()
	{
		$config['upload_path']          = './assets/img/imagepage';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('thumb_image')){
			$errors = array('error' => $this->upload->display_errors());
			redirect('backend_legacy/picturelegacy');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_home_model->update_pageimage($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	
	public function legacydescription()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_legacydescription');
		
		$data['legacy_content'] = $this->backend_home_model->get_home_content_by_position('legacy-content');
		$data['title'] = 'Update Legacy Content';
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/legacydescription', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function legacydescription_update()
	{
		$res=$this->backend_home_model->update_legacydescription();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//legacy

	//team
	public function team()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_team'); 

		$data['teams'] = $this->backend_team_model->get_team_without_pagination();
		$data['title'] = "Manage Team";
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/team', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function team_add()
	{
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/team_add');
		$this->load->view('backendnew/templates/footer');
	}

	public function team_addprocess()
	{
		$config['upload_path']          = './assets/img/team';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Add a Collection Item';
		if(!$this->upload->do_upload('thumb_image')){
			$data['file_name']= "noimage.png";
			$res=$this->backend_team_model->add_team($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add team done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_team_model->add_team($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add team done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		$this->load->library('user_agent');
		redirect('backend_legacy/team');
	}

	public function team_edit($id)
	{
		$data['team'] = $this->backend_team_model->get_team_by_id($id);
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/team_edit', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function team_updateprocess($id)
	{
		$config['upload_path']          = './assets/img/team';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if($this->upload->do_upload('thumb_image')){
			$data['image_name']= $this->upload->data('file_name');
			$res=$this->backend_team_model->update_team($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$res=$this->backend_team_model->update_team($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function team_delete($id)
	{
		$res=$this->backend_team_model->delete_team($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('backend_legacy/team');
	}

}
