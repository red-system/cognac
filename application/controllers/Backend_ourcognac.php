<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_ourcognac extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }
    
	//ourcognac
	public function pictureourcognac()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_picourcognac');
		
		$data['ourcognac_content'] = $this->home_model->get_pageimage('Ourcognac-image');
		$data['title'] = 'Our Cognac Page Image';
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/ourcognacpicture', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function pictureourcognac_update()
	{
		$config['upload_path']          = './assets/img/imagepage';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('thumb_image')){
			$errors = array('error' => $this->upload->display_errors());
			redirect('backend_legacy/pictureourcognac');
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$res=$this->backend_home_model->update_pageimage($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}

	public function picourcognac_add($id)
	{
		$data['ourcognac'] = $this->backend_collection_item_model->get_collection_item_by_id($id);
		$data['ourcognacpic'] = $this->backend_collection_item_model->get_collection_pic($id);
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/picourcognac_add', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function pictureourcognac_add()
	{
		$post = $this->input->post();
	
		$config['upload_path']          = './assets/img/collection';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$file = $this->upload->data();
			$data = array(	'image'		=> $file['orig_name'],
							'article_id'	=> $this->input->post('article_id'),
						);
			$this->backend_collection_item_model->picourcognac_add($data);
		}
	}

	public function picourcognac_delete($id)
	{
		$this->backend_collection_item_model->picourcognac_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete picture done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	
	public function ourcognac()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_ourcognac');
		
		$data['ourcognacs'] = $this->backend_collection_item_model->get_collection_item_without_pagination();
		$data['title'] = "Manage Our Cognac";
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/ourcognac', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function ourcognac_add()
	{
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/ourcognac_add');
		$this->load->view('backendnew/templates/footer');
	}

	public function ourcognac_addprocess()
	{
		$general_link = url_title($this->input->post('title'));
		$exist = $this->backend_collection_item_model->get_exist_generallink($general_link);

		if($exist==1)
		{
		  	$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	            	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-exclamation-sign"></i>
					Error!
				 </h4> 
				 <p>Add failed, title has been used!!!
				 </p>');
		  	$this->load->library('user_agent');
		  	redirect('backend_ourcognac/ourcognac_add');
		}

		$config['upload_path']          = './assets/img/collection';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		$data['title'] = 'Add a Collection Item';
		if(($this->upload->do_upload('thumb_image'))&&($exist==0)){
			$data['file_name']= $this->upload->data('file_name');
			$this->backend_collection_item_model->add_cognac_collection($data);
			
			$query = $this->db->query('SELECT id FROM article ORDER BY id DESC LIMIT 1');  
		    $result = $query->result_array();  
		    $general_id = $result[0]['id'];
			
			$files = $_FILES;
	        $count = count($_FILES['uploadfile']['name']);
	        for($i=0; $i<$count; $i++)
            {
                $_FILES['uploadfile']['name']= $files['uploadfile']['name'][$i];
                $_FILES['uploadfile']['type']= $files['uploadfile']['type'][$i];
                $_FILES['uploadfile']['tmp_name']= $files['uploadfile']['tmp_name'][$i];
                $_FILES['uploadfile']['error']= $files['uploadfile']['error'][$i];
                $_FILES['uploadfile']['size']= $files['uploadfile']['size'][$i];
                $config2['upload_path'] = './assets/img/collection';
            	$config2['allowed_types'] = 'gif|jpg|png|jpeg';
             	$config2['max_size']    = 10000;
           		$config2['max_width']  = 20000;
            	$config2['max_height']  = 20000;
        		$this->upload->initialize($config2);
                $this->upload->do_upload('uploadfile');
                $upload_data = $this->upload->data();
                $name_array[] = $upload_data['file_name'];
                $fileName = $upload_data['file_name'];
                $images[] = $fileName;

            }
          	$fileName = $images;
          	$file = count($fileName);
          	for($i=0; $i<$file; $i++)
          	{
          		$data = array('article_id'=> $general_id,
	                          'image' => $fileName[$i]);
          		$this->backend_collection_item_model->picourcognac_add($data);
          	}
          	$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add project done!!!
				 </p>');
          	$this->load->library('user_agent');
			redirect('backend_ourcognac/ourcognac');
        	}
	}

	public function ourcognac_edit($id)
	{
		$data['ourcognac'] = $this->backend_collection_item_model->get_collection_item_by_id($id);
		$data['ourcognacpic'] = $this->backend_collection_item_model->get_collection_pic($id);
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backendnew/templates/header', $data);
		$this->load->view('backendnew/templates/menu');
		$this->load->view('backendnew/ourcognac_edit', $data);
		$this->load->view('backendnew/templates/footer');
	}

	public function ourcognac_updateprocess($id)
	{
		$general_link = url_title($this->input->post('title'));
		$general_id = $this->input->post('id');
		$exist = $this->backend_news_item_model->get_exist_generallink($general_link,$general_id);

		$config['upload_path']          = './assets/img/collection';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(($this->upload->do_upload('thumb_image'))&&($exist==0)){
			$data['image_name']= $this->upload->data('file_name');
			$res=$this->backend_collection_item_model->update_cognac_collection($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} 

		if((!$this->upload->do_upload('thumb_image'))&&($exist==0)){ 
			$res=$this->backend_collection_item_model->update_cognac_collection($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}

		if($exist==1)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
            	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-exclamation-sign"></i>
				Error!
			 </h4> 
			 <p>Update failed, title has been used!!!
			 </p>');
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function ourcognac_delete($id)
	{
		$this->backend_collection_item_model->picourcognac_delete($id);
		$res=$this->backend_collection_item_model->delete_collection_item($id);
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Delete done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		$this->load->library('user_agent');
		redirect('backend_ourcognac/ourcognac');
	}



}
