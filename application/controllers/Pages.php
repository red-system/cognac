<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pages extends CI_Controller {
	public function index()
	{
		$data['sliders'] = $this->home_model->get_slider();
		$data['home_content'] = $this->backend_home_model->get_home_content_by_position('home-content');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Home";
    $this->load->view('templates/header',$data);
		$this->load->view('home');
    $this->load->view('templates/footer');
	}

	public function editorial()
	{
		$data['editorial_content'] = $this->backend_home_model->get_home_content_by_position('editorial-content');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Editorial";
		$this->load->view('templates/header',$data);
		$this->load->view('editorial');
		$this->load->view('templates/footer');
	}

	public function rekindled()
	{
		$data['rekindled_content'] = $this->backend_home_model->get_home_content_by_position('rekindled-content');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Rekindled";
		$this->load->view('templates/header',$data);
		$this->load->view('rekindled');
		$this->load->view('templates/footer');
	}

	public function perfect_ballance()
	{
		$data['perfect_ballance_content'] = $this->backend_home_model->get_home_content_by_position('perfect-ballance-content');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Perfect Ballance";
		$this->load->view('templates/header',$data);
		$this->load->view('perfect_ballance');
		$this->load->view('templates/footer');
	}

	public function collection()
	{
		$data['collection_header'] = $this->backend_home_model->get_home_content_by_position('collection-header');
		$data['collection_items'] = $this->backend_collection_item_model->get_collection_item();
		$data['count']=$this->backend_collection_item_model->get_count_by_position('collection-item');
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Collection";
		$this->load->view('templates/header',$data);
		$this->load->view('collection');
		$this->load->view('templates/footer');
	}

	public function collection_pagination($active_row = FALSE, $row_per_page=FALSE, $row_start=FALSE)
	{
		$data['active_row_controller'] = $active_row;
		$data['row_per_page'] = $row_per_page;
		$data['row_start'] =  $row_start;
		$data['collection_header'] = $this->backend_home_model->get_home_content_by_position('collection-header');
		$result_arrays =  $this->backend_collection_item_model->get_collection_item_by_pagination($data);
		$data['collection_items'] = $result_arrays['result_array'];
		$data['count']=$this->backend_collection_item_model->get_count_by_position('collection-item');
		$data['active_row_controller'] = $result_arrays['active_row_controller'];
		$data['row_per_page'] = $result_arrays['row_per_page'];
		$data['row_start'] =  $result_arrays['row_start'];
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Collection";
		$this->load->view('templates/header',$data);
		$this->load->view('collection');
		$this->load->view('templates/footer');
	}

	public function collection_details($slug = NULL)
	{
		$data['collection_details'] = $this->backend_collection_item_model->get_collection_item($slug);
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$data['title'] = "Collection Details";
		$this->load->view('templates/header',$data);
		$this->load->view('collection_details');
		$this->load->view('templates/footer');
	}

	public function testings()
	{
		$data['testings_content'] = $this->backend_home_model->get_home_content_by_position('testing-content');
		$data['title'] = "Testings";
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$this->load->view('templates/header',$data);
		$this->load->view('testings');
		$this->load->view('templates/footer');
	}

	public function collumns()
	{
		$data['collumns_header'] = $this->backend_home_model->get_home_content_by_position('collumns-header');
		$data['collumns_items'] =$this->backend_collumns_item_model->get_collumn_items_without_pagination();
		$data['title'] = "Collumns";
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$this->load->view('templates/header',$data);
		$this->load->view('collumns');
		$this->load->view('templates/footer');
	}
	public function contacts()
	{
		$data['contacts_content'] = $this->backend_home_model->get_home_content_by_position('contacts-content');
		$data['title'] = "Contacts";
		$data['footer_collumns'] = $this->home_model->get_footer_collumns();
		$data['footer_content'] = $this->home_model->get_footer_content();
		$data['social_medias'] = $this->home_model->get_social_medias();
		$this->load->view('templates/header',$data);
		$this->load->view('contacts');
		$this->load->view('templates/footer');
	}
}
