<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function index()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');

	    $this->load->view('backendnew/templates/header',$data);
	    $this->load->view('backendnew/templates/menu');
	    $this->load->view('backendnew/templates/body');
	    $this->load->view('backendnew/templates/footer');
	}
}