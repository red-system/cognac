<div class="col-md-12">
  <div class="block-web">
    <div class="header">
      <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
      <h3 class="panel-heading border center"><?php echo $title;?></h3>
    </div>
    
    <div class="porlets-content">
      <!--<form action="" class="form-horizontal row-border"> -->
      <!-- <div class="bs-example">
        <div class="alert alert-warning fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          Damn
        </div>
      </div> -->
        <?php echo form_open_multipart('advanced_backend/update_find/'.$find['id'],'class="form-horizontal row-border" id="articleCreateForm"');?>
        <div class="header"><br>
          <div class="form-group">
              <label class="col-sm-3 control-label"><h5><strong>Country</strong></h5></label>
              <div class="col-sm-9">
                <input type="hidden" class="form-control" name="idcountry" value="<?php echo $find['id'];?>" id="title">
                <input type="text" class="form-control" name="country" id="title" value="<?php echo $find['title'];?>">
                <?php echo form_error('title','<p class="alert alert-danger center">','</p>'); ?>
              </div>
            </div><!--/form-group-->
        </div>

        <?php foreach ($pts as $pt) :  ?>
           <div class="header"><br>
             <div class="form-group">
                <label class="col-sm-3 control-label"><h6><strong>Company</strong></h6></label>
                <div class="col-sm-9">
                   <input type="hidden" class="form-control" name="id[]" value="<?php echo $pt['id'];?>" id="title">
                  <input type="text" class="form-control" name="title[]" id="title" value="<?php echo $pt['title'];?>">
                  <?php echo form_error('content','<p class="alert alert-danger center">','</p>'); ?>
                </div>
              </div>
              
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Address</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="short_description[]" value="<?php echo $pt['short_description'];?>">
                </div>
              </div>

           </div>

        <?php endforeach; ?>

        

        
        <div class="bottom center">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="<?php echo base_url(); ?>advanced_backend/manage_find" ><button type="button" class="btn btn-default">Cancel</button></a>
        </div><!--/form-group-->
      </form>
    </div><!--/porlets-content-->
  </div><!--/block-web-->
</div><!--/col-md-6-->
